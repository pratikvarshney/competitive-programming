#include<stdio.h>
#include<math.h>

#define MAX_PRIME 1000000

_Bool sieve[MAX_PRIME]={0};

int init_primes();
int circular(int n);

int main()
{
    init_primes();

    int i, num=0, len;

    for(i=2; i<MAX_PRIME; i++)
    {
        if(!sieve[i])
        {
            if(circular(i))
            {
                num++;
                printf("%d\n", i);
            }
        }
    }

    printf("\nANSWER = %d", num);

    return 0;
}


int init_primes()
{
    int i, j, step;
    int limit=sqrt(MAX_PRIME);

    printf("\nInitializing Primes\n");
    sieve[0]=1;
    sieve[1]=1;
    for(i=2+2; i<MAX_PRIME; i+=2)
    {
        sieve[i]=1;
    }
    for(i=3; i<=limit; i+=2)
    {
        if(!sieve[i])
        {
            step=(2*i);
            for(j=3*i; j<MAX_PRIME; j += step)
            {
                sieve[j]=1;
            }
        }
    }
    printf("\nInitialization Done\n");
    return 0;
}


int circular(int n)
{
    int len=0, power=1, i=0, num=n;

    while(num>0)
    {
        len++;
        num /= 10;
        power *= 10;
    }
    power /= 10;
    num = n;

    for(i=0; i<len; i++)
    {
        if(sieve[num]) return 0;
        num = ((num%10)*power) + (num/10);
    }

    return len;
}
