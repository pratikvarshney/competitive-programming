import java.math.BigDecimal;
class p80 {
	public static int precision = 100;
	public static void main(String[] arr){
		long start = System.currentTimeMillis();
		long total = 0;
		int s = 2;
		for(int i=2; i<100; i++){
			if(s * s != i){
				total += digitalSum(sqrt(i).movePointRight(precision).toPlainString());
			}else{
				System.out.println("skipping: "+i);
				s++;
			}
		}
		System.out.println("answer: "+ total);
		System.out.println("Time (ms): "+ (System.currentTimeMillis() - start));
	}

	public static long digitalSum(String str){
		long sum = 0;
		for(int i = 0; i < precision; i++){
			sum += Long.parseLong(str.charAt(i)+"");
		}
		return sum;
	}
	public static BigDecimal sqrt(int n){
		BigDecimal a = new BigDecimal("0");
		BigDecimal b = new BigDecimal(""+n);
		BigDecimal k = new BigDecimal(""+n);
		BigDecimal two = new BigDecimal("2");
		BigDecimal err = new BigDecimal("0.1").movePointLeft(precision);
		while(k.subtract(a.multiply(a)).compareTo(err)>0){
			if(a.compareTo(b)==0){
				break;
			}
			BigDecimal mid = a.add(b).divide(two);
			if(mid.multiply(mid).compareTo(k) <= 0){
				a = mid;
			}else{
				b = mid;
			}
		}
		// System.out.println(a.toPlainString());
		return a;
	}
}

// answer: 40886
// Time (ms): 465