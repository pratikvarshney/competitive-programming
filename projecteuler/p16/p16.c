#include<stdio.h>
//group of 8
#define GROUP 100000000
//also remember to reset %0nld in printf
int main()
{
    long result[50], carry, sum;
    int grp=1, i, j;
    result[0]=1;
    for(i=0; i<1000; i++)
    {
        carry=0;
        for(j=0; j<grp; j++)
        {
            result[j]*=2;
            result[j]+=carry;
            if(result[j]>=GROUP)
            {
                carry=result[j]/GROUP;
                result[j]=result[j]%GROUP;
            }
            else
            {
                carry=0;
            }
        }
        if(carry)
        {
            result[grp]=carry;
            grp++;
        }
    }
    sum=0;
    for(i=grp-1; i>=0; i--)
    {
        printf("%08ld\n",result[i]);
        j=result[i];
        while(j>0)
        {
            sum = sum + (j%10);
            j/=10;
        }
    }
    printf("\n\nGRP : %d",grp);
    printf("\n\nSUM : %d",sum);
    return 0;
}
