#include<stdio.h>
#include<math.h>

int d(int n);

int main()
{
    int ab[7000];
    int val[28124]={1};
    int limit=28123, i, j, ab_length;
    int sum;

    ab_length=0;
    for(i=1; i<=limit; i++)
    {
        if(d(i)>i)
        {
            ab[ab_length]=i;
            ab_length++;
        }
    }


    for(i=0; i<ab_length; i++)
      {
        for(j=i; j<ab_length; j++)
          {
            if((ab[i]+ab[j]) <= limit) val[(ab[i]+ab[j])] = 1;
            else break;
          }
      }

    sum = 0;
    for(i=1; i<=limit; i++)
    {
        if(val[i]==0)
        {
            sum += i;
            //printf("%d\n", i);
        }
    }

    printf("\nsum = %d", sum);

    return 0;
}

int d(int input)
{
    int sum, i;
    int max=floor(sqrt(input));

    if(input==0) return 0;
    if(max==1) return 1;

    sum=1;

    if(input%max == 0)
    {
      sum += max;
      if((input/max)!=max) sum += (input/max);
    }
    max--;

    for(i=max; i>1; i--)
    {
      if(input%i == 0)
        {
          sum += i;
          sum += (input/i);
        }
    }

    return sum;
}
