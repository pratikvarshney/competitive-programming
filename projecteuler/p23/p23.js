function d(n)
{
  var input = Number(n);
  var max=Math.floor(Math.sqrt(input));
  if(input == 0) return 0;
  if(max == 1) return 1;
  var sum=1;
  if(input%max == 0)
    {
      sum += max;
      if((input/max)!=max) sum += (input/max);
    }
  max--;
  for(var i=max; i>1; i--)
    {
      if(input%i == 0)
        {
          sum += i;
          sum += (input/i);
        }
    }
  return sum;
}

var ab=[];
var limit=28123;

for(var i=1; i<=limit; i++)
  {
    if(d(i)>i) ab.push(i);
  }

var val=[];
for(var i=0; i<=limit; i++) val.push(1);
val[0]=0;
for(var i=0; i<ab.length; i++)
  {
    for(var j=i; j<ab.length; j++)
      {
        if((ab[i]+ab[j]) <= limit) val[(ab[i]+ab[j])] = 0;
        else break;
      }
  }

sum = 0;
for(var i=1; i<val.length; i++)
  {
    if(val[i]==1) sum += i;
  }
alert(sum);

