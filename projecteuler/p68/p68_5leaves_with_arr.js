var start = new Date().getTime();
var max = 10;
var numLeaves = 5;
var str = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10
];
var arr = [
];
var answer = 0;
function getArrayCopyAndAddValue(arr, val) {
  var a = [
  ];
  for (var i = 0; i < arr.length; i++) {
    a[i] = arr[i];
  }
  a.push(val);
  return a;
}
function getArrayCopyAndRemoveIndex(arr, r) {
  var a = [
  ];
  for (var i = 0, j = 0; i < arr.length; i++) {
    if (i != r) {
      a[j] = arr[i];
      j++;
    }
  }
  return a;
}
function compute(nodeArr) {
  var sum = nodeArr[0] + nodeArr[6] + nodeArr[7];
  if ((nodeArr[1] + nodeArr[7] + nodeArr[8]) == sum) {
    if ((nodeArr[2] + nodeArr[8] + nodeArr[9]) == sum) {
      if ((nodeArr[3] + nodeArr[9] + nodeArr[5]) == sum) {
        if ((nodeArr[4] + nodeArr[5] + nodeArr[6]) == sum) {
          var val = '' + nodeArr[0] + nodeArr[6] + nodeArr[7] + nodeArr[1] + nodeArr[7] + nodeArr[8] + nodeArr[2] + nodeArr[8] + nodeArr[9] + nodeArr[3] + nodeArr[9] + nodeArr[5] + nodeArr[4] + nodeArr[5] + nodeArr[6] + '';
          console.log(val);
          if ((val.length == 16) && (answer < Number(val))) {
            answer = Number(val);
          }
        }
      }
    }
  }
}
function process(processed, remaining) {
  if (remaining.length == 0) {
    arr.push(processed);
  } else {
    for (var i = 0; i < remaining.length; i++) {
      if (((processed.length < numLeaves) && (processed[0] < remaining[i])) || (processed.length >= numLeaves)) {
        process(getArrayCopyAndAddValue(processed, remaining[i]), getArrayCopyAndRemoveIndex(remaining, i));
      }
    }
  }
}
for (var i = max - 4; i > 0; i--) {
  process([i], getArrayCopyAndRemoveIndex(str, i - 1));
}
console.log('init time = ' + (new Date().getTime() - start));
for (var i = 0; i < arr.length; i++) {
  compute(arr[i]);
}
console.log('answer = ' + answer);
console.log('total time = ' + (new Date().getTime() - start));
// init time = 587
// 6357528249411013
// 6531031914842725
// 2594936378711015
// 27858434106101917
// 24105101817673934
// 28797161103104548
// 21049436378715110
// 2951051817673439
// 18102107379496568
// 16103104548782926
// 11085864693972710
// 11069627285843410
// answer = 6531031914842725
// total time = 608
