var max = 6;
var numLeaves = 3;
var str = [
  1,
  2,
  3,
  4,
  5,
  6
];
var arr = [
];
var res = [
];
function getArrayCopyAndAddValue(arr, val) {
  var a = [
  ];
  for (var i = 0; i < arr.length; i++) {
    a[i] = arr[i];
  }
  a.push(val);
  return a;
}
function getArrayCopyAndRemoveIndex(arr, r) {
  var a = [
  ];
  for (var i = 0, j = 0; i < arr.length; i++) {
    if (i != r) {
      a[j] = arr[i];
      j++;
    }
  }
  return a;
}
function process(processed, remaining) {
  if (remaining.length == 0) {
    arr.push(processed);
  } else {
    for (var i = 0; i < remaining.length; i++) {
      // to ensure processed[0] is smallest value among all leaves, thus saves duplicate processing
      if (((processed.length < numLeaves) && (processed[0] < remaining[i])) || (processed.length >= numLeaves)) {
        process(getArrayCopyAndAddValue(processed, remaining[i]), getArrayCopyAndRemoveIndex(remaining, i));
      }
    }
  }
}
for (var i = max - 2; i > 0; i--) {
  process([i], getArrayCopyAndRemoveIndex(str, i-1));
}
for (var i = 0; i < arr.length; i++) {
  var sum = arr[i][0] + arr[i][3] + arr[i][5];
  if ((arr[i][2] + arr[i][5] + arr[i][4]) == sum) {
    if ((arr[i][1] + arr[i][4] + arr[i][3]) == sum) {
      res.push('' + arr[i][0] + arr[i][3] + arr[i][5] + arr[i][2] + arr[i][5] + arr[i][4] + arr[i][1] + arr[i][4] + arr[i][3] + '');
    }
  }
}
res.sort().reverse()[0];
