#include<stdio.h>

#define NUM_COINS 8

int sum=0;
int val=200;
int c[NUM_COINS]={1,2,5,10,20,50,100,200};

int rec(int n, int start);

int main()
{
    rec(0, 0);
    printf("%d", sum);
    return 0;
}

int rec(int n, int start)
{
    int i, curr;
    for(i=start; i<NUM_COINS; i++)
    {
        curr=n+c[i];
        if(curr==val)
        {
            sum++;
            break;
        }
        if(curr>val) break;
        if(curr<val)
        {
            rec(curr, i);
        }
    }
    return 0;
}
