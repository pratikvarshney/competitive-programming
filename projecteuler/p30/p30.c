#include<stdio.h>

#define MAX_CHECK 1000000

int powers[10]={0};

int init_digit_powers(int n);
int digits_sum(int n);

int main()
{
    init_digit_powers(5);

    int i, sum=0;

    for(i=10; i<MAX_CHECK; i++)
    {
        if(i==digits_sum(i))
        {
            sum += i;
            printf("%d\n", i);
        }
    }
    printf("\nANSWER = %d", sum);
    return 0;
}

int init_digit_powers(int n)
{
    int i, j;
    powers[0]=0;
    powers[1]=1;
    for(i=2; i<10; i++)
    {
        powers[i]=i;
        for(j=1; j<n; j++) powers[i] *= i;
    }
    //for(i=0; i<10; i++) printf("%d, %d\n", i, powers[i]);
    return 0;
}

int digits_sum(int n)
{
    int sum=0;
    while(n>0)
    {
        sum += powers[n%10];
        n/=10;
    }
    return sum;
}
