/* 15/03/2013 */

/*
http://projecteuler.net/problem=3

Largest prime factor
Problem 3
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
*/

/*

maxm = 6857

*/
#include<stdio.h>
int main()
{
    unsigned long long num=600851475143L;
    //int num = 216600;
    int a = 3, maxm = 1;
    while(num%2==0)
    {
        num = num/2; /* or num>>1 */
        maxm = 2;
        printf("\n%d",2);
    }
    while(num>=a)
    {
        if(num%a==0)
        {
            num = num/a;
            maxm = a;
            printf("\n%d",a);
        }
        else a+=2;
    }
    printf("\nmaxm = %d\n",maxm);
    return 0;
}
