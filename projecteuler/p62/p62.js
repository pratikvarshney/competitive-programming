var permutationsLimit = 5;
function isPermutation(a, b) {
  return compareArr(digitsArr(a), digitsArr(b));
}
function compareArr(a, b) {
  for (var i = 0; i < a.length; i++) {
    if (a[i] != b[i]) {
      return false;
    }
  }
  return true;
}
var cache = new Object();
function digitsArr(a) {
  if (cache[a] != undefined) return cache[a];
  var digits = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  ];
  var arr = a.split('');
  for (var i = 0; i < arr.length; i++) {
    digits[Number(arr[i])]++;
  }
  cache[a] = digits;
  return digits;
}
function process() {
  var start = new Date().getTime();
  var cubeList = [
  ];
  for (var i = 0; ; i++) {
    var cube = (i * i * i) + '';
    cubeList[i] = [
      cube
    ];
    for (var j = 0; j < i; j++) {
      if (isPermutation(cubeList[j][0], cube)) {
        cubeList[j].push(cube);
        if (cubeList[j].length == permutationsLimit) {
          console.log('i = ' + i + ', answer = ' + cubeList[j][0] + ', time = ' + ((new Date().getTime()) - start));
          return cubeList[j][0];
        }
      }
    }
  }
}
process();
// i = 8384, answer = 127035954683, time = 56677
