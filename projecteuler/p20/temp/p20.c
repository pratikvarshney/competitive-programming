#include<stdio.h>
//group of 4
#define GROUP 10000
//also remember to reset %0nld in printf
int main()
{
    unsigned long long result[1000], carry, sum;
    int grp=1, i, j;
    result[0]=1;
    for(i=1; i<=1000; i++)
    {
        carry=0;
        for(j=0; j<grp; j++)
        {
            result[j]*=i;
            result[j]+=carry;
            if(result[j]>=GROUP)
            {
                carry=result[j]/GROUP;
                result[j]=result[j]%GROUP;
            }
            else
            {
                carry=0;
            }
        }
        if(carry)
        {
            result[grp]=carry;
            grp++;
        }
    }
    sum=0;
    for(i=grp-1; i>=0; i--)
    {
        printf("%04ld\n",result[i]);
        /*
        j=result[i];
        while(j>0)
        {
            sum = sum + (j%10);
            j/=10;
        }
        */
    }
    printf("\n\nGRP : %d",grp);
    //printf("\n\nSUM : %d",sum);
    return 0;
}
