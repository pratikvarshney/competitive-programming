class p40
{
	public static void main(String[] arg)
	{
		StringBuilder str = new StringBuilder(1000009);
		for(int i=1; str.length()<1000000; i++)
		{
			str.append(i);
		}
		int prod=1;
		for(int i=1; i<=1000000; i*=10)
		{
			prod *= (str.charAt(i-1)-'0');
		}
		
		System.out.println(prod);
	}
}