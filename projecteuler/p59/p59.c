#include<stdio.h>
#include <ctype.h>

void decrypt(int* a, int len, int* key, int key_length, int* decrypted);
int getFirstPossibility(int* a, int len, int key_index, int key_length);
int test(int i);

int main(){
	printf("test: %d^%d=%d\n", 65, 42, 65^42);
	/* read file data */
	FILE *fp = fopen("p059_cipher.txt", "r");
	int len=0;
	int a[1202];
	int decrypted[1202];
	int ch;
	int key[] = {'a', 'a', 'a'};
	int key_length = 3;
	while(1){
		fscanf(fp, "%d", &(a[len]));
		len++;

		/* skip comma, end reading at new line character */
		ch = fgetc(fp);
		if(ch=='\r' || ch=='\n'){
			break;
		}
	}
	/* find key */
	int i;
	printf("key : ");
	for(i=0; i<key_length; i++){
		key[i] = getFirstPossibility(a, len, i, key_length);
		printf("%c", key[i]);
	}
	printf("\n");
	/* show result */
	decrypt(a, len, key, key_length, decrypted);
	return 0;
}

int getFirstPossibility(int* a, int len, int key_index, int key_length){
	int i, j, k;
	int p[26]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	for(i=key_index; i<len; i+=key_length){
		for(j=0; j<26; j++){
			if(p[j]){
				p[j] = test(a[i]^(j+'a'));			
			}
		}
	}
	for(j=0; j<26; j++){
		if(p[j]){
			return (j+'a');		
		}
	}
	return '*';
}

void decrypt(int* a, int len, int* key, int key_length, int* decrypted){
	int i, j;
	int sum = 0;
	printf("\n----------\n");
	for(i=0, j=0; i<len; i++, j = (j+1)%key_length){
		decrypted[i] = a[i]^key[j];
		printf("%c", decrypted[i]);
		sum += decrypted[i];
	}
	printf("\n----------\n");
	printf("sum = %d\n", sum);
}

int test(int i){
	if('|'==i)return 0;
	if('`'==i)return 0;
	return isprint(i);
}
