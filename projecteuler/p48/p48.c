#include<stdio.h>

#define DIV 10000000000

unsigned long long power(unsigned long long a, unsigned long long b);

int main()
{
    unsigned long long i;
    unsigned long long sum=0;
    for(i=1; i<=1000; i++)
    {
        sum = (sum + power(i,i))%DIV;
    }
    printf("%llu\n", sum);
    return 0;
}

unsigned long long power(unsigned long long a, unsigned long long b)
{
    unsigned long long r=1;
    while(b>0)
    {
        r *= a;
        if(r > DIV) r = r%DIV;
        b--;
    }
    return r;
}
