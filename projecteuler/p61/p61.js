function Triangle(n) {
  return (n * (n + 1)) / 2;
}
function Square(n) {
  return n * n;
}
function Pentagonal(n) {
  return (n * ((3 * n) - 1)) / 2;
}
function Hexagonal(n) {
  return n * ((2 * n) - 1);
}
function Heptagonal(n) {
  return (n * ((5 * n) - 3)) / 2;
}
function Octagonal(n) {
  return n * ((3 * n) - 2);
}
function getValues(handle) {
  var i = 1;
  while (handle(i) < 1000) i++;
  var arr = [
  ];
  while (true) {
    var val = handle(i);
    if (val >= 10000) {
      break;
    }
    addValue(arr, val);
    i++;
  }
  return arr;
}
function addValue(arr, val) {
  var lastTwoDigits = val % 100;
  if (lastTwoDigits < 10) return;
  var firstTwoDigits = (val - lastTwoDigits) / 100;
  if (arr[firstTwoDigits] == undefined) {
    arr[firstTwoDigits] = [
    ];
  }
  arr[firstTwoDigits].push(lastTwoDigits);
}
var list = new Array(6);
// Triangle
list[0] = getValues(Triangle);
list[1] = getValues(Square);
list[2] = getValues(Pentagonal);
list[3] = getValues(Hexagonal);
list[4] = getValues(Heptagonal);
list[5] = getValues(Octagonal);
var pathArr = [
];
var digits = [
  1,
  2,
  3,
  4,
  5
];
function path(a, b) {
  if (b.length > 1) {
    for (var i = 0; i < b.length; i++) {
      var temp = b.slice(0); // clone
      temp.splice(i, 1);
      path(a + b[i].toString(), temp);
    }
  } else {
    pathArr.push('0' + a + b[0].toString());
  }
}
function contains(a, b){
	if(a!=undefined){
for(var i=0; i<a.length; i++){
if(a[i]==b) return true;
}
}
return false;
}
function getSum(a){
var sum = 0;
var b = a.split(',');
for(var i=0; i<b.length; i++){
sum += Number(b[i]);
}
return sum;
}
function check(firstTwoDigits, lastTwoDigitsArr, currPath, index, pathVal) {
//console.log('index: '+index);
if(index==6){
if (contains(lastTwoDigitsArr, firstTwoDigits)) {
          // found
          console.log('found : '+currPath + ' : ' + pathVal + firstTwoDigits + ' :: sum = ' + getSum(pathVal + firstTwoDigits));
        }
}else{
for (var i = 0; i < lastTwoDigitsArr.length; i++) {
    if (list[currPath[index]][lastTwoDigitsArr[i]] != undefined) {
      check(firstTwoDigits, list[currPath[index]][lastTwoDigitsArr[i]], currPath, index + 1, pathVal + lastTwoDigitsArr[i] + ',' + lastTwoDigitsArr[i]);
    }
  }
}
  
}
path('', digits);
for (i = 0; i < pathArr.length; i++) {
  //console.log('path: '+pathArr[i]);
  for (var firstTwoDigits in list[0]) {
    check(firstTwoDigits, list[0][firstTwoDigits], pathArr[i], 1, '' + firstTwoDigits);
  }
}

// answer = found : 014532 : 8256,5625,2512,1281,8128,2882 :: sum = 28684
