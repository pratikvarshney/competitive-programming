function isPrime(num)
{
	if(num%2==0) return false;
	for(var i=3; i<=Math.sqrt(num); i+=2)
	{
		if(num%i==0) return false;
	}
	return true;
}

var arr = [];
var digits = [1,2,3,4,5,6,7,8,9];
function perm(a, b)
{
	if(b.length>1)
	{
		for(var i=0; i<b.length; i++)
		{
			var temp=b.slice(0);
			temp.splice(i,1);
			perm(a+b[i].toString(), temp);
		}
	}
	else
	{
		arr.push(Number(a+b[0].toString()));
	}
}
for(var i=digits.length-1; i>=0; i--)
{
	perm("", digits);
	digits.pop();
}
arr.sort(function(a, b){return b-a});
var res=0;
for(var i=0; i<arr.length; i++)
{
	if(isPrime(arr[i])){
		res = arr[i];
		break;
	}
}
res;
/* 7652413 */