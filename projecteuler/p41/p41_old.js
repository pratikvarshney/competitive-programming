var arr=[];
var digits=[1,2,3,4,5,6,7,8,9];
for(var i=0; i<digits.length; i++) arr[i]=[];
function pandigit(a,b,c)
{
	for(var i=0; i<b.length; i++)
	{
		var temp=b.slice(0);
		var d=a+temp.splice(i,1)[0].toString();
		arr[c].push(d);
		pandigit(d,temp,c+1);
	}
}
pandigit("",digits,0);

function isPrime(str)
{
	var num = Number(str);
	if(num%2==0) return false;
	for(var i=3; i<=Math.sqrt(num); i+=2)
	{
		if(num%i==0) return false;
	}
	return true;
}
var pr=[];
for(var k=8; k>=0; k--)
{
	for(var i=0; i<arr[k].length; i++)
	{
		if(isPrime(arr[k][i])){pr.push(Number(arr[k][i]));}
	}
	if(pr.length>0) break;
}
pr.sort(function(a, b){return b-a});
pr[0];