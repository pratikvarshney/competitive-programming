#include<stdio.h>
#include<math.h>

#define MAX_PRIME 1000000

_Bool sieve[MAX_PRIME]={0};

int init_primes();
int num_primes(int a, int b);

int main()
{
    init_primes();

    int a, b, n;
    int max_a=0, max_b=0, max_n=0;

    for(a=-999; a<1000; a++)
    {
        for(b=-999; b<1000; b++)
        {
            n = num_primes(a, b);
            if(max_n < n)
            {
                max_n = n;
                max_a = a;
                max_b = b;
            }
        }
    }

    printf("\nANSWER = %d\n a = %d, b = %d, n = %d", (max_a * max_b), max_a, max_b, max_n);

    return 0;
}

int init_primes()
{
    int i, j, step;
    int limit=sqrt(MAX_PRIME);

    printf("\nInitializing Primes\n");
    sieve[0]=1;
    sieve[1]=1;
    for(i=2+2; i<MAX_PRIME; i+=2)
    {
        sieve[i]=1;
    }
    for(i=3; i<=limit; i+=2)
    {
        if(!sieve[i])
        {
            step=(2*i);
            for(j=3*i; j<MAX_PRIME; j += step)
            {
                sieve[j]=1;
            }
        }
    }
    printf("\nInitialization Done\n");
    return 0;
}


int num_primes(int a, int b)
{
    int i, n=0;

    while(1)
    {
        i = (n*n) + (a*n) + b;;
        if(i>=MAX_PRIME)
        {
            printf("\n\t!! LIMIT EXCEED !!\n");
            printf("a = %d, \tb = %d, \tn = %d, \ti = %d\n", a, b, n, i);
            return -1;
        }
        if(i>=0)
        {
            if(!sieve[i]) n++;
            else break;
        }else break;
    }

    return n;
}
