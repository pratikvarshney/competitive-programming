var prime = [
  2,
  3
];
// lib
function isPrime(n) {
  var limit = Math.ceil(Math.sqrt(n));
  for (var i = 0; prime[i] <= limit && i < prime.length; i++) {
    if (n % prime[i] == 0) {
      return false;
    }
  }
  for (var i = prime[prime.length - 1] + 2; i <= limit; i += 2) {
    if (isPrime(i)) {
      prime.push(i);
      if (n % i == 0) {
        return false;
      }
    }
  }
  return true;
}
// process

var minRatio = 10;
var answer = 0;
var base = 1;
var offset = 2;
var primeCount = 0,
total = 1;
while (true) {
  for (var i = 0; i < 4; i++) {
    base += offset;
    if (isPrime(base)) {
      primeCount++;
    }
  }
  total += 4;
  if ((100 * primeCount) < (minRatio * total)) {
    answer = (offset + 1);
    break;
  }
  offset += 2;
}
console.log('answer: ' + answer);
// answer: 26241
