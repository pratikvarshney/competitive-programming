#include<stdio.h>
#include<math.h>

#define MAX_PRIME 1000000

_Bool sieve[MAX_PRIME]={0};

int init_primes();

int trunc_ltr(int n);
int trunc_rtl(int n);

int main()
{
    init_primes();

    int i, sum=0;
    for(i=10; i<MAX_PRIME; i++)
    {
        if(!sieve[i] && trunc_ltr(i) && trunc_rtl(i))
        {
            printf("%d\n", i);
            sum += i;
        }
    }
    printf("\n\nsum = %d\n", sum);

    return 0;
}

int init_primes()
{
    int i, j, step;
    int limit=sqrt(MAX_PRIME);

    printf("\nInitializing Primes\n");
    sieve[0]=1;
    sieve[1]=1;
    for(i=2+2; i<MAX_PRIME; i+=2)
    {
        sieve[i]=1;
    }
    for(i=3; i<=limit; i+=2)
    {
        if(!sieve[i])
        {
            step=(2*i);
            for(j=3*i; j<MAX_PRIME; j += step)
            {
                sieve[j]=1;
            }
        }
    }
    printf("\nInitialization Done\n");
    return 0;
}

int trunc_rtl(int n)
{
    n /= 10;
    while(n>0)
    {
        if(sieve[n]) return 0;
        n /= 10;
    }
    return 1;
}

int trunc_ltr(int n)
{
    int div=10;
    while(n>div)
    {
        if(sieve[n%div]) return 0;
        div *= 10;
    }
    return 1;
}
