var limit = 1000000;
var prime = new Array(limit+1);
for(var i=0; i<limit; i++) prime[i]=true;
prime[0]=false;
prime[1]=false;
var crosslimit = Math.ceil(Math.sqrt(limit+1));
for(var i=2; i<=crosslimit; i++)
{
	if(prime[i])
	{
		for(var j=2*i; j<=limit; j+=i)
		{
			prime[j]=false;
		}
	}
}

var p=[];
p.push(2);
for(var i=3; i<=limit; i=i+2)
{
	if(prime[i]) p.push(i);
}

function isFDPF(a)
{
	var count=0;
	for(var i=0; p[i] < a; i++ )
	{
		if(a%p[i]==0) count++;
	}
	if(count==4) return true;
	return false;
}

res=0;
var n=0;
for(var i=10; i<=limit; i++)
{
	if(isFDPF(i))
	{
		n++;
		if(n==4)
		{
			 res = i-3;
			 break;
		}
	}
	else n=0;
}

res;

/* 134043 */