var start = new Date().getTime();
var n = 100;

function expand(remain, maxval) {
	if (remain == 0) {
		return 1;
	}
	var val = 0;
	for (var i = (remain < maxval) ? remain : maxval; i >= 1; i--) {
		val += expand(remain - i, i);
	}
	return val;
}

var answer = expand(n, n - 1);
console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));

// answer: 190569291
// Time (ms): 11896