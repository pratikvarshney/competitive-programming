/* 15/03/2013 */

/*
http://projecteuler.net/problem=1

Multiples of 3 and 5
Problem 1
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
*/

/*

sum = 233168

*/
#include<stdio.h>
int main()
{
    register unsigned int a=3, sum=0;
    do
    {
        if(a%3==0 || a%5==0)
        {
            sum += a;
            printf("\na = %d",a);
        }
        a++;
    }while(a<1000);
    printf("\nsum = %ld\n",sum);
    return 0;
}
