#include<stdio.h>

int main()
{
    long long int num;
    int index;
    int counter;
    int max_count=0;
    int max_num=0;

    for(index=3; index<1000000; index++)
    {
        num=index;
        counter=1;
        while(num!=1)
        {
            num=((num&1)==0)?(num>>1):((3*num)+1);
            counter++;
        }
        if(counter>max_count)
        {
            max_count=counter;
            max_num=index;
        }
        /*if((index%10000)==0)
        {
            printf("\nCURRENT INDEX : %d",index);
            printf("\nTEMP_MAX_NUM : %d",max_num);
            printf("\nTEMP_MAX_COUNT : %d",max_count);
        }*/
    }

    printf("\nMAX_NUM : %d",max_num);
    printf("\nMAX_COUNT : %d",max_count);

    return 0;
}
