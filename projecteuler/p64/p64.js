var start = new Date().getTime();
function getPeriod(n){
var s = Math.sqrt(n);
var p = Math.floor(s);
if((p*p)==n) return 0; // perfect square
var q = 1;
var k;
// asumption : i<n
for(var i=1; ; i++){
if(i>n) console.log('Long period for : '+n);
k = Math.floor(q/(s-p));
q = (n - (p*p))/q;
p = (q * k) - p;
if(q==1){
return i;
}
}
return 0;
}
var count = 0;
var maxVal = 10000;
for(i=2; i<=maxVal; i++){
if(getPeriod(i)%2!=0){
count++;
}
}
var time = (new Date().getTime()) - start;
var answer = count;
console.log(answer);
console.log('Time taken = '+time + ' ms');
