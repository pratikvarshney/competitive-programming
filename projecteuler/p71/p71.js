var start = new Date().getTime();
function gcd(a, b) {
	var c;
	while (a != 0) {
		c = a;
		a = b % a;
		b = c;
	}
	return b;
}

var limit = 1000000;
var resN, resD, res = 0;
var maxval = 3 / 7;
var div;
for (var d = 1; d <= limit; d++) {
	for (var n = Math.floor(maxval * d); n < d; n++) {
		if( gcd(n,d) != 1){
			continue;
		}
		if (n == 3 && d == 7) {
			continue;
		}
		div = n / d;
		if (div > maxval) {
			break;
		}
		if (div > res) {
			resN = n;
			resD = d;
			res = div;
		}
	}
}
console.log(resN + '/' + resD);
var answer = resN;
console.log('answer: '+answer);
console.log('Time (ms): '+( new Date().getTime() - start));

// answer: 428570
// Time (ms): 130