var start = new Date().getTime();

function gcd(a, b) {
	var c;
	while (a != 0) {
		c = a;
		a = b % a;
		b = c;
	}
	return b;
}

var limit = 12000;
var maxval = 1 / 2;
var minval = 1 / 3;
var count = 0;
for (var d = 1; d <= limit; d++) {
	var a = Math.ceil(minval * d);
	var b = Math.floor(maxval * d);
	for (var n = a; n <= b; n++) {
		if (gcd(n, d) == 1) {
			if(n==1 && d==2){
				continue;
			}
			if(n==1 && d==3){
				continue;
			}
			count++;
			//console.log(n + '/' + d);
		}
	}
}
var answer = count;
console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));

// answer: 7295372
// Time (ms): 769