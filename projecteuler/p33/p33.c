#include<stdio.h>

/* Standard C Function: Greatest Common Divisor */
int gcd ( int a, int b )
{
    int c;
    while ( a != 0 )
    {
        c = a;
        a = b%a;
        b = c;
    }
    return b;
}

int main()
{
    int n, d, i;
    int np=1, dp=1;
    for(n=1; n<9; n++)
    {
        for(d=n+1; d<=9; d++)
        {
            for(i=n+1; i<=9; i++)
            {
                if( (((n*10)+i)*d) == (((i*10)+d)*n) )
                {
                    printf("%d / %d , i = %d\n", n, d, i);
                    np *= n;
                    dp *= d;
                }
            }
        }
    }

    printf("\nANSWER = %d", dp/gcd(np, dp));

    return 0;
}
