var start = new Date().getTime();
var limit = 1000000;
var prime = new Array(limit + 1);
for (var i = 0; i < limit; i++) prime[i] = true;
prime[0] = false;
prime[1] = false;
var crosslimit = Math.ceil(Math.sqrt(limit + 1));
for (var i = 2; i <= crosslimit; i++) {
	if (prime[i]) {
		for (var j = 2 * i; j <= limit; j += i) {
			prime[j] = false;
		}
	}
}

var p = [];
p.push(2);
for (var i = 3; i <= limit; i += 2) {
	if (prime[i]) p.push(i);
}


var n = 10;

function expand(remain, maxval) {
	if (remain == 0) {
		return 1;
	}
	if (remain < 2) {
		return 0;
	}
	var val = 0;
	for (var i = 0; p[i] <= remain && p[i] <= maxval; i++) {
		val += expand(remain - p[i], p[i]);
	}
	return val;
}

var maxm = 5000;
var count = 0;
var answer;
for (var n = 10;; n++) {
	count = expand(n, n - 1);
	console.log('n='+n+', count='+count);
	if (count > maxm) {
		answer = n;
		break;
	}
}

console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));

// answer: 71
// Time (ms): 61