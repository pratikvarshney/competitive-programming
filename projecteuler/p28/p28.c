#include<stdio.h>

int main()
{
    int n=1001, i, k, val;
    int n_sqr = n*n;
    int sum = 0;
    k=0;
    val=1;
    while(1)
    {
        //right
        val += k;
        sum += val;
        //terminating condition
        if(val==n_sqr) break;
        val++;

        //down
        k++;
        val += k;
        sum += val;

        //left
        k++;
        val += k;
        sum += val;

        //up
        val += k;
        sum += val;

    }

    printf("%d", sum);

    return 0;
}
