#include<stdio.h>

int is_leap_year(int year);

int main()
{
    int year, month, day;
    int count=0;
    day = 2; //tuesday 1 jan,1901
    for(year=1901; year<=2000; year++)
    {
        for(month=1; month<=12; month++)
        {
            if(day==0)
            {
                printf("Sunday 01/%02d/%04d\n", month, year);
                count++;
            }
            switch(month)
            {
            case 2: //feb
                if(is_leap_year(year)) day = (day+29)%7;
                else day = (day+28)%7;
                break;
            case 4: //april
            case 6: //june
            case 9: //sept
            case 11: //nov
                day = (day+30)%7;
                break;
            default :
                day = (day+31)%7;
                break;
            }
        }
    }
    printf("\nCOUNT : %d", count);
    return 0;
}

int is_leap_year(int year)
{
    if( (year%4==0 && year%100!=0) || (year%100==0 && year%400==0) ) return 1;
    return 0;
}
