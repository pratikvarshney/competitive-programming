var start = new Date().getTime();
var a = document.body.innerText.trim().split('\n');
var b = [];
var c = [];
for (var i = 0; i < 10; i++) {
	b[i] = [];
	c[i] = 0;
}

for (var i = 0; i < a.length; i++) {
	var p = Number(a[i][0]);
	var q = Number(a[i][1]);
	var r = Number(a[i][2]);

	// check for atleast 1 occurance
	if (c[p] == 0) {
		c[p]++;
	}
	if (c[q] == 0) {
		c[q]++;
	}
	if (c[r] == 0) {
		c[r]++;
	}

	if (!b[p].includes(q)) {
		b[p].push(q);
		c[p]++;
	}

	if (!b[p].includes(r)) {
		b[p].push(r);
		c[p]++;
	}

	if (!b[q].includes(r)) {
		b[q].push(r);
		c[q]++;
	}
}

var d = [];
for (var i = 0; i <= 10; i++) {
	if (c[i] > 0) {
		d[c[i] - 1] = i;
	}
}

var answer = d.reverse().join('');
console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));

// answer: 73162890
// Time (ms): 1