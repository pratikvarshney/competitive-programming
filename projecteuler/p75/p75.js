var start = new Date().getTime();
var limit = 1500000;
var count = [];
for (var i = 0; i <= limit; i++) {
	count[i] = 0;
}

// Tree of Pythagorean triples
function processPythagoreanTree(a, b, c) {
	var l = a + b + c;
	if (l <= limit) {
		for (j = l; j <= limit; j += l) {
			count[j]++;
		}
		processPythagoreanTree(a - (2 * b) + (2 * c), (2 * a) - b + (2 * c), (2 * a) - (2 * b) + (3 * c));
		processPythagoreanTree(a + (2 * b) + (2 * c), (2 * a) + b + (2 * c), (2 * a) + (2 * b) + (3 * c));
		processPythagoreanTree(-a + (2 * b) + (2 * c), -(2 * a) + b + (2 * c), -(2 * a) + (2 * b) + (3 * c));
	}
}

processPythagoreanTree(3, 4, 5);

var answer = 0;
for (var i = 0; i <= limit; i++) {
	if (count[i] == 1) {
		answer++;
	}
}

console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));

// answer: 161667
// Time (ms): 95