#include<stdio.h>
#include<math.h>

int main()
{
    int limit=2000000;
    long long sum=0;
    int sievebound = (limit-1)/2;
    _Bool *sieve = (_Bool*)calloc(sievebound+1, sizeof(_Bool));
    int crosslimit = sqrt(limit-1)/2;
    int i, j;
    for(i=1; i<=crosslimit; i++)
    {
        if(!sieve[i])
        {
            for(j=2*i*(i+1); j<=sievebound; j+=(2*i)+1)
            {
                sieve[j]=1;
            }
        }
    }
    sum=2L;
    for(i=1; i<=sievebound; i++)
    {
        if(!sieve[i])
        {
            sum+=((2*i)+1);
        }
    }
    printf("%lld", sum);
    return 0;
}
