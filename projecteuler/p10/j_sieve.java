class j_sieve
{
	public static void main(String[] arr)
	{
		int limit = 2000000;
		long sum = 0;
		int sievebound = (limit-1)/2;
		boolean[] sieve = new boolean[sievebound+1];
		java.util.Arrays.fill(sieve,false);
		int crosslimit = (int)(Math.sqrt(limit-1)/2);
		int i, j;
		for(i=1; i<=crosslimit; i++)
		{
			if(!sieve[i])
			{
				for(j=2*i*(i+1); j<=sievebound; j+=(2*i)+1)
				{
					sieve[j]=true;
				}
			}
		}
		sum=2L;
		for(i=1; i<=sievebound; i++)
		{
			if(!sieve[i])
			{
				sum+=((2*i)+1);
			}
		}
		System.out.println(sum);
	}
}
