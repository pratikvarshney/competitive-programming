class j_prime
{
	public static void main(String[] arr)
	{
		long sum=2;
		for(int i=3; i<2000000; i+=2)
		{
			if(isPrime(i)) sum+=i;
		}
		System.out.println(sum);
	}
	static boolean isPrime(int num)
	{
		if(num%2==0) return false;
		for(int i=3; i<=Math.sqrt(num); i+=2)
		{
			if(num%i==0) return false;
		}
		return true;
	}
}