// init prime
var limit = 1000000;
var prime = new Array(limit + 1);
for (var i = 0; i < limit; i++) prime[i] = true;
prime[0] = false;
prime[1] = false;
var crosslimit = Math.ceil(Math.sqrt(limit + 1));
for (var i = 2; i <= crosslimit; i++)
{
  if (prime[i])
  {
    for (var j = 2 * i; j <= limit; j += i)
    {
      prime[j] = false;
    }
  }
}
var primeArr = [
];
// add largest prime sqrt (in init)
for (var i = crosslimit; i > 0; i--) {
  if (prime[i]) {
    primeArr.push(i);
    break;
  }
}
// lib

function isPrime(n) {
  if (n < limit) return prime[n];
  var sqrt = Math.ceil(Math.sqrt(n));
  for (var i = 0; primeArr[i] <= sqrt && i < primeArr.length; i++) {
    if (n % primeArr[i] == 0) {
      return false;
    }
  }
  for (var i = primeArr[primeArr.length - 1] + 2; i <= sqrt; i += 2) {
    if (isPrime(i)) {
      primeArr.push(i);
      if (n % i == 0) {
        return false;
      }
    }
  }
  return true;
}
// process

function test(i) {
  var str = '' + i;
  if (isPrime(Number('3' + str))) if (isPrime(Number(str + '3'))) if (isPrime(Number('7' + str))) if (isPrime(Number(str + '7'))) if (isPrime(Number('109' + str))) if (isPrime(Number(str + '109'))) if (isPrime(Number('673' + str))) if (isPrime(Number(str + '673'))) return true;
  return false;
}
var answer = 0;
for (var i = 677; ; i += 2) {
  if (isPrime(i)) {
    if (test(i)) {
      answer = i + 3 + 7 + 109 + 673;
      break;
    }
  }
}
console.log('answer: ' + answer);
// answer: 1237
