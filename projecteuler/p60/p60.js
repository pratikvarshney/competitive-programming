var start = new Date().getTime();
// init prime
var limit = 10000;
// with limit = 10000, there are 1229 primes => (1229*1228/2) = 754606 tests (i.e. less than a million tests)
var prime = new Array(limit + 1);
for (var i = 0; i < limit; i++) prime[i] = true;
prime[0] = false;
prime[1] = false;
var crosslimit = Math.ceil(Math.sqrt(limit + 1));
for (var i = 2; i <= crosslimit; i++) {
  if (prime[i]) {
    for (var j = 2 * i; j <= limit; j += i) {
      prime[j] = false;
    }
  }
}
var primeArr = [
  2
];
for (var i = 3; i < limit; i += 2) {
  if (prime[i]) {
    primeArr.push(i);
  }
}
var primeArrInitLength = primeArr.length;
console.log('init done : ' + (new Date().getTime() - start) + ' ms');
function isPrime(n) {
  if (n < limit) return prime[n];
  var sqrt = Math.ceil(Math.sqrt(n));
  for (var i = 0; primeArr[i] <= sqrt && i < primeArr.length; i++) {
    if (n % primeArr[i] == 0) {
      return false;
    }
  }
  for (var i = primeArr[primeArr.length - 1] + 2; i <= sqrt; i += 2) {
    if (isPrime(i)) {
      primeArr.push(i);
      if (n % i == 0) {
        return false;
      }
    }
  }
  return true;
}
// process
// ignoring 2, as aaa2 is never prime

var tested = new Array(primeArrInitLength);
var testcount = 0;
var found = 0;
for (var i = 1; i < primeArrInitLength; i++) {
  for (var j = i + 1; j < primeArrInitLength; j++) {
    if (test(i, j)) {
      for (var k = j + 1; k < primeArrInitLength; k++) {
        if (test(i, k) && test(j, k)) {
          for (var l = k + 1; l < primeArrInitLength; l++) {
            if (test(i, l) && test(j, l) && test(k, l)) {
              for (var m = l + 1; m < primeArrInitLength; m++) {
                if (test(i, m) && test(j, m) && test(k, m) && test(l, m)) {
                  var sum = primeArr[i] + primeArr[j] + primeArr[k] + primeArr[l] + primeArr[m];
                  found++;
                  console.log(sum + ' = ' + primeArr[i] + ' + ' + primeArr[j] + ' + ' + primeArr[k] + ' + ' + primeArr[l] + ' + ' + primeArr[m] + (' (' + (new Date().getTime() - start) + ' ms)'));
                }
              }
            }
          }
        }
      }
    }
  }
}
function test(a, b) {
  if (tested[a] != undefined) {
    if (tested[a][b] != undefined) {
      return tested[a][b];
    }
  } else {
    tested[a] = new Array(primeArrInitLength);
  }
  testcount++;
  if (isPrime(Number(('' + primeArr[a]) + primeArr[b])) && isPrime(Number(('' + primeArr[b]) + primeArr[a]))) {
    tested[a][b] = true;
    return true;
  }
  tested[a][b] = false;
  return false;
}
var end = new Date().getTime();
var time = end - start;
console.log('testcount : ' + testcount);
console.log('found : ' + found);
console.log('Total time taken = ' + time + ' ms');
// -------------------------------------------------------------------
// init done : 56 ms
// 26033 = 13 + 5197 + 5701 + 6733 + 8389 (4267 ms)
// testcount : 753378
// found : 1
// Total time taken = 88977 ms
// -------------------------------------------------------------------
// 34427 = 7 + 1237 + 2341 + 12409 + 18433 (around 25s if limit=20000)
// 26033 = 13 + 5197 + 5701 + 6733 + 8389 (around 50s if limit=20000)
