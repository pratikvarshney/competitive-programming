var start = new Date().getTime();
var primeArr = [
  2,
  3,
  5,
  7,
  11,
  13
];
var prime = new Array(primeArr[primeArr.length - 1] + 1);
for (var i = 0; i <= primeArr[primeArr.length - 1]; i++) {
  prime[i] = false;
}
for (var i = 0; i < primeArr.length; i++) {
  prime[primeArr[i]] = true;
}
function isPrime(n) {
  if (n < prime.length) return prime[n];
  var sqrt = Math.ceil(Math.sqrt(n));
  for (var i = 0; i < primeArr.length; i++) {
    if (primeArr[i] <= sqrt) {
      if (n % primeArr[i] == 0) {
        return false;
      }
    } else {
      break;
    }
  }
  for (var i = primeArr[primeArr.length - 1] + 2; i <= sqrt; i += 2) {
    // as i != 2, i += 2 will work
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}
function addNextPrime() {
  var nextPrime = 0;
  for (var i = primeArr[primeArr.length - 1] + 2; ; i += 2) {
    if (isPrime(i)) {
      nextPrime = i;
      break;
    }
  }
  for (var i = prime.length; i < nextPrime; i++) {
    prime[i] = false;
  }
  prime[nextPrime] = true;
  primeArr.push(nextPrime);
  console.log('added: ' + nextPrime);
}
// for(var i=0; i<1229; i++) addNextPrime();

function test(a, b) {
  var key = getKey(a, b);
  if (tested[key] != undefined) {
    return tested[key];
  }
  testcount++;
  if (isPrime(Number(('' + primeArr[a]) + primeArr[b])) && isPrime(Number(('' + primeArr[b]) + primeArr[a]))) {
    tested[key] = true;
    return true;
  }
  tested[key] = false;
  return false;
}
// key => initial value : (1, 2) = 0 (as we will never check for prime value=2), and a < b

function getKey(a, b) {
if(a>b){
return  getKey(b, a);
}
  return (((b - 2) * (b - 1)) / 2) + (a - 1);
}
// process
// ignoring 2, as aaa2 is never prime

var tested = new Array(10);
var testcount = 0;
var found = 0;
var initialDepth = 1000;
while(primeArr.length<1000){
addNextPrime();
}
function process() {
var firstTime = true;
while(true){
if(primeArr.length > 5){
for (var i = primeArr.length - 1; i > 0; i--) {
    for (var j = i - 1; j > 1; j--) {
      if (test(i, j)) {
        for (var k = j - 1; k > 2; k--) {
          if (test(i, k) && test(j, k)) {
            for (var l = k - 1; l > 3; l--) {
              if (test(i, l) && test(j, l) && test(k, l)) {
                for (var m = l - 1; m > 4; m--) {
                  if (test(i, m) && test(j, m) && test(k, m) && test(l, m)) {
                    var sum = primeArr[i] + primeArr[j] + primeArr[k] + primeArr[l] + primeArr[m];
                    found++;
                    console.log(sum + ' = ' + primeArr[i] + ' + ' + primeArr[j] + ' + ' + primeArr[k] + ' + ' + primeArr[l] + ' + ' + primeArr[m] + (' (' + (new Date().getTime() - start) + ' ms)'));
                    return;
                  }
                }
              } 
            }
          }
        }
      } 
    }
  }
if(!firstTime){
break;
}
}
firstTime = false;
addNextPrime();
}
}
process();
var end = new Date().getTime();
var time = end - start;
console.log('testcount : ' + testcount);
console.log('found : ' + found);
console.log('Total time taken = ' + time + ' ms');
// -------------------------------------------------------------------
// init done : 59 ms
// 26033 = 13 + 5197 + 5701 + 6733 + 8389 (3065 ms)
// testcount : 27598
// found : 1
// Total time taken = 3065 ms
// -------------------------------------------------------------------
// 34427 = 7 + 1237 + 2341 + 12409 + 18433 (around 25s if limit=20000)
// 26033 = 13 + 5197 + 5701 + 6733 + 8389 (around 50s if limit=20000)
