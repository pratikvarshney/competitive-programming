import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class p66 {

	public static void main(String[] args) {
		int maxVal = 1000;

		boolean[] isPerfectSquare = new boolean[maxVal + 1];
		for (int i = 0;; i++) {
			int square = i * i;
			if (square <= maxVal) {
				isPerfectSquare[square] = true;
			} else {
				break;
			}
		}

		BigInteger maxX = BigInteger.ZERO;
		int maxD = 0;
		for(int d=2; d<=maxVal; d++){
			if(!isPerfectSquare[d]){
				BigInteger x = getValueOfX(d);
				if(x.compareTo(maxX)>0){
					maxX = x;
					maxD = d;
				}
			}
		}
		System.out.println("maxX = "+maxX);
		System.out.println("maxD = "+maxD);
		// maxX = 16421658242965910275055840472270471049
		// maxD = 661

	}

	private static BigInteger getValueOfX(int intD) {
		String stringD = String.valueOf(intD);
		double doubleD = Double.valueOf(stringD);

		// init
		BigInteger D = new BigInteger(stringD);
		BigDecimal sqrtD = new BigDecimal(Math.sqrt(doubleD));
		// System.out.println(sqrtD+"\n");

		BigInteger P0 = BigInteger.ZERO;
		BigInteger Q0 = BigInteger.ONE;

		BigInteger Ai_2 = BigInteger.ZERO;
		BigInteger Ai_1 = BigInteger.ONE;
		BigInteger Bi_2 = BigInteger.ONE;
		BigInteger Bi_1 = BigInteger.ZERO;

		BigInteger Gi_2 = P0.negate();
		BigInteger Gi_1 = Q0;

		int i = 0;
		BigInteger Pi = P0;
		BigInteger Qi = Q0;

		BigInteger ai_1 = null, Qi_1 = null, Pi_1 = null;

		boolean negative = false;
		BigInteger minusOne = BigInteger.ONE.negate();

		// System.out.println("i\tPi\tQi\tai\tAi\tBi\tGi\n");

		while (true) {

			if (i >= 1) {
				Pi = ai_1.multiply(Qi_1).subtract(Pi_1);
				Qi = D.subtract(Pi.multiply(Pi)).divide(Qi_1);
				if (Qi.multiply(Q0).compareTo((negative) ? minusOne : BigInteger.ONE) == 0) {
					// solution is in i-1;
					System.out.println("For d = " + stringD + ", i = " + i + ", x = " + Gi_1.toString());
					return Gi_1;
				}
			}

			BigInteger ai = (new BigDecimal(Pi)).add(sqrtD).divide(new BigDecimal(Qi), RoundingMode.FLOOR)
					.toBigInteger();
			BigInteger Ai = ai.multiply(Ai_1).add(Ai_2);
			BigInteger Bi = ai.multiply(Bi_1).add(Bi_2);
			BigInteger Gi = ai.multiply(Gi_1).add(Gi_2);

			// System.out.println(i+"\t"+Pi+"\t"+Qi+"\t"+ai+"\t"+Ai+"\t"+Bi+"\t"+Gi+"\n");

			negative = (negative) ? false : true; // toggle
			// next i
			i++;

			Ai_2 = Ai_1;
			Bi_2 = Bi_1;
			Gi_2 = Gi_1;

			Ai_1 = Ai;
			Bi_1 = Bi;
			Gi_1 = Gi;

			ai_1 = ai;
			Pi_1 = Pi;
			Qi_1 = Qi;
		}
	}

}
