var start = new Date().getTime();
// init
var maxD = 1000;
var squares = [
];
var limit = Math.floor(Math.sqrt(maxD));
for (var i = 2; i <= limit; i++) {
  squares.push(i * i);
}
// process

var maxX = 0;
var j = 0;
for (var d = 2; d <= maxD; d++) {
  var x = 0;
  if (squares[j] == d) {
    // skip
    j++;
  } else {
    // test
    console.log('d=' + d);
    for (var k = 1; ; k++) {

      // check perfect square
      if (Number.isInteger(Math.sqrt( k * ( (k*d) -2 ) ))) {
	x = (k * d) - 1;
        break;
      }
      
      // check perfect square
      if (Number.isInteger(Math.sqrt( k * ( (k*d) +2 ) ))) {
	x = (k * d) + 1;
        break;
      }
    }
    if (maxX < x) {
      maxX = x;
    }
  }
}
console.log(maxX);
// answer
var time = (new Date().getTime()) - start;
