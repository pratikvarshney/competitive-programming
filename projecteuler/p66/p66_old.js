// init
var maxD = 1000;
var squares = [
];
var limit = Math.floor(Math.sqrt(maxD));
for (var i = 2; i <= limit; i++) {
  squares.push(i * i);
}
// process

var maxX = 0;
var j = 0;
for (var d = 2; d <= maxD; d++) {
  var x = 0;
  if (squares[j] == d) {
    // skip
    j++;
  } else {
    // test
    console.log('d=' + d);
    for (var y = 1; ; y++) {
      var k = 1 + (d * y * y);
      if (k > Number.MAX_SAFE_INTEGER) {
        console.log('limit exceed : y=' + y);
        x = 0;
        break;
      }
      x = Math.sqrt(k);
      // check perfect square
      if (Number.isInteger(x)) {
        break;
      }
    }
    if (maxX < x) {
      maxX = x;
    }
  }
}
console.log(maxX);
// answer
