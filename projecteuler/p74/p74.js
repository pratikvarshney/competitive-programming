var start = new Date().getTime();
var factorial = [];
for (var i = 0; i < 10; i++) {
	factorial[i] = 1;
	for (var j = 2; j <= i; j++) {
		factorial[i] *= j;
	}
}

var limit = 1000000;
var count = 0;
var terms = 60;
function next(n){
	var str = n.toString();
	var sum = 0;
	for(var i=0; i<str.length; i++){
		sum += factorial[str[i]];
	}
	return sum;
}

for(var i=1; i<limit; i++){
	var list = [i];
	var j = i;
	for(;;){
		j = next(j);
		if(list.includes(j)){
			break;
		}
		list.push(j);
	}
	if(list.length == terms){
		count++;
	}
}
var answer = count;
console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));

// answer: 402
// Time (ms): 11143