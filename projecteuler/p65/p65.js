var start = new Date().getTime();
var maxExpansion = 100;
// lib
function getSum(str){
var sum = 0;
var arr = str.split('');
for(var i=0; i<arr.length;i++){
sum+=Number(arr[i]);
}
return sum;
}
function addN(fraction, n) {
  if (fraction[0] == 0) {
    return [n.toString(),
    '1'];
  } else {
    return [addStr(fraction[0], multiply(fraction[1], n)),
    fraction[1]];
  }
}
function reciprocal(fraction) {
  return [fraction[1],
  fraction[0]];
}
function multiply(s, n) {
  if (n == 1) return s;
  var res = s.split('').reverse();
  var extra = 0;
  var val;
  var len = s.length;
  for (var i = 0; i < len; i++) {
    val = (res[i] * n) + extra;
    res[i] = val % 10;
    extra = (val - res[i]) / 10;
  }
  while (extra > 0) {
    res[len] = extra % 10;
    extra = (extra - res[len]) / 10;
    len++;
  }
  return res.reverse().join('');
}
function addStr(a, b) {
  var longStr,
  shortStr;
  if (a.length > b.length) {
    longStr = a.split('').reverse();
    shortStr = b.split('').reverse();
  } else {
    longStr = b.split('').reverse();
    shortStr = a.split('').reverse();
  }
  var extra = 0;
  var val;
  for (var i = 0; i < shortStr.length; i++) {
    val = Number(shortStr[i]) + Number(longStr[i]) + extra;
    longStr[i] = val % 10;
    extra = (val - longStr[i]) / 10;
  }
  for (var i = shortStr.length; i < longStr.length; i++) {
    val = Number(longStr[i]) + extra;
    longStr[i] = val % 10;
    extra = (val - longStr[i]) / 10;
  }
  var len = longStr.length;
  while (extra > 0) {
    longStr[len] = extra % 10;
    extra = (extra - longStr[len]) / 10;
    len++;
  }
  return longStr.reverse().join('');
}


// process
var integralPart = new Array(maxExpansion);
integralPart[0]=2;
integralPart[1]=1;
integralPart[2]=2;
integralPart[3]=1;
integralPart[4]=1;
for(var i=5; i<maxExpansion; i++){
integralPart[i] = (integralPart[i-3]==1)?1:(integralPart[i-3]+2);
}
// process
var frac = [0, 0];
for(var i=maxExpansion-1; i>=0; i--){
frac = addN(reciprocal(frac), integralPart[i]);
}
// get result
var answer = getSum(frac[0]);
console.log('answer: ' + answer);

var end = new Date().getTime();
var time = end - start;
console.log('time: ' + time + ' ms');
// answer: 272
