class p36
{
	public static void main(String arg[])
	{
		int i, sum;
		sum=0;
		for(i=1; i<1000000; i++)
		{
			if(isPalindrome(i) && isPalindromeBase2(i))
			{
				sum += i;
				System.out.println(i);
			}
		}
		System.out.println("\n\nsum = "+sum);
	}
	
	public static boolean isPalindrome(int n)
	{
		String str=String.valueOf(n);
		for(int i=0,j=(str.length()-1); i<j; i++,j--)
		{
			if(str.charAt(i)!=str.charAt(j)) return false;
		}
		return true;
	}
	public static boolean isPalindromeBase2(int n)
	{
		int len;
		boolean[] arr=new boolean[32];
		len=0;
		while(n>0)
		{
			arr[len]=((n&1)==1);
			len++;
			n = n >>> 1;
		}
		for(int i=0,j=len-1; i<j; i++,j--)
		{
			if(arr[i]!=arr[j]) return false;
		}
		return true;
	}
}