var start = new Date().getTime();

// process
var maxExpansion = 1000;
var res = new Array(maxExpansion + 1);
res[0] = [
  '1',
  '1'
];
expand(0, maxExpansion);
// get result
var count = 0;
for (var i = 1; i <= maxExpansion; i++) {
  if (isNumeratorMoreThanDenominator(res[i])) {
    count++;
  }
}
var end = new Date().getTime();
var time = end - start;
var answer = count;
alert(answer);
// lib
function expand(expansion, maxExpansion) {
  if (expansion < maxExpansion) {
    var recip = reciprocal(addN(expand(expansion + 1, maxExpansion), 2));
    res[maxExpansion - expansion] = addN(recip, 1);
    return recip;
  }
  return [
  0,
  0
  ];
}
function addN(fraction, n) {
  if (fraction[0] == 0) {
    return [n.toString(),
    '1'];
  } else {
    return [addStr(fraction[0], multiply(fraction[1], n)),
    fraction[1]];
  }
}
function reciprocal(fraction) {
  return [fraction[1],
  fraction[0]];
}
function isNumeratorMoreThanDenominator(frac) {
  return (frac[0].length > frac[1].length);
}
function multiply(s, n) {
  if (n == 1) return s;
  var res = s.split('').reverse();
  var extra = 0;
  var val;
  var len = s.length;
  for (var i = 0; i < len; i++) {
    val = (res[i] * n) + extra;
    res[i] = val % 10;
    extra = (val - res[i]) / 10;
  }
  while (extra > 0) {
    res[len] = extra % 10;
    extra = (extra - res[len]) / 10;
    len++;
  }
  return res.reverse().join('');
}
function addStr(a, b) {
  var longStr,
  shortStr;
  if (a.length > b.length) {
    longStr = a.split('').reverse();
    shortStr = b.split('').reverse();
  } else {
    longStr = b.split('').reverse();
    shortStr = a.split('').reverse();
  }
  var extra = 0;
  var val;
  for (var i = 0; i < shortStr.length; i++) {
    val = Number(shortStr[i]) + Number(longStr[i]) + extra;
    longStr[i] = val % 10;
    extra = (val - longStr[i]) / 10;
  }
  for (var i = shortStr.length; i < longStr.length; i++) {
    val = Number(longStr[i]) + extra;
    longStr[i] = val % 10;
    extra = (val - longStr[i]) / 10;
  }
  var len = longStr.length;
  while (extra > 0) {
    longStr[len] = extra % 10;
    extra = (extra - longStr[len]) / 10;
    len++;
  }
  return longStr.reverse().join('');
}
// answer = 153
