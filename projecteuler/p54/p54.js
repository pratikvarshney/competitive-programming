// ORDER: 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace
var order = [
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'T',
  'J',
  'Q',
  'K',
  'A'
];
// create card object
function getCard(str) {
  return {
    'value': order.indexOf(str.charAt(0)),
    'suit': str.charAt(1),
    'show': str
  };
}
// swap

function swap(a, i, j) {
  var temp = a[i];
  a[i] = a[j];
  a[j] = temp;
}
// to sort in ascending order

function compare(a, b) {
  if (a.value > b.value) {
    return 1;
  } else if (a.value < b.value) {
    return - 1;
  } else {
    return 0;
  }
}
function tieBreaker(a, b) {
  for (var i = 4; i >= 0; i--) {
    console.log('check ' + (4 - i) + ' : ' + a[i].show + ', ' + b[i].show);
    if (a[i].value > b[i].value) {
      return 1;
    }
    if (b[i].value > a[i].value) {
      return 2;
    }
  }
  return 0;
}
function winner(a, b) {
  a.sort(compare);
  console.log('Player 1 : ' + getAll(a));
  b.sort(compare);
  console.log('Player 2 : ' + getAll(b));
  var funHandle = [
    royalFlush,
    straightFlush,
    fourOfAKind,
    fullHouse,
    flush,
    straight,
    threeOfAKind,
    twoPairs,
    onePair
  ];
  for (var i = 0; i < funHandle.length; i++) {
    var testFunction = funHandle[i];
    var aPass = testFunction(a);
    var bPass = testFunction(b);
    if (aPass && bPass) {
      console.log('Player 1 : ' + testFunction.prototype.constructor.name + ' : ' + getAll(a));
      console.log('Player 2 : ' + testFunction.prototype.constructor.name + ' : ' + getAll(b));
      return tieBreaker(a, b);
    }
    if (aPass) {
      console.log('Player 1 : ' + testFunction.prototype.constructor.name + ' : ' + getAll(a));
      return 1;
    }
    if (bPass) {
      console.log('Player 2 : ' + testFunction.prototype.constructor.name + ' : ' + getAll(b));
      return 2;
    }
  }
  return tieBreaker(a, b);
}
// Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.

function royalFlush(a) {
  var suit = a[0].suit;
  var tVal = order.indexOf('T');
  for (var i = 0; i < 5; i++) {
    if ((a[i].suit != suit) || (a[i].value != (tVal + i))) {
      return false;
    }
  }
  return true;
}
// Straight Flush: All cards are consecutive values of same suit.

function straightFlush(a) {
  var suit = a[0].suit;
  var minVal = a[0].value;
  for (var i = 1; i < 5; i++) {
    if ((a[i].suit != suit) || (a[i].value != (minVal + i))) {
      return false;
    }
  }
  return true;
}
// Four of a Kind: Four cards of the same value.

function fourOfAKind(a) {
  if ((a[0].value == a[1].value) && (a[0].value == a[2].value) && (a[0].value == a[3].value)) {
    // move high priority value to right
    swap(a, 0, 4);
    return true;
  } else if ((a[1].value == a[2].value) && (a[1].value == a[3].value) && (a[1].value == a[4].value)) {
    return true;
  }
  return false;
}
// Full House: Three of a kind and a pair.

function fullHouse(a) {
  if ((a[0].value == a[1].value) && (a[0].value == a[2].value)) {
    if (a[3].value == a[4].value) {
      swap(a, 0, 4);
      swap(a, 1, 3);
      return true;
    }
  }
  if ((a[2].value == a[3].value) && (a[2].value == a[4].value)) {
    if (a[0].value == a[1].value) {
      return true;
    }
  }
  return false;
}
// Flush: All cards of the same suit.

function flush(a) {
  var suit = a[0].suit;
  for (var i = 1; i < 5; i++) {
    if (a[i].suit != suit) {
      return false;
    }
  }
  return true;
}
// Straight: All cards are consecutive values.

function straight(a) {
  var minVal = a[0].value;
  for (var i = 1; i < 5; i++) {
    if (a[i].value != (minVal + i)) {
      return false;
    }
  }
  return true;
}
// Three of a Kind: Three cards of the same value.

function threeOfAKind(a) {
  if ((a[0].value == a[1].value) && (a[0].value == a[2].value)) {
    swap(a, 0, 4);
    swap(a, 1, 3);
    return true;
  }
  if ((a[1].value == a[2].value) && (a[1].value == a[3].value)) {
    swap(a, 1, 4);
    return true;
  }
  if ((a[2].value == a[3].value) && (a[2].value == a[4].value)) {
    return true;
  }
  return false;
}
// Two Pairs: Two different pairs.

function twoPairs(a) {
  if ((a[0].value == a[1].value) && (a[2].value == a[3].value)) {
    swap(a, 4, 2);
    swap(a, 2, 0);
    return true;
  }
  if ((a[0].value == a[1].value) && (a[3].value == a[4].value)) {
    swap(a, 2, 0);
    return true;
  }
  if ((a[1].value == a[2].value) && (a[3].value == a[4].value)) {
    return true;
  }
  return false;
}
// One Pair: Two cards of the same value.

function onePair(a) {
  for (var i = 1; i < 5; i++) {
    if (a[i].value == a[i - 1].value) {
      swap(a, i, 4);
      swap(a, i - 1, 3);
      return true;
    }
  }
  return false;
}
// High Card: Highest value card.

function highCard(a) {
  return a[4];
}
// process

function getWinner(input) {
  console.log('Input : ' + input);
  var arr = input.split(' ');
  var player1 = new Array(5);
  var player2 = new Array(5);
  for (var i = 0; i < 5; i++) {
    player1[i] = getCard(arr[i]);
  }
  for (var i = 0; i < 5; i++) {
    player2[i] = getCard(arr[i + 5]);
  }
  var win = winner(player1, player2);
  console.log('Winner : ' + win);
  return win;
}
// get all cards

function getAll(a) {
  var string = a[0].show;
  for (var i = 1; i < 5; i++) {
    string = string + ' ' + a[i].show;
  }
  return string;
}


function process(input){
  var winCount = [0, 0, 0];
  var rows = input.split('\n');
  for(var i=0; i<rows.length; i++){
    var win = getWinner(rows[i]);
    winCount[win]++;
  }
  alert(winCount[1]);
}

// answer = 376