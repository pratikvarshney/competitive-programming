// ORDER: 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace
var card=["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"];
var index=new Object();
for(var i=0; i<card.length; i++){
	index[card[i]]=i;
}

// Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
function royalFlush(a){
	var suit=a[0].charAt(1);
	if((a.indexOf("T"+suit)>=0) && (a.indexOf("J"+suit)>=0) && (a.indexOf("Q"+suit)>=0) && (a.indexOf("K"+suit)>=0) && (a.indexOf("A"+suit)>=0)){
		// sort highest to lowest as true
		for(var i=0; i<5; i++){
			a[i]=card[12-i]+suit;
		}
		return true;
	}
	return false;
}

// Straight Flush: All cards are consecutive values of same suit.
function straightFlush(a){
	var suit=a[0].charAt(1);
	var min=index[a[0].charAt(0)];
	for(var i=1; i<5; i++){
		if(min>index[a[i].charAt(0)]){
			min=index[a[i].charAt(0)];
		}
	}
	if(min<9){
		if((a.indexOf(card[min]+suit)>=0) && (a.indexOf(card[min+1]+suit)>=0) && (a.indexOf(card[min+2]+suit)>=0) && (a.indexOf(card[min+3]+suit)>=0) && (a.indexOf(card[min+4]+suit)>=0)){
			// sort
			for(var i=0; i<5; i++){
				a[i]=card[min+(4-i)]+suit;
			}
			return true;
		}
	}
	return false;
}

// Four of a Kind: Four cards of the same value.
function fourOfAKind(a){
	var count=new Object();
	for(var i=0; i<card.length; i++){
		count[card[i]]=0;
	}
	for(var i=0; i<5; i++){
		var ch=a[i].charAt(0);
		count[ch]++;
		if(count[ch]==4){
			// sort
			if(i==4){
				for(var j=0; j<4; j++){
					if(ch!=a[j].charAt(0)){
						swap(a, 4, j);
						break;
					}
				}
			}
			return true;
		}
	}
	return false;
}

// Full House: Three of a kind and a pair.
function fullHouse(a){
	var count=new Object();
	for(var i=0; i<card.length; i++){
		count[card[i]]=0;
	}
	for(var i=0; i<5; i++){
		count[a[i].charAt(0)]++;
		if(count[a[i].charAt(0)]==3){
			// check pair for remaining cards
			var remaining=[];
			var threeOfAKindArr=[];
			for(var j=0; j<=i; j++){
				if(a[i].charAt(0)!=a[j].charAt(0)){
					remaining.push(a[j]);
				}else{
					threeOfAKindArr.push(a[j]);
				}
			}
			for(var j=i+1; j<5; j++){
				remaining.push(a[j]);
			}
			if(remaining[0].charAt(0)==remaining[1].charAt(0)){
				// sort
				a[0]=threeOfAKindArr[0];
				a[1]=threeOfAKindArr[1];
				a[2]=threeOfAKindArr[2];
				a[3]=remaining[0];
				a[4]=remaining[1];
				return true;
			}
			return false;
		}
	}
	return false;
}

// Flush: All cards of the same suit.
function flush(a){
	var suit=a[0].charAt(1);
	if((suit==a[1].charAt(1)) && (suit==a[2].charAt(1)) && (suit==a[3].charAt(1)) && (suit==a[4].charAt(1))){
		// sort
		var b=[];
		for(var i=0; i<5; i++){
			b.push(index[a[i].charAt(0)]);
		}
		b=b.sort().reverse();
		for(var i=0; i<5; i++){
			a[i]=card[b[i]]+suit;
		}
		return true;
	}
	return false;
}

// Straight: All cards are consecutive values.
function straight(a){
	var count=new Object();
	var b=new Array(card.length);
	for(var i=0; i<card.length; i++){
		count[card[i]]=0;
	}
	for(var i=0; i<5; i++){
		count[a[i].charAt(0)]++;
		b[index[a[i].charAt(0)]]=a[i];
	}
	for(var i=0; i<9; i++){
		if((count[card[i]]==1) && (count[card[i+1]]==1) && (count[card[i+2]]==1) && (count[card[i+3]]==1) && (count[card[i+4]]==1)){
			// sort
			for(var j=0; j<5; j++){
				a[j]=b[i+(4-j)];
			}
			return true;
		}
	}
	return false;
}

// Three of a Kind: Three cards of the same value.
function threeOfAKind(a){
	var count=new Object();
	for(var i=0; i<card.length; i++){
		count[card[i]]=0;
	}
	for(var i=0; i<5; i++){
		var ch = a[i].charAt(0);
		count[ch]++;
		if(count[ch]==3){
			// sort
			var right=4;
			for(var j=0; j<3; j++){
				if(ch!=a[j].charAt(0)){
					swap(a, right, j);
					right--;
				}
			}
			if(score[a[4].charAt(0)]>score[a[3].charAt(0)]){
				swap(a, 3, 4);
			}
			return true;
		}
	}
	return false;
}

// Two Pairs: Two different pairs.
function twoPairs(a){
	var count=new Object();
	for(var i=0; i<card.length; i++){
		count[card[i]]=0;
	}
	for(var i=0; i<5; i++){
		count[a[i].charAt(0)]++;
	}
	var pairs=0;
	for(var i=0; i<card.length; i++){
		if(count[card[i]]>1){
			pairs++;
		}
	}
	return (pairs==2);
}

// One Pair: Two cards of the same value.
function onePair(a){
	var count=new Object();
	for(var i=0; i<card.length; i++){
		count[card[i]]=0;
	}
	for(var i=0; i<5; i++){
		count[a[i].charAt(0)]++;
		if(count[a[i].charAt(0)]==2){
			return true;
		}
	}
	return false;
}

// High Card: Highest value card.
function highCard(a){
	a.sort(compare);
	return a[0];
}

function swap(a, i, j){
	var temp = a[i];
	a[i] = a[j];
	a[j] = temp;
}

// to sort in descending order
function compare(a,b) {
	if (index[a.charAt(0)] < index[b.charAt(0)]){
		return 1;
	} else if (index[a.charAt(0)] > index[b.charAt(0)]){
		return -1;
	} else {
		return 0;
	}
}