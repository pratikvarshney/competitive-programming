function multiply(s, n) {
  var res = s.split('').reverse();
  var extra = 0;
  var val;
  var len = s.length;
  for (var i = 0; i < len; i++) {
    val = (res[i] * n) + extra;
    res[i] = val % 10;
    extra = (val - res[i]) / 10;
  }
  while (extra > 0) {
    res[len] = extra % 10;
    extra = (extra - res[len]) / 10;
    len++;
  }
  return res.reverse().join('');
}
var maxVal = 100;
var pow = [
];
for (var a = 0; a < maxVal; a++) {
  for (var b = 0; b < maxVal; b++) {
    if (b == 0) {
      pow[a] = [
      ];
      pow[a][0] = '1';
    } else {
      pow[a][b] = multiply(pow[a][b - 1], a);
    }
  }
}
var digitalSum = [
];
var max = 0;
for (var a = 0; a < maxVal; a++) {
  for (var b = 0; b < maxVal; b++) {
    if (b == 0) {
      digitalSum[a] = [
      ];
    }
    digitalSum[a][b] = sum(pow[a][b]);
    if (max < digitalSum[a][b]) {
      max = digitalSum[a][b];
    }
  }
}
function sum(a) {
  var s = 0;
  for (var i = 0; i < a.length; i++) {
    s += Number(a.charAt(i));
  }
  return s;
}
max;
// answer = 972
