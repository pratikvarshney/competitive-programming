function d(n)
{
  var input = Number(n);
  var max=Math.floor(Math.sqrt(input));
  if(input == 0) return 0;
  if(max == 1) return 1;
  var sum=1;
  if(input%max == 0)
    {
      sum += max;
      if((input/max)!=max) sum += (input/max);
    }
  max--;
  for(var i=max; i>1; i--)
    {
      if(input%i == 0)
        {
          sum += i;
          sum += (input/i);
        }
    }
  return sum;
}

sum=0;
for(var i=1; i<10000; i++)
  {
    var a=d(i);
    if(a>i)
      {
        if(d(a) == i)
          {
            sum += i;
            sum += a;
          }
      }
  }
alert(sum);
/* 31626 */