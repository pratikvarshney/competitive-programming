#include<stdio.h>
#include<math.h>

int main()
{
    int a, b, c;
    int p, maxm=0, i;
    int sol[1001]={0};

    for(a=1; a<1000; a++)
    {
        for(b=a+1; b<1000; b++)
        {
            i = (a*a) + (b*b);
            c = sqrt(i);
            if((i == (c*c)) && (a+b+c)<=1000)
            {
                sol[a+b+c]++;
            }
        }
    }

    for(p=1; p<=1000; p++)
    {
        if(sol[maxm] < sol[p]) maxm = p;
    }

    printf("%d (num solutions = %d)", maxm, sol[maxm]);

    return 0;
}
