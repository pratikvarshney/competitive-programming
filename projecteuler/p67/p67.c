#include<stdio.h>
#include<malloc.h>
int main()
{
    int arr_size = 100;
    int** arr;
    int i, j;
    FILE* fp = fopen("p067_triangle.txt","r");
    arr = (int**) malloc((arr_size)*sizeof(int*));
    for(i=0; i<arr_size; i++){
	arr[i] = (int*) malloc((i+1)*sizeof(int));
	for(j=0; j<=i; j++){
	    fscanf(fp, "%d", &(arr[i][j]));
            //printf("%d ", arr[i][j]);
	}
	//printf("\n");
    }

    for(i=arr_size-2; i>=0; i--)
    {
        for(j=0; j<=i; j++)
        {
            arr[i][j] += (arr[i+1][j] > arr[i+1][j+1])?arr[i+1][j]:arr[i+1][j+1];
        }
    }
    printf("%d\n",arr[0][0]);

    fclose(fp);
    return 0;
}
