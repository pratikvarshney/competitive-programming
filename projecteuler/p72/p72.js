var start = new Date().getTime();
var maxD = 1000000;
var limit = 3163; // sqrt d
var prime = new Array(limit + 1);
for (var i = 0; i < limit; i++) prime[i] = true;
prime[0] = false;
prime[1] = false;
var crosslimit = Math.ceil(Math.sqrt(limit + 1));
for (var i = 2; i <= crosslimit; i++) {
	if (prime[i]) {
		for (var j = 2 * i; j <= limit; j += i) {
			prime[j] = false;
		}
	}
}

var p = [];
p.push(2);
for (var i = 3; i <= limit; i += 2) {
	if (prime[i]) p.push(i);
}

function getPrimeFactors(n) {
	var factors = [];
	for (var i = 0; i < p.length && p[i] <= n; i++) {
		if (n % p[i] == 0) {
			factors.push(p[i]);
			n /= p[i];
			while (n % p[i] == 0) {
				n /= p[i];
			}
		}
		if (n <= 1) {
			break;
		}
	}
	if (n > 1) {
		factors.push(n);
	}
	return factors;
}

function totient(n) {
	var factors = getPrimeFactors(n);
	var numerator = 1;
	var denominator = 1;
	for (var i = 0; i < factors.length; i++) {
		denominator *= factors[i];
		numerator *= (factors[i] - 1);
	}
	return (n * numerator) / denominator;
}

var sum = 0;
for (var d = 2; d <= maxD; d++) {
	sum += totient(d);
}
var answer = sum;
console.log('answer: '+answer);
console.log('Time (ms): '+( new Date().getTime() - start));

// answer: 303963552391
// Time (ms): 1372