// init prime
var limit = 1000000;
var prime = new Array(limit+1);
for(var i=0; i<limit; i++) prime[i]=true;
prime[0]=false;
prime[1]=false;
var crosslimit = Math.ceil(Math.sqrt(limit+1));
for(var i=2; i<=crosslimit; i++)
{
	if(prime[i])
	{
		for(var j=2*i; j<=limit; j+=i)
		{
			prime[j]=false;
		}
	}
}

console.log("init done");

// process

for(var i=3; i<=limit; i=i+2)
{
	if(prime[i]){
		if(fcheck(i)){
			console.log(i);
			break;		
		}
	}
}

var r=[/0/g, /1/g, /2/g, /3/g, /4/g, /5/g, /6/g, /7/g, /8/g, /9/g];
function fcheck(num){
	var str=""+num;
	var checked=[false,false,false,false,false,false,false,false,false,false];
	for(var i=0; i<str.length; i++){
		j=Number(str.charAt(i));
		if(!checked[j]){
			checked[j]=true;
			var count=0;
			var fail=0;
			for(var k=0; k<=9; k++){
				if(!(i==0 && k==0)){
					if(prime[Number(str.replace(r[j], k))]){
						count++;
					}else{
						fail++;
					}
				}else{
					fail++;
				}
				if(fail>2){
					break;
				}
			}
			if(count==8){
				return true;
			}
		}
	}
	return false;
}

// answer = 121313