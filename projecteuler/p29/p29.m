n=100;
a=zeros(n,n);
for i=1:n
    for j=1:n
        a(i,j)=i^j;
    end
end
a(1,:)=[];
a(:,1)=[];
disp(length(unique(a)));

% answer = 9183