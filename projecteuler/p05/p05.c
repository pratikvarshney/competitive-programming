/* START    :   17/02/2014 */
/* END      :   17/02/2014 */

/*
http://projecteuler.net/problem=5

Smallest multiple
Problem 5
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


*/

/*

ANS = 232792560 = (2520 * 11 * 13 * 2 * 17 * 19)

where 2520 = (2*5  *  3*3  *  2*2  *  7)

*/

#include<stdio.h>
#include<math.h>

struct factors
{
    int value;
    int largest_count;
    struct factors *next;
};

struct factors *get_factors(int num);

int main()
{
    int num=20, found;
    unsigned long output_val;
    struct factors *factor_list, *curr, *temp, *head;
    //printf("\nRUNNING...\n");
    head = (struct factors *)malloc(sizeof(struct factors));
    head->value=1;
    head->next=NULL;

    while(num>1)
    {
        factor_list = get_factors(num);

        curr = factor_list->next;
        while(curr!=NULL)
        {
            temp = head;
            found=0;
            while(temp->next!=NULL)
            {
                if(temp->next->value==curr->value)
                {
                    found = 1;
                    temp->next->largest_count = ((temp->next->largest_count)>(curr->largest_count))?(temp->next->largest_count):(curr->largest_count);
                }
                temp = temp->next;
            }
            if(!found)
            {
                temp->next = (struct factors *)malloc(sizeof(struct factors));
                temp = temp->next;
                temp->next = NULL;
                temp->value = curr->value;
                temp->largest_count = curr->largest_count;
            }
            curr=curr->next;
        }

        num--;
    }

    printf("\n\nFACTOR LIST:\n");
    output_val = 1L;
    curr = head->next;
    while(curr!=NULL)
    {
        printf("\n%3d ^ %d", curr->value, curr->largest_count);
        output_val *= powl(curr->value, curr->largest_count);
        curr=curr->next;
    }

    printf("\n\nRESULT = %ld", output_val);

    return 0;
}

struct factors *get_factors(int num)
{
    int a = 3;
    struct factors *factor_list, *curr;
    factor_list = (struct factors *)malloc(sizeof(struct factors));
    factor_list->next=NULL;
    curr = factor_list;
    curr->value=1;
    if(num%2==0){
        curr->next = (struct factors *)malloc(sizeof(struct factors));
        curr = curr->next;
        curr->next=NULL;
        curr->value=2;
        curr->largest_count=0;
    }
    while(num%2==0)
    {
        curr->largest_count++;
        num = num/2; /* or num>>1 */
        //printf("\n%d",2);
    }
    while(num>=a)
    {
        if(num%a==0)
        {
            if(a!=curr->value)
            {
                curr->next = (struct factors *)malloc(sizeof(struct factors));
                curr = curr->next;
                curr->next=NULL;
                curr->value=a;
                curr->largest_count=0;
            }
            curr->largest_count++;
            num = num/a;
            //printf("\n%d",a);
        }
        else a+=2;
    }
    return factor_list;
}
