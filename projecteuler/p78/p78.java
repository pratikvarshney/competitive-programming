import java.math.BigInteger;
import java.io.*;
class p78 {
	public static BigInteger[] cache;
	
	public static void main(String[] arr) {
		long start = System.currentTimeMillis();
		cache = new BigInteger[1000000];
		cache[0] = BigInteger.ONE;
		BigInteger million = new BigInteger("1000000");
		
		for (long n = 1; ; n++) {
			BigInteger e = p(n);
			// System.out.println("n=" + n + ", p(n)=" + e);
			if ((e.mod(million)).compareTo(BigInteger.ZERO)==0) {
				System.out.println("answer: "+ n);
				break;
			}
		}
		System.out.println("Time (ms): "+ (System.currentTimeMillis() - start));
	}

	public static BigInteger p(long n) {
		//System.out.println("\tcheck: "+ n);
		if(n<0){
			return BigInteger.ZERO;
		}
		if(cache[(int)n] != null){
			return cache[(int)n];
		}
		BigInteger val = BigInteger.ZERO;
		BigInteger temp;
		for(long k=1; k<=n; k++){
			// acc to wolfram
			temp = p(n - ( ( k * ((3 * k) - 1) )/2 )).add(p(n - ( ( k * ((3 * k) + 1) )/2 )));
			if(k%2==0){
				val = val.subtract(temp);
			}else{
				val = val.add(temp);
			}
		}
		cache[(int)n] = val;
		return val;
	}
}

// answer: 55374
// Time (ms): 11772
