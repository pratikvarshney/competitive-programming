var start = new Date().getTime();

var cache = [];

function expand(remain, maxval) {
	if (cache[remain] != undefined && cache[remain][maxval] != undefined) {
		return cache[remain][maxval];
	}
	if (cache[remain] == undefined) {
		cache[remain] = [];
	}
	if (remain == 0) {
		cache[remain][maxval] = 1;
		return 1;
	}
	var val = 0;
	for (var i = (remain < maxval) ? remain : maxval; i >= 1; i--) {
		val += expand(remain - i, i);
	}
	cache[remain][maxval] = val;
	return val;
}

var answer = 0;
for (var n = 5; ; n++) {
	var e = expand(n, n);
	console.log('n=' + n + ', p(n)=' + e);
	if ((e % 1000000) == 0) {
		answer = n;
		break;
	}
}

console.log('answer: ' + answer);
console.log('Time (ms): ' + (new Date().getTime() - start));