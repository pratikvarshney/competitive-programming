import java.math.BigInteger;
import java.io.*;
class p78 {
	public static BigInteger[][] cache;
	
	public static void main(String[] arr) throws Exception{
		int maxval = Integer.parseInt(arr[0]);
		System.out.println("maxval: "+maxval);
		cache = new BigInteger[maxval][maxval];
			
		BigInteger million = new BigInteger("1000000");
		
		for (int n = 1; n < maxval; n++) {
			BigInteger e = expand(n, n);
			System.out.println("n=" + n + ", p(n)=" + e);
			if ((e.mod(million)).compareTo(BigInteger.ZERO)==0) {
				System.out.println("answer: "+ n);
				break;
			}
		}
	}

	public static BigInteger expand(int remain, int maxval) throws Exception {
		if (cache[remain][maxval] != null) {
			return cache[remain][maxval];
		}
		if (remain == 0) {
			cache[remain][maxval] = BigInteger.ONE;
			return BigInteger.ONE;
		}
		BigInteger val = BigInteger.ZERO;
		for (int i = (remain < maxval) ? remain : maxval; i >= 1; i--) {
			val = val.add(expand(remain - i, i));
		}
		cache[remain][maxval] = val;
		return val;
	}
}