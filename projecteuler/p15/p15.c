//METHOD 1
// fact(40) / ( fact(20) * fact(20) )
//
//METHOD 2
//   # * * 5
//   * * * 4
//   * * * 3
//   5 4 3 2
//
//   a[i][j] = a[i+1][j] + a[i][j+1]
//   a[0][0] = answer
//137846528820
#include<stdio.h>
int main()
{
    int row=20, col=20, i, j;
    long long int arr[20][20] = {0};
    long long int val;
    //init
    val=2;
    i=row-1;
    j=col-1;
    while(i>=0)
    {
        arr[i][j]=val;
        val++;
        i--;
    }
    val=3;
    i=row-1;
    j=col-2;
    while(j>=0)
    {
        arr[i][j]=val;
        val++;
        j--;
    }
    for(i=row-2; i>=0; i--)
    {
        for(j=col-2; j>=0; j--)
        {
            arr[i][j] = arr[i][j+1] + arr[i+1][j];
        }
    }

    //DISPLAY
    /*for(i=0; i<row; i++)
    {
        printf("\n");
        for(j=0; j<col; j++)
        {
            printf("%16lld",arr[i][j]);
        }
    }*/
    printf("%lld",arr[0][0]);
    return 0;
}
