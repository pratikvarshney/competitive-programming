import java.math.BigInteger;
class p25
{
	public static void main(String arr[])
	{
		int i;
		BigInteger a, b, c, limit;
		a=BigInteger.valueOf(1);
		b=a;
		limit=BigInteger.valueOf(10);
		limit=limit.pow(999);
		i=2;
		while(b.compareTo(limit) < 0)
		{
			c=a.add(b);
			a=b;
			b=c;
			i++;
		}
		System.out.print(i);
	}
}