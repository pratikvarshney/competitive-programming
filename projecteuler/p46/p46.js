var limit = 10000;
var prime = new Array(limit+1);
for(var i=0; i<limit; i++) prime[i]=true;
prime[0]=false;
prime[1]=false;
var crosslimit = Math.ceil(Math.sqrt(limit+1));
for(var i=2; i<=crosslimit; i++)
{
	if(prime[i])
	{
		for(var j=2*i; j<=limit; j+=i)
		{
			prime[j]=false;
		}
	}
}

var res=0;
for(var i=9; i<=limit; i=i+2)
{
		if(!prime[i])
		{
			if(!isGoldbach(i))
			{
				res = i;
				break;
			}
		}
}

function isGoldbach(k)
{
	for(var j=k-2; j>2; j=j-2)
	{
		if(prime[j])
		{
			if(Number.isInteger(Math.sqrt((k-j)/2)))
			{
				return true;
			}
		}
	}
	return false;
}
res;

/* 5777 */