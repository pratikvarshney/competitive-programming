var start = new Date().getTime();
var limit = 3163;
var prime = new Array(limit + 1);
for (var i = 0; i < limit; i++) prime[i] = true;
prime[0] = false;
prime[1] = false;
var crosslimit = Math.ceil(Math.sqrt(limit + 1));
for (var i = 2; i <= crosslimit; i++) {
	if (prime[i]) {
		for (var j = 2 * i; j <= limit; j += i) {
			prime[j] = false;
		}
	}
}

var p = [];
p.push(2);
for (var i = 3; i <= limit; i += 2) {
	if (prime[i]) p.push(i);
}

function getPrimeFactors(n) {
	var factors = [];
	for (var i = 0; i < p.length && p[i] <= n; i++) {
		if (n % p[i] == 0) {
			factors.push(p[i]);
			n /= p[i];
			while (n % p[i] == 0) {
				n /= p[i];
			}
		}
		if (n <= 1) {
			break;
		}
	}
	if (n > 1) {
		factors.push(n);
	}
	return factors;
}

function totient(n) {
	var factors = getPrimeFactors(n);
	var numerator = 1;
	var denominator = 1;
	for (var i = 0; i < factors.length; i++) {
		denominator *= factors[i];
		numerator *= (factors[i] - 1);
	}
	return (n * numerator) / denominator;
}
function isPermutation(a, b) {
	var strA = a.toString();
	var strB = b.toString();
	var charMapA = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	var charMapB = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	for (var i = 0; i < strA.length; i++) {
		charMapA[Number(strA[i])]++;
	}
	for (var i = 0; i < strB.length; i++) {
		charMapB[Number(strB[i])]++;
	}
	for (var i = 0; i < 10; i++) {
		if (charMapA[i] != charMapB[i]) {
			return false;
		}
	}
	return true;
}
var minN = 2;
var minRatio = minN/totient(minN);
var val = 0;
var t = 0;
for(var n=3; n<10000000; n++){
	t = totient(n);
	if(!isPermutation(n, t)){
		continue;
	}
	val = n/t;
	if(val < minRatio){
		minRatio = val;
		minN = n;
	}
}
var answer = minN;
console.log('answer: '+answer);
console.log('Time (ms): '+( new Date().getTime() - start));

// answer: 8319823
// Time (ms): 22098