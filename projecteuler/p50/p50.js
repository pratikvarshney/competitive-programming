var limit = 1000000;
var prime = new Array(limit+1);
for(var i=0; i<limit; i++) prime[i]=true;
prime[0]=false;
prime[1]=false;
var crosslimit = Math.ceil(Math.sqrt(limit+1));
for(var i=2; i<=crosslimit; i++)
{
	if(prime[i])
	{
		for(var j=2*i; j<=limit; j+=i)
		{
			prime[j]=false;
		}
	}
}

var p=[];
p.push(0);
p.push(2);
for(var i=3; i<=limit; i=i+2)
{
	if(prime[i]) p.push(i);
}
for(var i=1; i<p.length; i++) p[i] += p[i-1];

//matrix
var max=0;
var maxval=0;
for(var i=1; i<p.length; i++)
{
	for(var j=i-1; j>=0; j--)
	{
		if((p[i]-p[j])>1000000)
		{
			break;
		}
		else
		{
			if(prime[p[i]-p[j]])
			{
				if((i-j)>max){
					max = (i-j);
					maxval = p[i]-p[j];
				}
			}
		}
	}
}
maxval;

/* 997651,543 */
/* ans = 997651 */