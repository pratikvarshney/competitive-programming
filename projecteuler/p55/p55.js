// init
var maxVal = 10000;
var maxIteration = 50;
var lychrel = 0;
var count = new Array(maxVal);
for (var i = 0; i < maxVal; i++) {
  count[i] = 0;
}
for (var i = 0; i < maxVal; i++) {
  count[i] = getCount(i);
  if (count[i] > maxIteration) {
    lychrel++;
  }
}
console.log(lychrel);
function getCount(a) {
  var num = [
  ];
  var val;
  while (a > 0) {
    val = a % 10
    num.push(val);
    a = (a - val) / 10;
  }
  for (var i = 1; i <= maxIteration; i++) {
    num = addReverse(num);
    if (isPalindromic(num)) {
      return i;
    }
  }
  return maxIteration + 1;
}
function addReverse(num) {
  var res = new Array(num.length);
  var extra = 0;
  var val;
  var right = num.length - 1;
  for (var i = 0; i < num.length; i++) {
    val = num[i] + num[right - i] + extra;
    res[i] = (val % 10);
    extra = (val - res[i]) / 10;
  }
  while (extra > 0) {
    right++;
    res[right] = (extra % 10);
    extra = (extra - res[right]) / 10;
  }
  return res; // real value = res.reverse();
}
function isPalindromic(num) {
  var max = Math.floor(num.length / 2);
  for (var left = 0, right = num.length - 1; left < max; left++, right--) {
    if (num[left] != num[right]) {
      return false;
    }
  }
  return true;
}
// answer = 249
