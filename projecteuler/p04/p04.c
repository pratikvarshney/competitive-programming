/* START    :   17/02/2014 */
/* END      :   17/02/2014 */

/*
http://projecteuler.net/problem=4

Largest palindrome product
Problem 4
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 * 99.
Find the largest palindrome made from the product of two 3-digit numbers.

*/

/*

ANS = LARGEST: 993 * 913 = 906609

*/

#include<stdio.h>

int int_to_str_rev(int input, char *str); /* Reversed string */
int is_pallindrome(int input);

int main()
{
    int max = 999, min=100, i, j, k, largest=0, a=0, b=0;
    int iterations=0;
    //int found=0;
    for(i=max; i>=min; i--)
    {
        for(j=i; j>=min; j--)
        {
            iterations++;
            k=i*j;
            if(is_pallindrome(k))
            {
                //found=1;
                printf("\n%d * %d = %d",i,j,k);
                if(k>largest)
                {
                    largest=k;
                    a=i;
                    b=j;
                    min=j;
                }
                //break;
            }
        }
        //if(found) break;
    }

    printf("\n\nLARGEST: %d * %d = %d", a, b, largest);

    printf("\n\niterations = %d", iterations);

    return 0;
}
int int_to_str_rev(int input, char *str) /* Reversed string */
{
    int i=0;
    do{
        str[i]=(input%10)+'0';
        i++;
        input/=10;
    }while(input>0);
    str[i]='\0';
    return i;
}
int is_pallindrome(int input)
{
    char str[11];
    int length, found=1, i;
    length = int_to_str_rev(input,str);
    length--;
    for(i=(length-1)/2; i>=0; i--)
    {
        if(str[i]!=str[length-i])
        {
            found=0;
            break;
        }
    }
    return found;
}
