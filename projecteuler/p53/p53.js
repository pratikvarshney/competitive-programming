var maxNum=100;
var maxVal=1000000;
var result=0;

// init
var primes=[];
primes.push(2);
var fArr=new Array(maxNum);
fArr[0]=new Array(maxNum);
for(var i=0; i<=maxNum; i++){
fArr[0][i]=0;
}
for(var i=1; i<=maxNum; i++){
fArr[i]=getFactorsArr(i);
}

function getFactorsArr(input){
var n=input;
var arr=new Array(maxNum);
for(var i=0; i<=maxNum; i++){
arr[i]=0;
}
for(var i=0; i<primes.length; i++){
var divider=primes[i];
while((n%divider)==0){
n/=divider;
arr[divider]++;
}
}
if(n>1){
arr[n]++;
primes.push(n);
}
return arrAdd(arr,fArr[input-1]);
}

// process
for(var n=1; n<=maxNum; n++){
result+=cValExceeds1M(n);
}

function cValExceeds1M(n){
var maxR=(n+(n%2))/2; //ceil
for(var r=1; r<=maxR; r++){
if(c(n,r)>maxVal){
return ((n-r)-r)+1; //max-min+1 
}
}
return 0;
}

function c(n,r){
return prod(arrSubtract(fArr[n], arrAdd(fArr[r], fArr[n-r])));
}

function prod(a){
var res=1;
for(var i=2; i<=maxNum; i++){
if(a[i]>0){
res*=(Math.pow(i,a[i]));
}
}
return res;
}

function arrAdd(a, b){
var arr=new Array(maxNum);
for(var i=0; i<=maxNum; i++){
arr[i]=a[i]+b[i];
}
return arr;
}

function arrSubtract(a, b){
var arr=new Array(maxNum);
for(var i=0; i<=maxNum; i++){
arr[i]=a[i]-b[i];
}
return arr;
}

result;


// result = 4075
