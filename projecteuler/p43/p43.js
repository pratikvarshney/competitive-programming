var arr = [];
var digits = [0,1,2,3,4,5,6,7,8,9];
function perm(a, b)
{
	if(b.length>1)
	{
		for(var i=0; i<b.length; i++)
		{
			var temp=b.slice(0);
			temp.splice(i,1);
			perm(a+b[i].toString(), temp);
		}
	}
	else
	{
		if(a.charAt(0)!="0")
			arr.push(Number(a+b[0].toString()));
	}
}
perm("", digits);

function check(num)
{
	if((Math.floor(num/1000000)%1000)%2 != 0) return false;
	if((Math.floor(num/100000)%1000)%3 != 0) return false;
	if((Math.floor(num/10000)%1000)%5 != 0) return false;
	if((Math.floor(num/1000)%1000)%7 != 0) return false;
	if((Math.floor(num/100)%1000)%11 != 0) return false;
	if((Math.floor(num/10)%1000)%13 != 0) return false;
	if((num%1000)%17 != 0) return false;
	return true;
}

var res = 0;
for(var i=0; i<arr.length; i++)
{
	if(check(arr[i])) res = res + arr[i];
}
res;

/* 16695334890 */