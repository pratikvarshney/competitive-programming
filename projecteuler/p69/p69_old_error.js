var start = new Date().getTime();
var maxVal = 1000000;
// init primes
prime = [
  2
];
var limit = maxVal / 2;
for (var i = 3; i <= limit; i += 2) {
  var isPrime = true;
  var sqrt = Math.sqrt(i);
  for (var j = 0; prime[j] <= sqrt; j++) {
    if (i % prime[j] == 0) {
      isPrime = false;
      break;
    }
  }
  if (isPrime) prime.push(i);
}
function getPrimeFactors(n) {
  var input = n;
  var limit = n / 2;
  var factors = [
  ];
  for (var i = 0; prime[i] <= limit; i++) {
    if (n % prime[i] == 0) {
      factors.push(prime[i]);
// we can use cache to speed this up
      while(n % prime[i] == 0){
         n /= prime[i];
      }
    }
  }
  return factors;
}
function phi(n) {
  var res = n - 1; // exclude itself
  var k = n - 1;
  var factors = getPrimeFactors(n);
  for (var i = 0; i < factors.length; i++) {
    var removeFactorMultiples = Math.floor(k / factors[i]);
    for (var j = 0; j < i; j++) {
      var alreadyRemoved = Math.floor(k / (factors[i] * factors[j]));
      removeFactorMultiples -= alreadyRemoved;
    }
    res -= removeFactorMultiples;
  }
  return res;
}
var answer = 0;
var maxRatio = 0;
for (var n = 2; n <= maxVal; n++) {
  var ratio = n / phi(n);
  if (maxRatio < ratio) {
    maxRatio = ratio;
    answer = n;
  }
}
console.log('Total time = ' + (new Date().getTime() - start) + ' ms');
console.log('maxRatio = ' + maxRatio);
console.log('answer = ' + answer);
