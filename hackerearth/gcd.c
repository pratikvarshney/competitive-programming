#include <stdio.h>

unsigned int gcd(unsigned int a, unsigned int b)
{
	if( (a==0) || (b==0) )
	{
		return a+b;
	}
	return gcd(b, (a%b));
}

int main()
{
    unsigned int t=0, a=0, b=0;
    unsigned int curr_gcd=0;

    scanf("%u", &t);
    while(t>0)
    {
    	scanf("%u%u",&a,&b);
    	while(a>1 && b>1)
    	{
    		curr_gcd = gcd(a,b);
	    	if(curr_gcd>1)
	    	{
	    		b--;
	    		if(b==1) break;
	    		b/=gcd(a,b);
	    	}
	    	else if(curr_gcd==1)
	    	{
	    		b--;
	    	}

	    	if(b==1) break;

	    	curr_gcd = gcd(b,a);
	    	if(curr_gcd>1)
	    	{
	    		a--;
	    		if(a==1) break;
	    		a/=gcd(b,a);
	    	}
	    	else if(curr_gcd==1)
	    	{
	    		a--;
	    	}
	    	if(a==1) break;
    	}
    	if(a==1 && b==1)
    		printf("Draw\n");
    	else if(a==1)
    		printf("Chandu Don\n");
    	else if(b==1)
    		printf("Arjit\n");

    	t--;
    }


    return 0;
}
