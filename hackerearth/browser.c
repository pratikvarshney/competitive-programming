#include<stdio.h>
#include<string.h>
int main()
{
    int tc=0, a=0, i=0, e=0;
    char input[200]={'\0'};
    scanf("%d",&tc);
    while(tc>0)
    {
        scanf("%s",input);
        a=0;
        if(input[0]=='w' && input[1]=='w' && input[2]=='w' && input[3]=='.') i=4;
        else i=0;
        e=strrchr(input,'.')-input;
        if(e<0) e=strlen(input);
        for(; i<=e; i++)
        {
            switch(input[i])
            {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                break;
            default:
                a++;
                break;
            }
        }
        for(;input[i]!='\0';i++)
            a++;
        printf("%d/%d\n",a,i);
        tc--;
    }

    return 0;
}
