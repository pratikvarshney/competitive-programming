#include <stdio.h>

int main()
{
    int T, N, K, i;
    scanf("%d",&T);
    while(T>0)
    {
    	scanf("%d%d",&N,&K);
    	int A[N];
    	for(i=0; i<N; i++)
    	{
    		scanf("%d",&A[i]);
    	}
    	int start=0, range = N-1, min;
    	while(K>=range)
    	{
    		min=start;
    		for(i=0; i<=range; i++)
    		{
    			if(A[min]>A[start+i])
    			{
    				min=start+i;
    			}
    		}
    		int temp;
    		for(i=min-1; i>=start; i--)
    		{
    			temp = A[i];
    			A[i]=A[i+1];
    			A[i+1]=temp;
    			K--;
    		}

    		start++;
    		range=N-start-1;

    	}
    	min=start;
		for(i=0; i<=K; i++)
		{
			if(A[min]>A[start+i])
			{
				min=start+i;
			}
		}
		int temp;
		for(i=min-1; i>=start; i--)
        {
            temp = A[i];
            A[i]=A[i+1];
            A[i+1]=temp;
            K--;
        }

    	for(i=0; i<N; i++)
    	{
    		printf("%d ",A[i]);
    	}
    	printf("\n");
    	T--;
    }
    return 0;
}
