#include <stdio.h>

struct friends
{
	int popularity;
	struct friends *next;
}*head;
typedef struct friends friend_ptr;
int fun(int N, int K);
int main()
{
    int T, N, K, i;
    scanf("%d",&T);
    while(T>0)
    {
        scanf("%d%d",&N,&K);
        head = (friend_ptr*)malloc(sizeof(friend_ptr));
        head->next=NULL;
        friend_ptr *curr;
        curr = head;
        for(i=0; i<N; i++)
        {
            curr->next = (friend_ptr*)malloc(sizeof(friend_ptr));
            curr = curr->next;
            curr->next = NULL;
            scanf("%d",&(curr->popularity));
        }
        fun(N,K);
        curr = head;
        while(curr->next!=NULL)
        {
            printf("%d ",curr->next->popularity);
            curr = curr->next;
        }
        printf("\n");


        T--;
    }



    return 0;
}
int fun(int N, int K)
{
    friend_ptr *curr;
    curr = head;
    int i;
    while(curr->next!=NULL && curr->next->next!=NULL)
    {
        if(curr->next->popularity < curr->next->next->popularity )
        {
            curr->next = curr->next->next;
            K--;
        }
        else curr = curr->next;
        if(K==0) break;
    }
    if(K>0)
    {
        int rem_elem=0;
        curr = head;
        while(curr->next!=NULL)
        {
            curr = curr->next;
            rem_elem++;
        }
        rem_elem-=K;
        if(rem_elem>0)
        {
            curr = head;
            while(rem_elem>0)
            {
                curr = curr->next;
                rem_elem--;
            }
            curr->next=NULL;
        }
        else {head->next=NULL;}
    }

    return 0;
}
