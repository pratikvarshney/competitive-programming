#include<stdio.h>

int main()
{
    int N,k,i,j;
    unsigned int divisor = 1000000007;
    long long unsigned temp_012, temp_345, temp_res;
    unsigned int a[6];
    unsigned int b[7]={2,2,2,2,3,3,5};

    scanf("%d",&N);
    if(N<13)
    {
        printf("0\n");
        return 0;
    }
    if(N<15)
    {
        printf("1\n");
        return 0;
    }
    k=(N%2==0)?(N-14)/2:(N-13)/2;

    a[0]=k+1;
    a[1]=k+2;
    a[2]=k+3;
    a[3]=k+4;
    a[4]=k+5;
    a[5]=k+6;

    for(i=0; i<7; i++)
    {
        for(j=0; j<6; j++)
        {
            if(a[j]%b[i]==0)
            {
                a[j]/=b[i];
                break;
            }
        }
    }

    temp_012 = ((long long unsigned)a[0])*((long long unsigned)a[1])*((long long unsigned)a[2]);
    temp_345 = ((long long unsigned)a[3])*((long long unsigned)a[4])*((long long unsigned)a[5]);
    temp_res = (temp_012%divisor)*(temp_345%divisor);


    printf("%llu\n", temp_res%divisor);

    return 0;
}
