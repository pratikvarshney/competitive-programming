#include <stdio.h>

int main()
{
    int M,N,d;
    int A[100000];
    int base=0;
    int i;
    char op[2];
    int val;
    scanf("%d%d",&N,&M);

    for(i=0; i<N; i++)
    {
        scanf("%d",&A[i]);
    }

    while(M>0)
    {
        scanf("%s%d", op, &val);

        switch(op[0])
        {
        case 'C':
            base=(base+val)%N;
            break;
        case 'A':
            base=(base+(N-val))%N;
            break;
        case 'R':
            printf("%d\n",A[(base+val-1)%N]);
        }

    	M--;
    }
    return 0;
}
