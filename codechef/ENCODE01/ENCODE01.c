#include<stdio.h>

int main()
{
    int sum, t, n;

    scanf("%d", &t);

    while(t--)
    {
        scanf("%d", &n);
        sum = (int)(n/2) * (int)((n+1)/2); //OR n*n/4
        printf("%d\n", sum);
    }
    return 0;
}
