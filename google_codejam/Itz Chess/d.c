#include<stdio.h>

int check(char b[8][8], int i, int j);

int main()
{
    int c, t, n, i, j, k, kill, p, q, s;
    char str[8];
    char b[8][8];
    scanf("%d", &t);
    for(c=1; c<=t; c++)
    {
        for(i=0; i<8; i++)
        {
            for(j=0; j<8; j++)
            {
                b[i][j]='.';
            }
        }
        scanf("%d", &n);
        for(k=0; k<n; k++)
        {
            scanf("%s", str);
            b[str[1]-'1']['H'-str[0]]=str[3];
        }
        kill=0;
        for(i=0; i<8; i++)
        {
            for(j=0; j<8; j++)
            {
                switch(b[i][j])
                {
                case 'K':
                    if(check(b, i-1, j-1)==1) kill++;
                    if(check(b, i-1, j)==1) kill++;
                    if(check(b, i-1, j+1)==1) kill++;
                    if(check(b, i, j-1)==1) kill++;
                    if(check(b, i, j+1)==1) kill++;
                    if(check(b, i+1, j-1)==1) kill++;
                    if(check(b, i+1, j)==1) kill++;
                    if(check(b, i+1, j+1)==1) kill++;
                    break;
                case 'Q':
                    //right
                    for(p=j+1; p<8; p++)
                    {
                         s=check(b, i, p);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //left
                    for(p=j-1; p>=0; p--)
                    {
                         s=check(b, i, p);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //up
                    for(p=i-1; p>=0; p--)
                    {
                        s=check(b, p, j);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //down
                    for(p=i+1; p<8; p++)
                    {
                        s=check(b, p, j);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //down-right
                    for(p=i+1, q=j+1; p<8 && q<8; p++, q++)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //down-left
                    for(p=i+1, q=j-1; p<8 && q>=0; p++, q--)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //up-right
                    for(p=i-1, q=j+1; p>=0 && q<8; p--, q++)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //up-left
                    for(p=i-1, q=j-1; p>=0 && q>=0; p--, q--)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    break;
                case 'R':
                    //right
                    for(p=j+1; p<8; p++)
                    {
                         s=check(b, i, p);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //left
                    for(p=j-1; p>=0; p--)
                    {
                         s=check(b, i, p);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //up
                    for(p=i-1; p>=0; p--)
                    {
                        s=check(b, p, j);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //down
                    for(p=i+1; p<8; p++)
                    {
                        s=check(b, p, j);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    break;
                case 'B':
                    //down-right
                    for(p=i+1, q=j+1; p<8 && q<8; p++, q++)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //down-left
                    for(p=i+1, q=j-1; p<8 && q>=0; p++, q--)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //up-right
                    for(p=i-1, q=j+1; p>=0 && q<8; p--, q++)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    //up-left
                    for(p=i-1, q=j-1; p>=0 && q>=0; p--, q--)
                    {
                        s=check(b, p, q);
                         if(s==1)
                         {
                             kill++;
                             break;
                         }
                    }
                    break;
                case 'N':
                    if(check(b, i+1, j+2)==1) kill++;
                    if(check(b, i-1, j+2)==1) kill++;
                    if(check(b, i+1, j-2)==1) kill++;
                    if(check(b, i-1, j-2)==1) kill++;
                    if(check(b, i+2, j+1)==1) kill++;
                    if(check(b, i+2, j-1)==1) kill++;
                    if(check(b, i-2, j+1)==1) kill++;
                    if(check(b, i-2, j-1)==1) kill++;
                    break;
                case 'P':
                    if(check(b, i-1, j-1)==1) kill++;
                    if(check(b, i+1, j-1)==1) kill++;
                    break;
                default :
                    break;
                }
            }
        }
        printf("Case #%d: %d\n", c, kill);
    }
    return 0;
}

int check(char b[8][8], int i, int j)
{
    if(i<0) return -1;
    if(i>=8) return -1;
    if(j<0) return -1;
    if(j>=8) return -1;
    if(b[i][j]=='.') return 0;
    return 1;
}
