#include<stdio.h>
#include<math.h>
#include<string.h>
#include<ctype.h>

FILE *fp, *outp;

long long int get_min(int k[], int v, long long int *look, char *check);

int main()
{
    int N, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &N);

    for(c_no=1; c_no<=N; c_no++)
    {
        int k[10], i, v;
        fprintf(outp, "Case #%d:", c_no);
        fprintf(stdout, "Case #%d:\n", c_no);
        for(i=0; i<10; i++) fscanf(fp, "%d", &(k[i]));

        fscanf(fp, "%d", &v);
        char *check = (char *)calloc(v+1, sizeof(char));
        long long int *look = (long long int *)calloc(v+1, sizeof(long long int));
        if(look==NULL) printf("ERROR");
        long long int min = get_min(k, v, look, check);

        if(min!=0)
        {
            fprintf(outp, " %lld\n", min+1);
        }
        else fprintf(outp, " Impossible\n");
    }

    return 0;
}
long long int get_min(int k[], int v, long long int *look, char *check)
{
    if(check[v])
    {
        return look[v];
    }
    long long int min;
    char str[8];
    itoa(v, str, 10);
    int len=strlen(str);
    int i, found=1;
    for(i=0; i<len; i++)
    {
        if(k[str[i]-'0']==0)
        {
            found=0;
            break;
        }
    }
    if(found)
    {
        check[v]=1;
        look[v]=len;
        return len;
    }
    else
    {
        int rt = v/2;
        long long int a, b, temp;
        min=9223372036854775807;
        for(i=rt; i>1; i--)
        {
            if(v%i==0)
            {
                a = get_min(k, i, look, check);
                b = get_min(k, v/i, look, check);
                if(a>0 && b>0)
                {
                    temp=a+b+1;
                }
                else continue;
                if(min>temp)
                {
                    min=temp;
                }
            }
        }
    }

    if(min==9223372036854775807)
    {
        check[v]=1;
        look[v]=0;
        return 0;
    }
    check[v]=1;
    look[v]=min;
    return min;
}
