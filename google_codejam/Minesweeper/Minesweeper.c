#include<stdio.h>
#include<math.h>
#include<string.h>
#include<ctype.h>

FILE *fp, *outp;
char mat[302][302];
char check[302][302];

int mark(int n, int i, int j);

int main()
{
    int t, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &t);

    for(c_no=1; c_no<=t; c_no++)
    {
        int i, j, n, r;
        char str[320];
        fscanf(fp, "%d", &n);
        r=n+2;
        for(i=0; i<r; i++)
            for(j=0; j<r; j++)
            {
                mat[i][j]='0';
                check[i][j]=0;
            }
        for(i=1; i<=n; i++)
        {
            fscanf(fp, "%s", str);
            for(j=1; j<=n; j++)
            {
                if(str[j-1]=='*')
                {
                    mat[i][j]='*';
                    check[i][j]++;
                }
            }
        }
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                if(mat[i][j]=='*')
                {
                    //mark neigh
                    if(mat[i-1][j-1]!='*') mat[i-1][j-1]++;
                    if(mat[i-1][j]!='*')mat[i-1][j]++;
                    if(mat[i-1][j+1]!='*')mat[i-1][j+1]++;

                    if(mat[i][j-1]!='*')mat[i][j-1]++;

                    if(mat[i][j+1]!='*')mat[i][j+1]++;

                    if(mat[i+1][j-1]!='*')mat[i+1][j-1]++;
                    if(mat[i+1][j]!='*')mat[i+1][j]++;
                    if(mat[i+1][j+1]!='*')mat[i+1][j+1]++;
                }
            }
        }
        int marked=0;
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                if(!check[i][j] && mat[i][j]=='0')
                {
                    mark(n, i, j);
                    marked++;
                }
            }
        }
        for(i=1; i<=n; i++)
        {
            for(j=1; j<=n; j++)
            {
                if(!check[i][j])
                {
                    marked++;
                }
            }
        }
        fprintf(outp, "Case #%d: %d\n", c_no, marked);
        printf("Case #%d: %d\n", c_no, marked);
    }

    return 0;
}

int mark(int n, int i, int j)
{
    if(i==0 || i==(n+1)) return;
    if(j==0 || j==(n+1)) return;

    check[i][j]=1;
    int p, q;

    for(p=i-1; p<=i+1; p++)
    {
        for(q=j-1; q<=j+1; q++)
        {
            if(!check[p][q])
            {
                if(mat[p][q]=='0')
                {
                    mark(n, p, q);
                }
                else check[p][q] = 1;
            }
        }
    }

}
