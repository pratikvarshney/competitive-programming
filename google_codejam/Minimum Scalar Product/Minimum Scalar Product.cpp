#include <iostream>
#include <algorithm>
#include <vector>
#include<fstream>

using namespace std;

int main()
{
    ifstream inp("sample.in");
    ofstream output("output.out");
    if(!inp.is_open() || !output.is_open()) return -1;

    long long T, n, c, i, temp;

    inp>>T;

    for(c=1; c<=T; c++)
    {
        vector<long long> x, y;
        x.clear();
        y.clear();

        inp >> n;

        for(i=0; i<n; i++)
        {
             inp >> temp;
             x.push_back(temp);
        }
        for(i=0; i<n; i++)
        {
            inp >> temp;
            y.push_back(temp);
        }

        sort(x.begin(), x.end());
        sort(y.begin(), y.end());

        reverse(y.begin(), y.end());

        long long sum=0;

        vector<long long>::iterator it_x=x.begin();
        vector<long long>::iterator it_y=y.begin();
        while(it_x!=x.end())
        {
            sum += ((*it_x) * (*it_y));
            it_x++;
            it_y++;
        }

        output << "Case #" << c << ": " << sum << endl;

    }
    inp.close();
    output.close();

    return 0;
}
