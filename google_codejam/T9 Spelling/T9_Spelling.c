#include<stdio.h>
#include<string.h>

FILE *fp, *outp;
int arr[26]={
    2,22,222,
    3,33,333,
    4,44,444,
    5,55,555,
    6,66,666,
    7,77,777,7777,
    8,88,888,
    9,99,999,9999
};

int main()
{
    int N, c_no, i, len;
    int pre_key;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &N);

    for(c_no=1; c_no<=N; c_no++)
    {
        char input[1001];

        fscanf(fp, "%[\n]", input);
        fscanf(fp, "%[^\n]", input);

        fprintf(outp, "Case #%d: ", c_no);
        len=strlen(input);
        pre_key=-1;
        for(i=0; i<len; i++)
        {
            if(input[i]==' ')
            {
                if(pre_key==0)
                {
                    fprintf(outp, " ");
                }
                fprintf(outp, "0");
                pre_key=0;
            }
            else
            {
                if(pre_key==arr[input[i]-'a']%10)
                {
                    fprintf(outp, " ");
                }
                fprintf(outp, "%d", arr[input[i]-'a']);
                pre_key=arr[input[i]-'a']%10;
            }
        }
        fprintf(outp, "\n");
    }


    return 0;
}
