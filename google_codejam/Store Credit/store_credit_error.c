#include<stdio.h>

int main()
{
    FILE *fp, *outp;
    int i, j, N, L, C, val, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &N);

    for(c_no=1; c_no<=N; c_no++)
    {
        fscanf(fp, "%d", &C);
        fscanf(fp, "%d", &L);

        int input[1001]={0};
        int index[1001]={0};
        int second[1001]={0};

        for(i=1; i<=L; i++)
        {
            fscanf(fp, "%d", &input[i]);
            if(index[input[i]]>0)
            {
                second[input[i]]=i;
            }
            else
            {
                index[input[i]]=i;
            }
        }

        for(i=1; i<=L; i++)
        {
            val = C - input[i];
            if(val>0)
            {
                if(index[val]>0 && index[val]!=i)
                {
                    fprintf(outp, "Case #%d: %d %d\n", c_no, i, index[val]);
                    break;
                }
                if(index[val]>0 && second[val]>0 && index[val]==i)
                {
                    fprintf(outp, "Case #%d: %d %d\n", c_no, index[val], second[val]);
                    break;
                }
            }
        }

    }


    return 0;
}
