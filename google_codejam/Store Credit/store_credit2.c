#include<stdio.h>

int main()
{
    FILE *fp, *outp;
    int i, j, N, L, C, val, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &N);

    for(c_no=1; c_no<=N; c_no++)
    {
        fscanf(fp, "%d", &C);
        fscanf(fp, "%d", &L);

        int input[1001]={0};
        int index[1001]={0};
        int second[1001]={0};

        for(i=1; i<=L; i++)
        {
            fscanf(fp, "%d", &input[i]);
        }
        int found=0;
        for(i=1; i<L; i++)
        {
            val = C - input[i];
            if(val>0)
            {
                for(j=i+1; j<=L; j++)
                {
                    if(input[j]==val)
                    {
                        fprintf(outp, "Case #%d: %d %d\n", c_no, i, j);
                        found=1;
                        break;
                    }
                }
            }
            if(found) break;
        }

    }


    return 0;
}
