#include<stdio.h>

FILE *fp, *outp;

int main()
{
    int i,j,k;
    int T,n, c_no;
    char dir[8];

    fp=fopen("sample.in", "r");
    outp=fopen("output.out", "w");

    fscanf(fp, "%d", &T);

    for(c_no=1; c_no<=T; c_no++)
    {
        fscanf(fp, "%d%s", &n, dir);
        int a[20][20]={0};

        for(i=0; i<n; i++)
        {
            for(j=0; j<n; j++)
            {
                fscanf(fp, "%d", &a[i][j]);
            }
        }
        fprintf(outp, "Case #%d:\n", c_no);

        switch(dir[0])
        {
        case 'u':
            for(i=0; i<n; i++)
            {
                for(j=0; j<n-1; j++)
                {
                    for(k=j+1; k<n; k++)
                    {
                        if(a[k][i]==0)continue;
                        if(a[k][i]!=a[j][i]) break;
                        if(a[k][i]==a[j][i])
                        {
                            a[j][i]*=2;
                            a[k][i]=0;
                            break;
                        }
                    }
                }
                for(j=0; j<n-1; j++)
                {
                    if(a[j][i]==0)
                    {
                        for(k=j+1; k<n; k++)
                        {
                            if(a[k][i]!=0)
                            {
                                a[j][i]=a[k][i];
                                a[k][i]=0;
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case 'd':
            for(i=0; i<n; i++)
            {
                for(j=n-1; j>0; j--)
                {
                    for(k=j-1; k>=0; k--)
                    {
                        if(a[k][i]==0)continue;
                        if(a[k][i]!=a[j][i]) break;
                        if(a[k][i]==a[j][i])
                        {
                            a[j][i]*=2;
                            a[k][i]=0;
                            break;
                        }
                    }
                }
                for(j=n-1; j>0; j--)
                {
                    if(a[j][i]==0)
                    {
                        for(k=j-1; k>=0; k--)
                        {
                            if(a[k][i]!=0)
                            {
                                a[j][i]=a[k][i];
                                a[k][i]=0;
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case 'l':
            for(i=0; i<n; i++)
            {
                for(j=0; j<n-1; j++)
                {
                    for(k=j+1; k<n; k++)
                    {
                        if(a[i][k]==0)continue;
                        if(a[i][k]!=a[i][j]) break;
                        if(a[i][k]==a[i][j])
                        {
                            a[i][j]*=2;
                            a[i][k]=0;
                            break;
                        }
                    }
                }
                for(j=0; j<n-1; j++)
                {
                    if(a[i][j]==0)
                    {
                        for(k=j+1; k<n; k++)
                        {
                            if(a[i][k]!=0)
                            {
                                a[i][j]=a[i][k];
                                a[i][k]=0;
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case 'r':
            for(i=0; i<n; i++)
            {
                for(j=n-1; j>0; j--)
                {
                    for(k=j-1; k>=0; k--)
                    {
                        if(a[i][k]==0)continue;
                        if(a[i][k]!=a[i][j]) break;
                        if(a[i][k]==a[i][j])
                        {
                            a[i][j]*=2;
                            a[i][k]=0;
                            break;
                        }
                    }
                }
                for(j=n-1; j>0; j--)
                {
                    if(a[i][j]==0)
                    {
                        for(k=j-1; k>=0; k--)
                        {
                            if(a[i][k]!=0)
                            {
                                a[i][j]=a[i][k];
                                a[i][k]=0;
                                break;
                            }
                        }
                    }
                }
            }
            break;
        }
        for(i=0; i<n; i++)
        {
            for(j=0; j<n; j++)
            {
                fprintf(outp, "%d ", a[i][j]);
            }
            fprintf(outp, "\n");
        }

    }

    return 0;
}
