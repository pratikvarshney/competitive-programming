#include<stdio.h>

int main()
{
    int t, n, c, i, m, p, q, j;
    scanf("%d", &t);
    for(c=1; c<=t; c++)
    {
        printf("Case #%d:", c);
        scanf("%d", &n);
        int a[n];
        int b[n];
        m=0;
        for(i=0; i<n; i++)
        {
            scanf("%d%d", &p, &q);
            if(p<q)
            {
                a[i]=p;
                b[i]=q;
                if(m<q) m=q;
            }
            else
            {
                a[i]=q;
                b[i]=p;
                if(m<p) m=p;
            }
        }
        int d[m+1];
        for(i=0; i<=m; i++) d[i]=0;

        for(i=0; i<n; i++)
        {
            for(j=a[i]; j<=b[i]; j++) d[j]++;
        }
        scanf("%d", &q);
        for(i=0; i<q; i++)
        {
            scanf("%d", &j);
            printf(" %d", (j<=m)?d[j]:0);
        }
        printf("\n");
    }
    return 0;
}
