#include<cstdio>
#include<iostream>
#include<vector>
#include<queue>
#include<climits>

using namespace std;

class qnode
{
public:
    int index;
    int val;

    qnode(int in, int v);
};
qnode::qnode(int in, int v)
{
    index=in;
    val=v;
}

class compare
{
public:
    bool operator()(const qnode& l, const qnode& r)
    {
       return l.val > r.val;
    }
};

class edge
{
public:
    int index;
    int weight;
    bool is_train;
    int wait;

    edge(int ind, int wt, bool train, int tm);
};

edge::edge(int ind, int wt, bool train, int tm)
{
    index=ind;
    weight=wt;
    is_train=train;
    wait=tm;
}

class node
{
public:
    vector<edge> elist;

    node();
};

node::node(){}

FILE *fp, *outp;

int main()
{
    int t, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &t);

    for(c_no=1; c_no<=t; c_no++)
    {
        int n, s, i, j, k;
        vector<node> st;
        st.clear();
        fscanf(fp, "%d", &n);
        int *w=new int[n]; //weight
        int *b=new int[n]; //base
        int l;
        k=0;
        for(i=0; i<n; i++)
        {
            fscanf(fp, "%d%d", &s, &(w[i]));
            b[i]=k;
            int sn[s];

            st.push_back(node());
            k++;
            for(j=1; j<s; j++)
            {
                fscanf(fp, "%d", &l);
                sn[j-1]=l;
                st.push_back(node());
                //st[k-1].elist.push_back(edge(k, l, true, w[i])); //fwd edge
                //st[k].elist.push_back(edge(k-1, l, true, w[i])); //rev edge

                k++;
            }
            int u, v;
            for(u=b[i]; u<k; u++)
            {
                for(v=u+1; v<k; v++)
                {
                    l=0;
                    for(j=(u-b[i]); j<(v-b[i]); j++) l += sn[j];
                    st[u].elist.push_back(edge(v, l, true, w[i]));
                    st[v].elist.push_back(edge(u, l, true, w[i]));
                }
            }
        }
        //now k stores total num of stations

        int m, m1, s1, m2, s2, t0;

        fscanf(fp, "%d", &m);

        for(i=0; i<m; i++)
        {
            fscanf(fp, "%d%d%d%d%d", &m1, &s1, &m2, &s2, &t0);
            st[b[m1-1]+(s1-1)].elist.push_back(edge(b[m2-1]+(s2-1), t0, false, 0));
            st[b[m2-1]+(s2-1)].elist.push_back(edge(b[m1-1]+(s1-1), t0, false, 0));
        }

        /*for(i=0; i<k; i++)
        {
            cout << i << " -> ";
            int len=st[i].elist.size();
            if(len==0) cout << "NULL";
            else
            {
                for(vector<edge>::iterator it=st[i].elist.begin(); it!=st[i].elist.end(); it++)
                {
                    cout << "(" << it->index << "," << it->weight << ") ";
                }
            }
            cout << endl;
        }
        cout << endl;
        */

        int q, x1, y1, x2, y2;
        int curr, tr_wait;
        fscanf(fp, "%d", &q);
        priority_queue<qnode,vector<qnode>,compare> pq;
        int pre[k];
        fprintf(outp, "Case #%d:\n", c_no);
        printf("Case #%d:\n", c_no);
        for(i=0; i<q; i++)
        {
            fscanf(fp, "%d%d%d%d", &x1, &y1, &x2, &y2);
            int dist[k];
            bool vis[k];
            bool pre_train[k];
            for(j=0; j<k; j++)
            {
                dist[j] = INT_MAX;
                vis[j]=false;
            }
            curr=b[x1-1]+(y1-1);
            dist[curr]=0;
            pre[curr]=-1;
            vis[curr]=true;
            pq.push(qnode(curr, 0));
            pre_train[curr]=false;

            while(!pq.empty())
            {
                curr = pq.top().index;
                pq.pop();
                vis[curr]=false;

                for(vector<edge>::iterator it=st[curr].elist.begin(); it!=st[curr].elist.end(); it++)
                {
                    if(!pre_train[curr] && it->is_train)
                    {
                        tr_wait = it->wait;
                    }
                    else tr_wait=0;
                    if(dist[it->index]>(dist[curr]+(it->weight)+tr_wait))
                    {
                        dist[it->index]=(dist[curr]+(it->weight)+tr_wait);
                        pre_train[it->index]=it->is_train;
                        if(!vis[it->index])
                        {
                            pq.push(qnode(it->index, dist[it->index]));
                            vis[it->index]=true;
                        }
                        /*else
                        {
                            //update priority q
                            priority_queue<qnode,vector<qnode>,compare> temp;
                            while(!pq.empty())
                            {
                                qnode qn = pq.top();
                                pq.pop();
                                if(qn.index==it->index)
                                {
                                    qn.val = dist[it->index];
                                }
                                temp.push(qn);
                            }
                            pq=temp;
                        }*/
                        pre[it->index] = curr;
                    }
                }
            }
            if(dist[b[x2-1]+(y2-1)]==INT_MAX) fprintf(outp, "-1\n");
            else
            {
                fprintf(outp, "%d\n", dist[b[x2-1]+(y2-1)]);
                /*for(int d=(b[x2-1]+(y2-1)); ; d=pre[d])
                {
                    int x, y;
                    for(x=1; x<n; x++)
                    {
                        if(b[x]>d) break;
                    }
                    y=(d+1)-b[x-1];

                    cout << "("<< x << "," << y << ") = "<< dist[d] << endl;
                    if(pre[d]==-1) break;
                }*/
            }
        }


        delete[] w;
        delete[] b;
    }

    return 0;
}
