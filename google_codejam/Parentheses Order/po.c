#include<stdio.h>

int arr[202];
int maxm[202];

int rec(int index);
char res[202];

FILE *fp, *outp;

int main()
{
    long long  i, j, k;
    int n, t, c, a;

    fp=fopen("sample.in", "r");
    outp=fopen("output.txt","w");

    fscanf(fp,"%d", &t);

    for(c=1; c<=t; c++)
    {
        fscanf(fp,"%d", &n);
        fscanf(fp,"%lld", &k);

        res[2*n]='\0';
        for(i=1; i<=n; i++)
        {
            arr[i] = i;
            maxm[i]=(2*i)-1;
        }

        for(i=1; i<k; i++)
        {
            rec(n);
        }
        for(j=(2*n)-1; j>=0; j--) res[j]=')';
            for(a=1; a<=n; a++)
                res[arr[a]-1]='(';
            if(arr[1]==1)
                fprintf(outp,"Case #%d: %s\n", c, res);
            else
                fprintf(outp,"Case #%d: %s\n", c, "Doesn't Exist!");
    }



    return 0;
}

int rec(int index)
{
    if(index==1)
    {
        arr[index]++;
        return 0;
    }
    if(arr[index]==maxm[index]) arr[index] = rec(index-1)+1;
    else arr[index]++;
    return arr[index];
}
