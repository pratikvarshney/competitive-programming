#include<stdio.h>

#define MAX_MASK 0b1111111

int num_mask[10]={0b1111110, 0b0110000, 0b1101101, 0b1111001, 0b0110011, 0b1011011, 0b1011111, 0b1110000, 0b1111111, 0b1111011};
int rev_mask[10]={0b0000001, 0b1001111, 0b0010010, 0b0000110, 0b1001100, 0b0100100, 0b0100000, 0b0001111, 0b0000000, 0b0000100};
int pos_mask[7]={0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001};

int main()
{
    int i,j,n=1;
    int input[]={0b0110000};
    int result[10];

    for(i=0; i<10; i++)
    {
        result[i]=1;
    }

    int res_mask=0;

    for(i=0; i<n; i++)
    {
        res_mask = res_mask | input[i];
        printf("\n\nVAL: %d\n", i);
        for(j=0; j<10; j++)
        {
            if((input[i] & rev_mask[j])!=0)
            {
                result[((10+j)-((n-i)%10))%10]=0;
                printf("NOT: %d\n", ((10+j)-((n-i)%10))%10);
            }
        }
    }
    printf("\n\n");
    for(j=0; j<10; j++)
    {
        if(result[j]!=0)printf("RES: %d\n",j);
    }

    int flag=0;
    int curr;

    for(j=0; j<10; j++)
    {
        if(result[j]!=0)
        {
            curr=(j+n)%10;
            int act_mask=0b1111111;
            for(i=0; i<n; i++)
            {
                act_mask = act_mask & (input[i] ^ 0b1111111);
                if((num_mask[curr] & res_mask)!=input[i])
                {
                    result[j]=0;
                    printf("NOT: %d\n", j);
                    break;
                }
                curr=(curr+9)%10;
            }
            printf("\nACT: %d\n", act_mask);
            if((act_mask & num_mask[j])!=0)
            {
                result[j]=0;
                printf("NOT: %d\n", j);
            }
        }
    }

    printf("\nFINALLY:\n");
    for(j=0; j<10; j++)
    {
        if(result[j]!=0)printf("RES: %d\n",j);
    }
    return 0;
}
