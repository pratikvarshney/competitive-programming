#include<stdio.h>

int num_mask[10]={0b1111110, 0b0110000, 0b1101101, 0b1111001, 0b0110011, 0b1011011, 0b1011111, 0b1110000, 0b1111111, 0b1111011};
int rev_mask[10]={0b0000001, 0b1001111, 0b0010010, 0b0000110, 0b1001100, 0b0100100, 0b0100000, 0b0001111, 0b0000000, 0b0000100};
int pos_mask[7]={0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001};

FILE *fp, *outp;

int main()
{
    int t,i,j,n,c_no,temp,div;
    int result[10];
    int res_mask;

    fp=fopen("sample.in","r");
    outp=fopen("output.txt","w");

    fscanf(fp, "%d", &t);

    for(c_no=1; c_no<=t; c_no++)
    {
        fscanf(fp, "%d", &n);
        fprintf(outp, "Case #%d: ", c_no);
        printf("Case #%d: ", c_no);

        int input[n];
        res_mask=0;
        for(i=0; i<n; i++)
        {
            fscanf(fp, "%d", &temp);
            input[i]=0;
            div=1;
            while(temp>0)
            {
                input[i] += ((temp%10)*div);
                temp/=10;
                div*=2;
            }
            res_mask = res_mask | input[i];
        }

        for(i=0; i<10; i++)
        {
            result[i]=1;
        }

        for(i=0; i<n; i++)
        {
            for(j=0; j<10; j++)
            {
                if((input[i] & rev_mask[j])!=0)
                {
                    result[((10+j)-((n-i)%10))%10]=0;
                }
            }
        }
        int flag=0;
        int curr;

        for(j=0; j<10; j++)
        {
            if(result[j]!=0)
            {
                curr=(j+n)%10;
                int act_mask=0b1111111;
                for(i=0; i<n; i++)
                {
                    act_mask = act_mask & rev_mask[curr];
                    if((num_mask[curr] & res_mask)!=input[i])
                    {
                        result[j]=0;
                    }
                    curr=(curr+9)%10;
                }
                if((act_mask & num_mask[j])!=0) result[j]=0;
            }
        }
        temp=0;
        for(j=0; j<10; j++)
        {
            if(result[j]!=0) temp++;
        }
        if(temp!=1)
        {
            fprintf(outp, "ERROR!\n");
            printf("ERROR!, %x\n", res_mask);
        }
        else
        {
            char buffer[33];
            for(j=0; j<10; j++)
            {
                if(result[j]!=0)
                {
                    itoa((num_mask[j] & res_mask), buffer, 2);
                    temp=atoi(buffer);
                    printf("%d, %x\n",j, res_mask);
                    break;
                }
            }
            fprintf(outp, "%07d\n", temp);
            //printf("%07d\n", temp);
        }
    }

    return 0;
}
