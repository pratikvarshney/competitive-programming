#include<stdio.h>

#define MAX_MASK 0b1111111

int num_mask[10]={0b1111110, 0b0110000, 0b1101101, 0b1111001, 0b0110011, 0b1011011, 0b1011111, 0b1110000, 0b1111111, 0b1111011};
int pos_mask[7]={0b1000000, 0b0100000, 0b0010000, 0b0001000, 0b0000100, 0b0000010, 0b0000001};

int show_led(int led_code);

int main()
{
    int i;
    for(i=0; i<10; i++)
        show_led(num_mask[i]);
    return 0;
}

int show_led(int c)
{

    printf("\n");
    printf(" %c", ((c & pos_mask[0])==0)?' ':'_'); //A

    printf("\n");
    printf("%c", ((c & pos_mask[5])==0)?' ':'|'); //F
    printf(" %c", ((c & pos_mask[1])==0)?' ':'|'); //B

    printf("\n");
    printf(" %c", ((c & pos_mask[6])==0)?' ':'-'); //G

    printf("\n");
    printf("%c", ((c & pos_mask[4])==0)?' ':'|'); //E
    printf(" %c", ((c & pos_mask[2])==0)?' ':'|'); //C

    printf("\n");
    printf(" %c", ((c & pos_mask[3])==0)?' ':'"'); //D

    printf("\n");

    return 0;
}
