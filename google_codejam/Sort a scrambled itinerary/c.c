#include<stdio.h>
#include<string.h>

struct node_list
{
    char *a;
    char *b;
};

typedef struct node_list list;

int main()
{
    int t, n, c, i, j;
    char str[10];
    char *curr;
    scanf("%d", &t);
    for(c=1; c<=t; c++)
    {
        printf("Case #%d:", c);
        scanf("%d", &n);
        list node[n];
        for(i=0; i<n; i++)
        {
            scanf("%s", str);
            node[i].a = strdup(str);
            scanf("%s", str);
            node[i].b = strdup(str);
        }
        int nxt[n];
        int pre[n];
        for(i=0; i<n; i++) pre[i]=-1;
        for(i=0; i<n; i++) nxt[i]=n;
        for(i=0; i<n; i++)
        {
            curr = node[i].b;
            for(j=0; j<n; j++)
            {
                if(strcmp(curr, node[j].a)==0)
                {
                    nxt[i]=j;
                    pre[j]=i;
                    break;
                }
            }
        }
        i=0;
        while(pre[i]!=-1) i++;
        do
        {
            printf(" %s-%s", node[i].a, node[i].b);
            i = nxt[i];
        }while(i!=n);
        printf("\n");

    }
    return 0;
}
