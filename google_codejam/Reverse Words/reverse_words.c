#include<stdio.h>
#include<string.h>

FILE *fp, *outp;

void rec_show(char *tok);

int main()
{
    int N, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &N);

    for(c_no=1; c_no<=N; c_no++)
    {
        char input[1001];
        char *tok;
        fscanf(fp, "%[\n]", input);
        fscanf(fp, "%[^\n]", input);
        tok=strtok(input, " ");
        fprintf(outp, "Case #%d:", c_no);
        rec_show(tok);
        fprintf(outp, "\n");
    }


    return 0;
}

void rec_show(char *tok)
{
    char *temp;
    if(tok!=NULL)
    {
        temp=strdup(tok);
        tok=strtok(NULL, " ");
        rec_show(tok);
        fprintf(outp, " %s", temp);
    }
}
