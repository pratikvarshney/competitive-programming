#include<iostream>
#include<cstdio>
#include<vector>
using namespace std;

int main()
{
    int s, s2, t, c, p;
    int i, j, k, x;
    int r=0, d=0;
    scanf("%d", &t);
    for(c=1; c<=t; c++)
    {
        scanf("%d", &s);
        s2 = s*s;
        int maxr=1, maxd=1;
        vector<int> a(s2, 0);
        vector<int> n(s2, 1);
        vector<int> b(s2+1, 0);

        for(i=0; i<s2; i++)
        {
            cin >> a[i];
            b[a[i]]=i;
        }

        for(x=s2; x>0; x--)
        {
            k=b[x];
            d=n[k];
            d++;
            if(k>=s) //up
            {
                p = k-s;
                if((n[p]<d) && (a[p]==(x-1)))
                {
                     n[p]=d;
                     if(maxd<=d){ maxd=d; maxr=a[p];}
                }
            }
            if(k< ((s-1)*s)) //down
            {
                p = k+s;
                if((n[p]<d)  && (a[p]==(x-1)))
                {
                    n[p]=d;
                    if(maxd<=d){ maxd=d; maxr=a[p];}
                }
            }
            if((k%s)!=0) //left
            {
                p = k-1;
                if((n[p]<d)  && (a[p]==(x-1)))
                {
                    n[p]=d;
                    if(maxd<=d){ maxd=d; maxr=a[p];}
                }
            }
            if((k%s)!=(s-1)) //left
            {
                p = k+1;
                if((n[p]<d)  && (a[p]==(x-1)))
                {
                    n[p]=d;
                    if(maxd<=d){ maxd=d; maxr=a[p];}
                }
            }
        }


        printf("Case #%d: %d %d\n", c, maxr, maxd);
    }

    return 0;
}
