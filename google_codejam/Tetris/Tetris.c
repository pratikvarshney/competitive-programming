#include<stdio.h>
//#include<conio.h>

#define NUM_TET 7

FILE *fp, *outp;

struct tetromino
{
    int w;
    int h;
    char **mat;
}tm[NUM_TET][4];

int init_tm();
int set_tm(int i, int w, int h, char *str);

int main()
{
    init_tm();

    int T, c_no;
    fp=fopen("sample.in","r");
    outp=fopen("output.out","w");

    fscanf(fp, "%d", &T);

    for(c_no=1; c_no<=T; c_no++)
    {
        int w, h, n;
        int t, r, x;
        int p, q;
        int i, j, y;
        int game_over=0;
        fscanf(fp, "%d%d%d", &w, &h, &n);
        char mat[h][w];
        for(i=0; i<h; i++)
        {
            for(j=0; j<w; j++)
            {
                mat[i][j]='.';
            }
        }
        for(i=0; i<n; i++)
        {
            fscanf(fp, "%d%d%d", &t, &r, &x);
            t--;
            if(!game_over)
            {
                int flag=0;
                int max = (h-(tm[t][r].h));
                for(y=0; y<=max; y++)
                {
                    flag = 0;
                    for(p=0; p<tm[t][r].h; p++)
                    {
                        for(q=0; q<tm[t][r].w; q++)
                        {
                            if(mat[y+p][x+q]=='x' && tm[t][r].mat[p][q]=='x')
                            {
                                flag=1;
                                break;
                            }
                        }
                        if(flag) break;
                    }
                    if(flag) break;
                }
                y--;

                if(y<0)
                {
                    game_over=1;
                    /*printf("Game Over!\n");
                    getch();*/
                    continue;
                }
                else
                {
                    for(p=0; p<tm[t][r].h; p++)
                    {
                        for(q=0; q<tm[t][r].w; q++)
                        {
                            if(tm[t][r].mat[p][q]=='x')
                            {
                                mat[y+p][x+q]='x';
                            }
                        }
                    }
                    char ch;
                    int rc[2];
                    for(p=h-1; p>=0; p--)
                    {
                        rc[0]=0;
                        rc[1]=0;
                        for(q=0; q<w; q++)
                        {
                            ch=mat[p][q];
                            if(ch=='x') rc[0]++;
                            if(ch=='.') rc[1]++;
                        }
                        if(rc[0]==w)
                        {
                            //clear line
                            int r;
                            for(r=p; r>0; r--)
                            {
                                for(q=0; q<w; q++)
                                {
                                    mat[r][q]=mat[r-1][q];
                                }
                            }
                            for(q=0; q<w; q++)
                            {
                                mat[0][q]='.';
                            }
                            p++;
                        }
                        if(rc[1]==w) break;
                    }
                }
                //show mat
                /*system("cls");
                for(p=0; p<h; p++)
                {
                    for(q=0; q<w; q++)
                    {
                        putchar(mat[p][q]);
                    }
                    printf("\n");
                }
                getch();
                */
            }
        }
        //write
        fprintf(outp, "Case #%d:\n", c_no);
        if(game_over) fprintf(outp, "Game Over!\n");
        else
        {
            for(p=0; p<h; p++)
            {
                for(q=0; q<w; q++)
                {
                    fprintf(outp, "%c", mat[p][q]);
                }
                fprintf(outp, "\n");
            }
        }
    }

    return 0;
}

int init_tm()
{
    set_tm(0, 2, 3, "x xx x");
    set_tm(1, 2, 3, " xxxx ");
    set_tm(2, 2, 3, "x x xx");
    set_tm(3, 2, 3, " x xxx");
    set_tm(4, 2, 2, "xxxx");
    set_tm(5, 1, 4, "xxxx");
    set_tm(6, 3, 2, " x xxx");
    return 0;
}

int set_tm(int i, int w, int h, char *str)
{
    int m, n, j, k, temp;

    j=0;

    tm[i][j].w = w;
    tm[i][j].h = h;

    tm[i][j].mat = (int **)malloc(h*sizeof(int *));
    for(m=0; m<h; m++)
    {
        tm[i][j].mat[m] = (int *)malloc(w*sizeof(int));
    }
    k=0;
    for(m=0; m<h; m++)
    {
        for(n=0; n<w; n++)
        {
            tm[i][j].mat[m][n]=str[k];
            k++;
        }
    }

    j=1;
    temp=w;
    w=h;
    h=temp;

    tm[i][j].w = w;
    tm[i][j].h = h;

    tm[i][j].mat = (int **)malloc(h*sizeof(int *));
    for(m=0; m<h; m++)
    {
        tm[i][j].mat[m] = (int *)malloc(w*sizeof(int));
    }
    k=0;
    for(n=0; n<w; n++)
    {
        for(m=h-1; m>=0; m--)
        {
            tm[i][j].mat[m][n]=str[k];
            k++;
        }
    }

    j=2;
    temp=w;
    w=h;
    h=temp;

    tm[i][j].w = w;
    tm[i][j].h = h;

    tm[i][j].mat = (int **)malloc(h*sizeof(int *));
    for(m=0; m<h; m++)
    {
        tm[i][j].mat[m] = (int *)malloc(w*sizeof(int));
    }
    k=0;
    for(m=h-1; m>=0; m--)
    {
        for(n=w-1; n>=0; n--)
        {
            tm[i][j].mat[m][n]=str[k];
            k++;
        }
    }

    j=3;
    temp=w;
    w=h;
    h=temp;

    tm[i][j].w = w;
    tm[i][j].h = h;

    tm[i][j].mat = (int **)malloc(h*sizeof(int *));
    for(m=0; m<h; m++)
    {
        tm[i][j].mat[m] = (int *)malloc(w*sizeof(int));
    }
    k=0;
    for(n=w-1; n>=0; n--)
    {
        for(m=0; m<h; m++)
        {
            tm[i][j].mat[m][n]=str[k];
            k++;
        }
    }

    //show
    /*for(j=0; j<4; j++)
    {
        for(m=0; m<tm[i][j].h; m++)
        {
            for(n=0; n<tm[i][j].w; n++)
            {
                putchar(tm[i][j].mat[m][n]);
            }
            printf("\n");
        }
        printf("\n");
    }

    printf("\n----------------------------------------\n\n");*/

    return 0;
}
