import java.util.Scanner;
import java.math.BigInteger;

class CANDY3
{
	public static void main(String[] arr)
	{
		BigInteger a, sum, n;
		BigInteger bigOne = BigInteger.valueOf(1);
		int t;
		String str;
		Scanner sc = new Scanner(System.in);
		t = sc.nextInt();
		str = sc.nextLine();
		str = sc.nextLine();
		
		while(t>0)
		{
			n=BigInteger.valueOf(0);
			sum=BigInteger.valueOf(0);
			while(sc.hasNext())
			{
				str = sc.nextLine();
				if(str.length()==0) break;
				
				sum = sum.add(new BigInteger(str));
				n=n.add(bigOne);
			}
			if(sum.mod(n).compareTo(BigInteger.valueOf(0)) == 0)
			{
				System.out.println("YES");
			}
			else System.out.println("NO");
			
			t--;
		}
	}
}