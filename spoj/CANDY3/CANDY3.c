#include<stdio.h>

#define MAX_SIZE 16

int main()
{
    char str[MAX_SIZE];
    long long int arr[256000];
    int i, n, t, b;

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%d", &n);
        for(i=0; i<n; i++)
        {
            scanf("%lld", &(arr[i]));
        }
        b=0;
        for(i=0; i<n; i++)
        {
            b = (b+arr[i])%n;
        }
        if(b==0)
        {
            printf("YES\n");
        }
        else
        {
            printf("NO\n");
        }
        t--;
    }

    return 0;
}
