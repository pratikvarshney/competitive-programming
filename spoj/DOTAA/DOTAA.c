#include<stdio.h>

int main()
{
    int t, n, m, d, h, capacity, i;

    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d%d", &n, &m, &d);
        capacity = 0;
        for(i=0; i<n; i++)
        {
            scanf("%d", &h);
            capacity += ((h-1)/d);
        }
        if(capacity >= m)
            printf("YES\n");
        else
            printf("NO\n");
    }

    return 0;
}
