#include<stdio.h>

#define MAX_COUNT 11
#define MAX_SIZE 1001

int counting_sort_inplace(int arr[], int size);

int main()
{
    int i, n, t, sum;
    int a[MAX_SIZE], b[MAX_SIZE];

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%d", &n);
        for(i=0; i<n; i++)
        {
            scanf("%d", &(a[i]));
        }
        for(i=0; i<n; i++)
        {
            scanf("%d", &(b[i]));
        }
        counting_sort_inplace(a, n);
        counting_sort_inplace(b, n);

        sum=0;
        for(i=0; i<n; i++)
        {
            sum += (a[i]*b[i]);
        }
        printf("%d\n", sum);
        t--;
    }

    return 0;
}

int counting_sort_inplace(int a[], int size)
{
    int b[size];
    int c[MAX_COUNT]={0};
    int i, j;

    for(i=0; i<size; i++)
    {
        c[a[i]]++;
    }
    for(i=1; i<MAX_COUNT; i++)
    {
        c[i] += c[i-1];
    }
    for(i=size-1; i>=0; i--)
    {
        b[c[a[i]]-1]=a[i];
        c[a[i]]--;
    }
    for(i=0; i<size; i++)
    {
        a[i]=b[i];
    }
    return 0;
}
