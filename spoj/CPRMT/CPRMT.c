#include<stdio.h>

#define MAXSIZE 2002

int proc(char *a, char *b, char *x);

int main()
{
    char a[MAXSIZE], b[MAXSIZE], x[MAXSIZE];

    while(!feof(stdin))
    {
        scanf("%s%s", a, b);
        proc(a, b, x);
        printf("%s\n", x);
    }

    return 0;
}

int proc(char *a, char *b, char *x)
{
    int p[26]={0};
    int q[26]={0};
    int r[26]={0};
    int i, j, k;
    for(i=0; a[i]!='\0'; i++) p[a[i]-'a']++;
    for(i=0; b[i]!='\0'; i++) q[b[i]-'a']++;
    for(i=0; i<26; i++) r[i] = (p[i]<q[i])?p[i]:q[i];
    k=0;
    for(i=0; i<26; i++)
    {
        for(j=0; j<r[i]; j++)
        {
            x[k]='a'+i;
            k++;
        }
    }
    x[k]='\0';
    return 0;
}
