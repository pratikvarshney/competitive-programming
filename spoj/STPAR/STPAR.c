#include<stdio.h>

#define MAX_SIZE 1000

int main()
{
    int stk_a[MAX_SIZE], stk_top;
    int n, i, val;
    int req, found;

    while(1)
    {
        scanf("%d", &n);
        if(n==0) break;

        req=1;
        stk_top = -1;
        for(i=0; i<n; i++)
        {
            scanf("%d", &val);

            //pop stack
            while(stk_top>=0)
            {
                if(stk_a[stk_top]==req)
                {
                    stk_top--;
                    req++;
                }
                else break;
            }
            if(val==req)
            {
                req++;
            }
            else
            {
                //push to stack
                stk_top++;
                stk_a[stk_top]=val;
            }
        }
        found=0;
        while(req<=n)
        {
            if(stk_a[stk_top]==req)
            {
                req++;
                stk_top--;
            }
            else
            {
                found = 1;
                break;
            }
        }
        if(found) printf("no\n");
        else printf("yes\n");
    }
    return 0;
}
