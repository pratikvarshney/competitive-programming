/*
 * main.cpp
 *
 *  Created on: 01-Mar-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <vector>
#include <climits>
#include <unordered_set>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

struct node {
	int val;
	int priority;
	node() {
	}
	node(int v, int r) {
		val = v;
		priority = r;
	}
	void assign(const node& copy) {
		val = copy.val;
		priority = copy.priority;
	}
	void set(int v, int r) {
		val = v;
		priority = r;
	}
};

node q[1002];
int pos[1002];

class min_priority_queue {
private:
	int size;

	void swap(int i, int j) {
		node tmp;
		tmp.assign(q[i]);
		q[i].assign(q[j]);
		q[j].assign(tmp);
		pos[q[i].val] = i;
		pos[q[j].val] = j;
	}

	void heapify_up(int i) {
		while (q[i].priority < q[i / 2].priority) {
			swap(i, i / 2);
			i /= 2;
		}
	}

	void heapify_down(int i) {
		while (2 * i < size) {
			int min_child = 2 * i; // 1st child
			if (min_child + 1 < size
					&& q[min_child + 1].priority < q[min_child].priority) {
				min_child++; // 2nd child
			}
			if (q[i].priority <= q[min_child].priority) {
				return;
			}
			swap(i, min_child);
			i = min_child;
		}
	}

public:
	min_priority_queue(int n) {
		q[0].set(0, -1); // adding 1 extra elem to make 1 base indexing
		size = 1;
		for (int i = 1; i < n; i++) {
			pos[i] = 0;
		}
	}
	bool empty() {
		return size <= 1; // for 1 based indexing
	}
	void add_with_priority(int v, int p) {
		// append to last
		int curr = size;
		pos[v] = curr;
		q[size++].set(v, p);

		heapify_up(curr);
	}
	void decrease_priority(int v, int p) {
		int pos_v = pos[v];
		if (p < q[pos_v].priority) {
			q[pos_v].priority = p;
			heapify_up(pos_v);
		}
	}
	int extract_min() {
		int min = q[1].val;
		size--;
		swap(1, size);
		pos[min] = 0;
		heapify_down(1);
		return min;
	}
	int min_priority() {
		return q[1].priority;
	}
};

int mst(vector<vector<pair<int, int> > >& graph, int a, int b, int c) {
	int len = c;
	unordered_set<int> visited;
	visited.insert(a);
	visited.insert(b);

	// add all elem with inf priority
	min_priority_queue q(graph.size());
	for (int i = 1; i < graph.size(); i++) {
		if (i != a && i != b) {
			q.add_with_priority(i, INT_MAX);
		}
	}
	// update elem connected to a or b
	for (auto p : graph[a]) {
		q.decrease_priority(p.first, p.second);
	}
	for (auto p : graph[b]) {
		q.decrease_priority(p.first, p.second);
	}

	for (int i = 3; i < graph.size(); i++) {
		len += q.min_priority();
		int u = q.extract_min();
		for (auto p : graph[u]) {
			q.decrease_priority(p.first, p.second);
		}
	}

	return len;
}

void solve() {
	int p, n, m, a, b, c;
	scanint(p);
	scanint(n);
	scanint(m);
	vector<vector<pair<int, int> > > graph(n + 1);
	int min_a, min_b, min_c = INT_MAX;
	for (int i = 0; i < m; i++) {
		scanint(a);
		scanint(b);
		scanint(c);
		graph[a].emplace_back(b, c);
		graph[b].emplace_back(a, c);
		if (c < min_c) {
			min_c = c;
			min_a = a;
			min_b = b;
		}
	}
	printf("%d\n", p * mst(graph, min_a, min_b, min_c));
}

int main() {
	int t;
	scanint(t);
	while (t--) {
		solve();
	}
}
