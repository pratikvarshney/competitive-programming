#include<stdio.h>

#define MAXSIZE 408

void infixToPostfix(char infix[], char postfix[]);

int main()
{
    int t;
    char infix[MAXSIZE], postfix[MAXSIZE];

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%s", infix);
        infixToPostfix(infix, postfix);
        printf("%s\n", postfix);
        t--;
    }

    return 0;
}


void infixToPostfix(char infix[], char postfix[])
{
    char mystack[MAXSIZE], ch;
    int mystack_pos, infix_pos, postfix_pos=0, flag, infix_len;
    mystack[0]='(';
    mystack_pos=1;
    infix_len = strlen(infix);
    infix[infix_len] = ')';
    for(infix_pos=0;mystack_pos!=0;infix_pos++)
    {
        ch = infix[infix_pos];
        if( ch == '(' )
        {
            mystack[mystack_pos] = ch;
            mystack_pos++;
        }
        else if( ch == '^' )
        {
            while(mystack[mystack_pos-1] == '^')
            {
                postfix[postfix_pos] = mystack[mystack_pos-1];
                postfix_pos++;
                mystack_pos--;
            }
            mystack[mystack_pos] = ch;
            mystack_pos++;
        }
        else if( (ch == '*') || (ch == '/') )
        {
            flag = 0;
            while(flag == 0)
            {
                switch(mystack[mystack_pos-1])
                {
                    case '^':
                    case '*':
                    case '/':
                        postfix[postfix_pos] = mystack[mystack_pos-1];
                        postfix_pos++;
                        mystack_pos--;
                    break;
                    default:
                        flag = 1;
                    break;
                }
            }
            mystack[mystack_pos] = ch;
            mystack_pos++;
        }
        else if( (ch == '+') || (ch == '-') )
        {
            flag = 0;
            while(flag == 0)
            {
                switch(mystack[mystack_pos-1])
                {
                    case '^':
                    case '*':
                    case '/':
                    case '+':
                    case '-':
                        postfix[postfix_pos] = mystack[mystack_pos-1];
                        postfix_pos++;
                        mystack_pos--;
                    break;
                    default:
                        flag = 1;
                    break;
                }
            }
            mystack[mystack_pos] = ch;
            mystack_pos++;
        }
        else if( ch == ')' )
        {
            while(mystack[mystack_pos-1] != '(')
            {
                postfix[postfix_pos] = mystack[mystack_pos-1];
                postfix_pos++;
                mystack_pos--;
            }
            mystack_pos--; /* remove ( from stack */
        }
        else
        {
            postfix[postfix_pos] = ch;
            postfix_pos++;
        }
        //view stack changes
        /*printf("\n\nSCANNED SYMBOL: %c",ch);
        postfix[postfix_pos] = '\0';
        mystack[mystack_pos] = '\0';
        printf("\nSTACK:    %s",mystack);
        printf("\nPOSTFIX: %s",postfix);*/
    }
    infix[infix_len] = '\0';
    postfix[postfix_pos] = '\0';
}
