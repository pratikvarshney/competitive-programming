/*
 * main.cpp
 *
 *  Created on: 05-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> prime;

vector<pair<int, int> > getPrimeFactors(long long n) {
	vector<pair<int, int> > f;

	int m = (int) ceil(sqrt(n));
	for (auto p : prime) {
		if (p > m || n <= 1) {
			break;
		}
		if ((n % p) == 0) {
			int count = 1;
			n /= p;
			while ((n % p) == 0) {
				n /= p;
				count++;
			}
			f.emplace_back(p, count);
		}
	}
	if (n > 1) {
		f.emplace_back(n, 1);
	}

	return f;
}

void init4DigitPrimes() {
	// init
	int limit = (10e6) + 1;
	int sievebound = (limit - 1) / 2;
	bool sieve[sievebound + 1] = { 0 };
	int crosslimit = sqrt(limit - 1) / 2;
	int i, j;
	for (i = 1; i <= crosslimit; i++) {
		if (!sieve[i]) {
			for (j = 2 * i * (i + 1); j <= sievebound; j += (2 * i) + 1) {
				sieve[j] = 1;
			}
		}
	}
	prime.push_back(2);

	for (i = 1; i <= sievebound; i++) {
		if (!sieve[i]) {
			prime.push_back((2 * i) + 1);
		}
	}
}

bool check(long long n) {
	vector<pair<int, int> > factor = getPrimeFactors(n);
	for (auto f : factor) {
		if ((f.first % 4) == 3) {
			if (f.second & 1) { // odd
				return false;
			}
		}
	}
	return true;
}

int main() {
	init4DigitPrimes();
	int c;
	scanf("%d", &c);
	for (int i = 0; i < c; i++) {
		long long n;
		scanf("%lld", &n);
		if (check(n)) {
			printf("Yes\n");
		} else {
			printf("No\n");
		}
	}
	return 0;
}
