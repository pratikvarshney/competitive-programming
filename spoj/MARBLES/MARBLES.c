#include<stdio.h>

unsigned long long int ncr(unsigned long long int n, unsigned long long int r);
unsigned long long int gcd(unsigned long long int a, unsigned long long int b);

int main()
{
    int t;
    unsigned long long int n, k;
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%llu%llu", &n, &k);
        printf("%llu\n", ncr(n-1, k-1));
    }
    return 0;
}

unsigned long long int ncr(unsigned long long int n, unsigned long long int r)
{
    unsigned long long int i, j, k;
    unsigned long long int *nr, *dr;
    int found = 0;

    if(r > n/2) r = n-r; //c(n, r) = c(n, n-r)
    if(r==0) return 1;


    nr = (unsigned long long int *) malloc(r*sizeof(unsigned long long int));
    dr = (unsigned long long int *) malloc(r*sizeof(unsigned long long int));
    //fill numerator
    for(i=0; i<r; i++)
    {
        nr[i] = n-i;
        dr[i] = i+1;
    }

    //process
    for(i=0; i<r; i++)
    {
        while(dr[i] != 1)
        {
            for(j=0; j<r; j++)
            {
                k = gcd(nr[j], dr[i]);
                dr[i] /= k;
                nr[j] /= k;
            }
        }
    }

    //calc
    j=nr[0];
    for(i=1; i<r; i++)
    {
        j *= (unsigned long long int)nr[i];
    }

    free(nr);
    free(dr);
    return j;
}

unsigned long long int gcd(unsigned long long int a, unsigned long long int b)
{
	if( (a==0) || (b==0) )
	{
		return a+b;
	}
	return gcd(b, (a%b));
}
