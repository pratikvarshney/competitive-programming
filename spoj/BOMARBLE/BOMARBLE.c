#include<stdio.h>

#define MAXM 1001

int main()
{
    int m[MAXM];
    int i, n;

    //init
    m[0]=1;
    for(i=1; i<MAXM; i++)
    {
        m[i] = m[i-1] + (3*i) + 1;
    }

    while(1)
    {
        scanf("%d", &n);
        if(n==0) break;

        printf("%d\n", m[n]);
    }
    return 0;
}
