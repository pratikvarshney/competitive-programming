 #include<stdio.h>
 #include<math.h>

int main()
{
    int e, f, s, m, e2, f2, s2, m2;
    int c, i, k;

    while(1)
    {
        scanf("%d%d%d%d%d%d%d%d", &e, &f, &s, &m, &e2, &f2, &s2, &m2);
        if(e==-1) break;

        k = ceil((double)e/e2);
        c = ceil((double)f/f2);
        if(k<c) k=c;
        c = ceil((double)s/s2);
        if(k<c) k=c;
        c = ceil((double)m/m2);
        if(k<c) k=c;

        printf("%d %d %d %d\n", (e2 * k) - e, (f2 * k) - f, (s2 * k) - s, (m2 * k) - m);
    }

    return 0;
}
