#include<stdio.h>
#include<math.h>

int main()
{
    char str[5];
    int n, i, p, f;
    while(1)
    {
        scanf("%s", str);
        if(str[0]=='0' && str[1]=='0' && str[3]=='0') break;

        n = ((str[0]-'0')*10)+(str[1]-'0');
        for(i='0'; i<str[3]; i++)
        {
            n *= 10;
        }

        //calc josephus for k=2
        p = floor(log((double)n)/log(2.0));
        f = (2*(n - (1<<p)))+1;

        printf("%d\n", f);
    }
    return 0;
}
