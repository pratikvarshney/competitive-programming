#include<stdio.h>
//group of 4
#define GROUP 10000
//also remember to reset %0nld in printf

int fact(int n);

int main()
{
    int t, n;

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%d", &n);
        fact(n);
        t--;
    }

    return 0;
}

int fact(int n)
{

    long result[50], carry;
    int grp=1, i, j;
    result[0]=1;
    for(i=1; i<=n; i++)
    {
        carry=0;
        for(j=0; j<grp; j++)
        {
            result[j]*=i;
            result[j]+=carry;
            if(result[j]>=GROUP)
            {
                carry=result[j]/GROUP;
                result[j]=result[j]%GROUP;
            }
            else
            {
                carry=0;
            }
        }
        if(carry)
        {
            result[grp]=carry;
            grp++;
        }
    }
    grp--;
    printf("%ld",result[grp]);
    for(i=grp-1; i>=0; i--)
    {
        printf("%04ld",result[i]);
    }
    printf("\n");
}
