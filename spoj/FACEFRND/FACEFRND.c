//NOTE: input might contain some 5-digit IDs
#include<stdio.h>

#define MAXVAL 100000

int main()
{
    int ff[MAXVAL]={0};
    int f[200];
    int n, m, i, j, k, count=0;
    scanf("%d", &n);
    for(i=1000; i<10000; i++) ff[i]=0;
    for(i=0; i<n; i++)
    {
        scanf("%d", &(f[i]));
        scanf("%d", &m);
        for(j=0; j<m; j++)
        {
            scanf("%d", &k);
            if(ff[k]!=1) count++;
            ff[k]=1;
        }
    }
    for(i=0; i<n; i++)
    {
        if(ff[f[i]]==1) count--;
        ff[f[i]]=0;
    }
    printf("%d\n", count);
    return 0;
}
