#include<stdio.h>

int main()
{
    int t, n, p;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%d%d", &n, &p);
        if(p)puts("Pagfloyd wins.");
        else puts("Airborne wins.");
    }
    return 0;
}
