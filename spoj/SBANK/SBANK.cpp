#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>
#include<string>

bool myfunction (std::string i,std::string j);

int main()
{
    int t, n, i, k;
    std::string str, pre;
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d\n", &n);
        std::vector<std::string> arr(n);
        for(i=0; i<n; i++)
        {
            std::getline(std::cin,arr[i]);
        }

        std::sort(arr.begin(), arr.end(), myfunction);

        k=1;
        std::cout << arr[0];
        pre = arr[0];
        for(i=1; i<n; i++)
        {
            if(pre.compare(arr[i])==0)
            {
                k++;
            }
            else
            {
                printf("%d\n", k);
                k=1;
                std::cout << arr[i];
                pre = arr[i];
            }
        }
        printf("%d\n", k);
        if(t>1) printf("\n");
    }
    return 0;
}

bool myfunction (std::string i,std::string j)
{
    return i.compare(j)<0;
}
