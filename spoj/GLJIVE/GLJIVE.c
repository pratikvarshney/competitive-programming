#include<stdio.h>

int main()
{
    int i, val, sum=0;

    for(i=0; i<10; i++)
    {
        scanf("%d", &val);
        if( (100-sum) >= ((sum+val)-100) )
        {
            sum += val;
        }
        else break;
    }
    printf("%d\n", sum);
    return 0;
}
