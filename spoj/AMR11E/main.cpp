/*
 * main.cpp
 *
 *  Created on: 23-Feb-2017
 *      Author: Pratik Varshney
 */

#include<stdio.h>

int p[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 }; // list of primes till sqrt(1000) = 32

int numDistinctPrimeFactors(int i) {
	int count = 0;
	for (int k = 0; k < 11; k++) {
		if (i % p[k] == 0) {
			count++;
			while (i % p[k] == 0) {
				i /= p[k];
			}
		}
	}
	if (i > 1) {
		count++;
	}
	return count;
}

int main() {
	int v[1001];
	for (int i = 2 * 3 * 5, k = 0; k < 1000; i++) {
		if (numDistinctPrimeFactors(i) >= 3) {
			k++;
			v[k] = i;
		}
	}
	int t, n;
	scanf("%d", &t);
	while (t--) {
		scanf("%d", &n);
		printf("%d\n", v[n]);
	}
}
