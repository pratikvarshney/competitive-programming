#include<stdio.h>
#include<string.h>

#define MAXSIZE 256

int check_cpp(char str[MAXSIZE]);
int cpp_to_java(char str[MAXSIZE], char res[MAXSIZE]);
int check_java(char str[MAXSIZE]);
int java_to_cpp(char str[MAXSIZE], char res[MAXSIZE]);

int main()
{
    char str[MAXSIZE], res[MAXSIZE];

    while(!feof(stdin))
    {
        gets(str);
        if(strlen(str)==0) continue;

        if(check_cpp(str)==0)
        {
            cpp_to_java(str, res);
            printf("%s\n", res);
        }
        else if(check_java(str)==0)
        {
            java_to_cpp(str, res);
            printf("%s\n", res);
        }
        else
        {
            printf("Error!\n");
        }
    }
    return 0;
}

int check_cpp(char str[MAXSIZE])
{
    int i;
    if(!((str[0]>='a') && (str[0]<='z'))) return -1;

    for(i=1; str[i]!='\0'; i++)
    {
        if(str[i] == '_')
        {
            if(!((str[i+1]>='a') && (str[i+1]<='z'))) return -1;
        }
        else if(!((str[i]>='a') && (str[i]<='z'))) return -1;
    }


    return 0;
}

int cpp_to_java(char str[MAXSIZE], char res[MAXSIZE])
{
    int i, j;
    int cap=0;
    for(i=0, j=0; str[i]!='\0'; i++)
    {
        if(str[i]=='_')
        {
            cap = 1;
            continue;
        }
        if(cap == 1)
        {
            cap = 0;
            res[j] = str[i] + ('A' - 'a');
        }
        else
        {
            res[j] = str[i];
        }
        j++;
    }
    res[j] = '\0';
    return 0;
}

int check_java(char str[MAXSIZE])
{
    int i;

    if(!((str[0]>='a') && (str[0]<='z'))) return -1;

    for(i=1; str[i] != '\0'; i++)
    {
        if((str[i]>='A') && (str[i]<='Z'))
        {
            //ok
        }
        else if((str[i]>='a') && (str[i]<='z'))
        {
            //ok
        }
        else
        {
            return -1; //error
        }
    }
    return 0;
}

int java_to_cpp(char str[MAXSIZE], char res[MAXSIZE])
{
    int i, j;
    int cap=0;
    for(i=0, j=0; str[i]!='\0'; i++)
    {
        if((str[i]>='A') && (str[i]<='Z'))
        {
            res[j] = '_';
            j++;
            res[j] = str[i] + ('a' - 'A');
        }
        else
        {
            res[j] = str[i];
        }
        j++;
    }
    res[j] = '\0';
    return 0;
}
