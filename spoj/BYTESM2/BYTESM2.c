#include<stdio.h>
#define max(a,b) (a>b)?a:b

int main()
{
    int t, h, w, i, j, m;
    int mat[102][102];
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d", &h, &w);
        for(i=1; i<=h; i++)
        {
            for(j=1; j<=w; j++)
            {
                scanf("%d", &(mat[i][j]));
            }
        }
        //make border = 0

        /*
        //row 0
        i=0;
        for(j=0; j<=(w+1); j++) mat[i][j]=0;


        //row h+1
        i=h+1;
        for(j=0; j<=(w+1); j++) mat[i][j]=0;
        */

        //col 0
        j=0;
        for(i=0; i<=(h+1); i++) mat[i][j]=0;

        //col w+1
        j=w+1;
        for(i=0; i<=(h+1); i++) mat[i][j]=0;

        for(i=h; i>1; i--)
        {
            for(j=1; j<=w; j++)
            {
                //max
                m = max(mat[i][j-1], mat[i][j+1]);
                m = max(m, mat[i][j]);

                mat[i-1][j] += m;
            }
        }

        m = mat[1][1];
        for(j=1; j<=w; j++) m = max(m, mat[1][j]);

        printf("%d\n", m);
    }
    return 0;
}
