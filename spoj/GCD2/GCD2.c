#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define MAXCHAR 256

int gcd(int a, int b);

int mod(char str[MAXCHAR], int a);


int main()
{
    char str[MAXCHAR];
    int a, t;
    scanf("%d", &t);
    for(;t>0; t--)
    {
        scanf("%d%s", &a, str);
        if(a!=0) printf("%d\n", gcd(a, mod(str,a)));
        else printf("%s\n", str);
    }
    return 0;
}

int gcd(int a, int b)
{
	if (b==0)
		return a;
	else
		return gcd(b,a%b);
}

int mod(char str[MAXCHAR], int a)
{
    int len, i;
    int val;

    len = strlen(str);
    val = 0;
    for(i=0; i<len; i++)
    {
        val = ( (val*10) + (str[i]-'0') ) % a;
    }
    return val;
}
