#include<stdio.h>

int main()
{
    int b[11], c[11];
    int min_d1, min_d2, min_a1;
    int a, d;
    int i;

    while(1)
    {
        scanf("%d%d", &a, &d);
        if(a==0 && d==0) break;

        min_a1 = 100000;
        for(i=0; i<a; i++)
        {
            scanf("%d", &(b[i]));
            if(min_a1 > b[i])
            {
                min_a1 = b[i];
            }
        }
        min_d1 = 100000;
        min_d2 = 100000;
        for(i=0; i<d; i++)
        {
            scanf("%d", &(c[i]));
            if(min_d1 > c[i] || min_d2 > c[i])
            {
                if(min_d1 > min_d2)
                {
                    min_d1 = c[i];
                }
                else
                {
                    min_d2 = c[i];
                }
            }
        }

        if(min_d1 > min_a1 || min_d2 > min_a1)
        {
            printf("Y\n");
        }
        else printf("N\n");
    }

    return 0;
}
