/*
 * main.cpp
 *
 *  Created on: 05-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <vector>
#include <cstring>
#include <stack>
using namespace std;
#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	register int neg = 0;
	val = 0;
	while (ch < '0' || ch > '9') {
		if (ch == '-') {
			neg = 1;
			ch = gc();
			break;
		}
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	if (neg) {
		val = -val;
	}
}

bool isBipartiteGraph(vector<vector<int>>& v) {
	char color[v.size()];
	memset(color, 0, sizeof(char) * v.size());
	bool inq[v.size()] = { false };
	for (int i = 1; i < v.size(); i++) {
		if (color[i] == 0) {
			// dfs
			stack<int> s;
			s.push(i);
			inq[i] = true;
			color[i] = 1;
			while (!s.empty()) {
				int j = s.top();
				s.pop();
				char nxt = (color[j] == 1) ? 2 : 1;
				for (auto k : v[j]) {
					if (color[j] == color[k]) {
						return false;
					}
					if (!inq[k]) {
						inq[k] = true;
						s.push(k);
						color[k] = nxt;
					}
				}
			}
		}
	}
	return true;
}

void solve() {
	int b, n;
	scanint(b);
	scanint(n);
	vector<vector<int>> v(b + 1, vector<int>());
	int p, q;
	for (int i = 0; i < n; i++) {
		scanint(p);
		scanint(q);
		v[p].push_back(q);
		v[q].push_back(p);
	}
	if (isBipartiteGraph(v)) {
		printf("No suspicious bugs found!\n");
	} else {
		printf("Suspicious bugs found!\n");
	}
}

int main() {

	int t;
	scanint(t);
	for (int i = 1; i <= t; i++) {
		printf("Scenario #%d:\n", i);
		solve();
	}
	return 0;
}
