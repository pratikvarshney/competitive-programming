/*
 * main.cpp
 *
 *  Created on: 05-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>

#include <cstdlib>

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	register int neg = 0;
	val = 0;
	while (ch < '0' || ch > '9') {
		if (ch == '-') {
			neg = 1;
			ch = gc();
			break;
		}
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	if (neg) {
		val = -val;
	}
}

int a[300000];

int main() {
	int n, m;
	scanint(n);
	scanint(m);
	int sum = 0;
	int max = 0;
	int left = 0;
	for (int i = 0; i < n; i++) {
		scanint(a[i]);
		sum += a[i];
		while (sum > m) {
			sum -= a[left];
			left++;
		}
		if (max < sum) {
			max = sum;
			if (max == m) {
				printf("%d\n", max);
				return 0;
			}
		}
	}
	printf("%d\n", max);
	return 0;
}
