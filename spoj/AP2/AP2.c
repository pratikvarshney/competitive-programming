#include<stdio.h>

int main()
{
    long long a, b, c, d, n, s, i;
    int t;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%lld%lld%lld", &b, &c, &s);
        n = (2*s) / (b+c);
        d = (c-b) / (n-5);

        printf("%lld\n", n);

        a = b - (2*d);
        printf("%lld", a);
        for(i=1; i<n; i++)
        {
            a += d;
            printf(" %lld", a);
        }
        printf("\n");
    }
    return 0;
}
