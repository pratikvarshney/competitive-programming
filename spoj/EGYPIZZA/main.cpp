/*
 * main.cpp
 *
 *  Created on: 01-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <iostream>

using namespace std;

int main() {
	int n;
	scanf("%d", &n);
	char a[10];
	int half = 0, quarter = 0, threefourth = 0;
	for (int i = 0; i < n; i++) {
		scanf("%s", a);
		if (a[0] == '1' && a[2] == '2') {
			half++;
		} else if (a[0] == '1' && a[2] == '4') {
			quarter++;
		} else if (a[0] == '3' && a[2] == '4') {
			threefourth++;
		}
	}
	int total = 1;
	total += threefourth;
	quarter -= threefourth;
	total += (half / 2);
	if ((half % 2) == 1) { // as remaining half is either 0 or 1
		quarter -= 2;
		total += 1;
	}
	if (quarter > 0) {
		total += ((quarter + 3) / 4);
	}
	printf("%d\n", total);
	return 0;
}
