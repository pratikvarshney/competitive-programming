#include<stdio.h>
#include<string.h>

int next_pallindrome(char *str);
char* str_add1(char *input, int len);
int is_right_greater(char *str, int right, int left);

int main()
{
    char input[1000009];
    int t;
    scanf("%d", &t);
    while(t>0)
    {
        scanf("%s", input);
        next_pallindrome(input);
        t--;
    }
    return 0;
}

int next_pallindrome(char *str)
{
    int len, found, left, right;
    char *res, *temp;

    res = str_add1(str, strlen(str));
    len = strlen(res);
    if(len>1)
    {
        switch(len%2)
        {
        case 0:
            right = len/2;
            left = right-1;
            break;
        case 1:
            right = (len+1)/2;
            left = right-2;
            break;
        }
        temp = (char *)malloc((sizeof(char) * (len+1)));
        if(is_right_greater(res, right, left))
        {
            strcpy(temp, str_add1(res, right));
        }
        else
        {
            strncpy(temp, res, right);
        }
        while(left>=0)
        {
            temp[right]=temp[left];
            right++;
            left--;
        }
        temp[len]='\0';
        res=temp;
    }

    printf("%s\n", res);

    return 0;
}

char* str_add1(char *input, int len)
{
    int i, carry;
    char *temp, *str;
    str=(char *)malloc((sizeof(char) * (len+1)));
    strncpy(str, input, len);
    str[len]='\0';
    carry=1;
    for(i=len-1; i>=0; i--)
    {
        str[i] += carry;
        if(str[i] > '9')
        {
            str[i] -= 10;
        }
        else
        {
            carry = 0;
            break;
        }
    }
    if(carry!=0)
    {
        temp=(char *)malloc((sizeof(char) * (len+2)));
        temp[0]='0'+carry;
        temp[1]='\0';
        strcat(temp, str);
        free(str);
        return temp;
    }
    return str;
}

int is_right_greater(char *str, int right, int left)
{
    while(left>=0)
    {
        if(str[right]>str[left])
        {
            return 1;
        }
        else if(str[right]<str[left])
        {
            return 0;
        }
        right++;
        left--;
    }
    return 0;
}
