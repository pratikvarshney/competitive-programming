/*
 * main.cpp
 *
 *  Created on: 02-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <climits>

#define MAX 500
#define INF 10000000

int min(int a, int b) {
	return (a < b) ? a : b;
}

int solve() {
	int e, f;
	scanf("%d%d", &e, &f);
	int wmax = f - e;
	int n;
	scanf("%d", &n);
	int w[MAX + 1], p[MAX + 1];
	for (int i = 1; i <= n; i++) {
		scanf("%d%d", p + i, w + i);
	}
	if (wmax <= 0) {
		return 0;
	}

	int dp[n + 1][wmax + 1];
	for (int j = 0; j <= wmax; j++) {
		dp[0][j] = INF; // ??
	}

	for (int i = 1; i <= n; i++) {
		dp[i][0] = 0;
		for (int j = 1; j <= wmax; j++) {
			if (j < w[i]) {
				dp[i][j] = dp[i - 1][j];
			} else {
				dp[i][j] = min(dp[i - 1][j], p[i] + dp[i][j - w[i]]);
			}
		}
	}

	return dp[n][wmax];
}

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		int v = solve();
		if (v == 0 || v == INF) {
			printf("This is impossible.\n");
		} else {
			printf("The minimum amount of money in the piggy-bank is %d.\n", v);
		}
	}
	return 0;
}
