//Chudnovsky algorithm
import java.math.BigDecimal;
class PIVAL
{
	public static void main(String[] arr)
	{
		int precision = 10000;
		int iter = (precision+13)/14;
		BigDecimal res = BigDecimal.ZERO;
		
		//init
		BigDecimal a = new BigDecimal("13591409");
		BigDecimal a_add = new BigDecimal("545140134");
		
		BigDecimal b = BigDecimal.ONE;
		
		BigDecimal c = BigDecimal.ONE;
		
		BigDecimal d = BigDecimal.ONE;

		BigDecimal e = BigDecimal.ONE;
		BigDecimal e_mul = new BigDecimal("-640320");
		e_mul=e_mul.pow(3);
		
		int k=0;
		while(k<iter)
		{
			//proc
			BigDecimal nr=a.multiply(b);
			BigDecimal dr=c.multiply(d);
			dr=dr.multiply(e);
			res=res.add(nr.divide(dr, precision, BigDecimal.ROUND_HALF_EVEN));
			
			//nxt
			k++;
			
			a=a.add(a_add);
			
			int t=6*k;
			BigDecimal tmp=new BigDecimal(t);
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			b=b.multiply(tmp);
			
			t=3*k;
			tmp=new BigDecimal(t);
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			t--;
			tmp=tmp.multiply(new BigDecimal(t));
			c=c.multiply(tmp);
			
			tmp=new BigDecimal(k);
			d=d.multiply(tmp.pow(3));
			
			e=e.multiply(e_mul);
		}
		BigDecimal pi_val = new BigDecimal("426880");
		pi_val = pi_val.multiply(sqrt(new BigDecimal("10005"), precision));
		pi_val = pi_val.divide(res, precision, BigDecimal.ROUND_HALF_EVEN);
		System.out.println(pi_val.toPlainString());
	}
	public static BigDecimal sqrt(BigDecimal A, final int SCALE) {
		BigDecimal x0 = BigDecimal.ZERO;
		BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
		BigDecimal x2 = new BigDecimal("2");
		while (x0.compareTo(x1)!=0) {
			x0 = x1;
			x1 = A.divide(x0, SCALE, BigDecimal.ROUND_HALF_UP);
			x1 = x1.add(x0);
			x1 = x1.divide(x2, SCALE, BigDecimal.ROUND_HALF_UP);

		}
		return x1;
	}
}