/*
 * main.cpp
 *
 *  Created on: 07-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <cmath>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

double solve(double U, double W, double v, double V, double w, double u) {
	double X = (w - U + v) * (U + v + w);
	double x = (U - v + w) * (v - w + U);
	double Y = (u - V + w) * (V + w + u);
	double y = (V - w + u) * (w - u + V);
	double Z = (v - W + u) * (W + u + v);
	double z = (W - u + v) * (u - v + W);
	double a = sqrt(x * Y * Z);
	double b = sqrt(y * Z * X);
	double c = sqrt(z * X * Y);
	double d = sqrt(x * y * z);
	return sqrt(
			(-a + b + c + d) * (a - b + c + d) * (a + b - c + d)
					* (a + b + c - d)) / (192 * u * v * w);
}

int main() {
	int t;
	int AB, AC, AD, BC, BD, CD;
	scanint(t);
	while (t--) {
		scanint(AB);
		scanint(AC);
		scanint(AD);
		scanint(BC);
		scanint(BD);
		scanint(CD);
		printf("%.4f\n", solve(AB, AC, AD, BC, BD, CD));
	}
	return 0;
}
