#include<stdio.h>
#include<math.h>

int main()
{
    int t, c;
    long long n, k, a, b;
    int found = 0;
    int num = 0;
    scanf("%d", &t);
    for(c=1; c<=t; c++)
    {
        scanf("%lld", &n);
        printf("Case #%d:\n", c);
        num = 0;
        for(a=2; a<65536; a++)
        {
            if((n%((a*a)-1))==0)
            {
                k = (n/((a*a)-1)) + 1;
                b = sqrt(k);
                if((b*b)==k)
                {
                    if(a<=b)
                    {
                        if(num==0) printf("%lld", n);
                        printf("=(%lld^2-1)*(%lld^2-1)", a, b);
                        num++;
                    }
                    else break;
                }
            }
        }
        if(num==0) printf("For n=%lld there is no almost square factorisation.", n);
        printf("\n");
    }
	return 0;
}
