/*
 * main.cpp
 *
 *  Created on: 10-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>

#define gc getchar

char a[50][50];
int m[50][50];

char nextChar() {
	char ch = gc();
	while (ch < 'A' || ch > 'Z') {
		ch = gc();
	}
	return ch;
}

int getPathLength(char c, int i, int j, int h, int w) {
	// check valid dim
	if (i < 0 || i >= h || j < 0 || j >= w) {
		return 0;
	}
	if (a[i][j] == c) {
		if (m[i][j] > 0) {
			return m[i][j];
		}
		int maxLen = 0, currLen;
		for (int p = i - 1; p <= i + 1; p++) {
			for (int q = j - 1; q <= j + 1; q++) {
				if (p != i || q != j) {
					currLen = getPathLength(c + 1, p, q, h, w);
					if (maxLen < currLen) {
						maxLen = currLen;
					}
				}
			}
		}
		m[i][j] = 1 + maxLen;
		return 1 + maxLen; // 1 because of current char match
	}
	return 0;
}

int maxPathLength(int h, int w) {
	for (int i = 0; i < h; i++) {
		for (int j = 0; j < w; j++) {
			a[i][j] = nextChar();
			m[i][j] = 0;
		}
	}

	int maxLength = 0;

	// dfs

	for (int i = 0; i < h; i++) {
		for (int j = 0; j < w; j++) {
			if (a[i][j] == 'A') {
				int len = getPathLength('A', i, j, h, w);
				if (maxLength < len) {
					maxLength = len;
				}
			}
		}
	}

	return maxLength;
}

int main() {
	int h, w;

	for (int i = 1;; i++) {
		scanf("%d%d", &h, &w);
		if (h == 0 && w == 0) {
			break;
		}
		printf("Case %d: %d\n", i, maxPathLength(h, w));
	}
	return 0;
}
