/*
 * main.cpp
 *
 *  Created on: 08-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <climits>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	register int neg = 0;
	val = 0;
	while (ch < '0' || ch > '9') {
		if (ch == '-') {
			neg = 1;
			ch = gc();
			break;
		}
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	if (neg) {
		val = -val;
	}
}

long long min(long long a, long long b) {
	return (a < b) ? a : b;
}

long long min(long long a, long long b, long long c) {
	return min(min(a, b), c);
}

long long min(long long a, long long b, long long c, long long d) {
	return min(min(a, b), min(c, d));
}

long long solve(int n) {
	long long a = 0, b = 0, c = 0;
	int t;
	scanint(t);
	a = INT_MAX;
	scanint(t);
	b = t;
	scanint(t);
	c = t + b;
	for (int i = 1; i < n; i++) {

		scanint(t);
		long long p = t + min(a, b);

		scanint(t);
		long long q = t + min(p, a, b, c);

		scanint(t);
		long long r = t + min(q, b, c);

		a = p;
		b = q;
		c = r;
	}
	return b;
}

int main() {
	int t;
	for (int k = 1;; k++) {
		scanint(t);
		if (t == 0) {
			break;
		}
		printf("%d. %lld\n", k, solve(t));
	}
	return 0;
}
