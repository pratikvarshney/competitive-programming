/*
 * main.cpp
 *
 *  Created on: 08-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <cmath>
#include <cstring>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

char dp[1000001];

void init(int k, int l, int max) {
	memset(dp, 0, sizeof(char) * max);
	dp[1] = 'A';
	dp[k] = 'A';
	dp[l] = 'A';
	for (int i = 2; i <= max; i++) {
		if (dp[i] == 0) {
			dp[i] = 'A';
			if (i <= k) {
				if (dp[i - 1] == 'A') {
					dp[i] = 'B';
				}
			} else if (i <= l) {
				if (dp[i - 1] == 'A' && dp[i - k] == 'A') {
					dp[i] = 'B';
				}
			} else {
				if (dp[i - 1] == 'A' && dp[i - k] == 'A' && dp[i - l] == 'A') {
					dp[i] = 'B';
				}
			}
		}
	}
}

int main() {
	int k, l, m;
	scanint(k);
	scanint(l);
	scanint(m);
	int n[50];
	int max = 0;
	for (int i = 0; i < m; i++) {
		scanint(n[i]);
		if (max < n[i]) {
			max = n[i];
		}
	}
	init(k, l, max);
	for (int i = 0; i < m; i++) {
		putchar(dp[n[i]]);
	}
	return 0;
}
