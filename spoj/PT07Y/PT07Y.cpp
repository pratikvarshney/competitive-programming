#include<iostream>
#include<cstdio>
#include<vector>
#include<stack>

int main()
{
    int n, m, i;
    int u, v;
    int k=0;
    scanf("%d%d", &n, &m);
    std::vector<int> node[n+1];
    bool checked[10001]={0};
    bool noroot[10001]={0};
    checked[0]=1;

    for(i=0; i<m; i++)
    {
        scanf("%d%d", &u, &v);
        node[u].push_back(v);
        noroot[v]=1;
    }

    //find root
    int root=0;
    for(i=1; i<n; i++)
    {
        if(!noroot[i])
        {
            root = i;
            break;
        }
    }


    //DFS
    std::stack<int> stk;
    stk.push(root);

    while(!stk.empty())
    {
        i = stk.top();
        stk.pop();
        if(!checked[i])
        {
            checked[i]=1;
            k++;
            for(std::vector<int>::iterator it = node[i].begin(); it!=node[i].end(); it++)
            {
                stk.push(*it);
            }
        }
        else break;
    }

    if(k==n)
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }

    return 0;
}
