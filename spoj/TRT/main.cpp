/*
 * main.cpp
 *
 *  Created on: 31-Dec-2016
 *      Author: Pratik Varshney
 */

#include <cstdio>

inline int max(int a, int b);
inline int max(int a, int b) {
	return (a > b) ? a : b;
}

// TRT
int main() {
	int n;
	int v[2000];
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", v + i);
	}

	int dp[2000];
	// opt(i,i) = a*v[i]
	// opt(i,j) = max(a*v[i] + opt(i+1,j), a*v[j]+opt(i, j-1));
	for (int i = n - 1; i >= 0; i--) {
		dp[i] = n * v[i]; // age = n
		for (int j = i + 1; j < n; j++) {
			int age = n - (j - i);
			dp[j] = max((age * v[i]) + dp[j], (age * v[j]) + dp[j - 1]);
		}
	}

	printf("%d\n", dp[n - 1]);

	return 0;
}
