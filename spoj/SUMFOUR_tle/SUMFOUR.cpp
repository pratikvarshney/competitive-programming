#include<cstdio>
#include<set>
#include <unordered_set>

#define MAXM 4000

int main()
{
    long a[MAXM];
    long b[MAXM];
    long c[MAXM];
    long d[MAXM];

    long n, i, j;
    long long num=0;

    scanf("%d", &n);

    for(i=0; i<n; i++)
    {
        scanf("%ld%ld%ld%ld", &(a[i]), &(b[i]), &(c[i]), &(d[i]));
    }

    std::unordered_multiset<long> s;
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            s.insert(a[i]+b[j]);
        }
    }
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            num += s.count(-(c[i]+d[j]));
        }
    }

    printf("%lld", num);

    return 0;
}
