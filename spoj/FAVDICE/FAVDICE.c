#include<stdio.h>

#define MAXM 1000

int main()
{
    double val[MAXM+1]={0};
    int i, n, t;

    /* init */
    for(i=1; i<=MAXM; i++) val[i] = 1.0/(double)i;
    for(i=2; i<=MAXM; i++) val[i] += val[i-1]; //1/1 + 1/2 + 1/3 ... (harmonic series)

    scanf("%d", &t);
    for( ;t>0; t--)
    {
        scanf("%d", &n);
        printf("%.2f\n", n*val[n]); // n * harmonic(n)
    }
    return 0;
}
