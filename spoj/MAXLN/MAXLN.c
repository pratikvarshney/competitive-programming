#include<stdio.h>
#include<math.h>

int main()
{
    long long i, t, r;
    double s, dsqr, s_old;;
    double bmin, bmax, bmid;
    double sqrt2 = sqrt(2);

    scanf("%lld", &t);
    for(i=1; i<=t; i++)
    {
        scanf("%lld", &r);
        dsqr = 4*r*r;
        bmin = 0;
        bmax = sqrt2 * r;
        s = 0;
        while(bmax-bmin > 0.005)
        {
            s_old = s;
            bmid = (bmin+bmax)/2;
            if((dsqr-(bmin*bmin)+bmin) < (dsqr-(bmax*bmax)+bmax))
            {
                bmin = bmid;
            }
            else
            {
                bmax = bmid;
            }
            s = (dsqr-(bmid*bmid)+bmid);
        }
        printf("Case %d: ", i);
        printf("%.2lf\n", s);
    }

    return 0;
}
