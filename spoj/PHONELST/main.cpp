/*
 * main.cpp
 *
 *  Created on: 08-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

void scanstr(char* s) {
	register char ch = gc();
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		*s = ch;
		s++;
		ch = gc();
	}
	*s = '\0';
}

struct node {
	char data = 0;
	node *next[10] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
			nullptr, nullptr, nullptr, nullptr };
};

char a[10000][11];

void solve() {
	int n;
	scanint(n);
	for (int i = 0; i < n; i++) {
		scanstr(a[i]);
	}
	node* trie = new node;
	for (int i = 0; i < n; i++) {
		node* curr = trie;
		for (int j = 0; a[i][j] != '\0'; j++) {
			if (curr->next[a[i][j] - '0'] == nullptr) {
				curr->next[a[i][j] - '0'] = new node;
			} else if (curr->next[a[i][j] - '0']->data == '*') {
				printf("NO\n");
				return;
			}
			curr = curr->next[a[i][j] - '0'];

		}
		if (curr->data == '*') {
			printf("NO\n");
			return;
		}
		for (int i = 0; i < 10; i++) {
			if (curr->next[i] != nullptr) {
				printf("NO\n");
				return;
			}
		}
		curr->data = '*';
	}
	printf("YES\n");
}

int main() {

	int t;
	scanint(t);
	while (t--) {
		solve();
	}

	return 0;
}
