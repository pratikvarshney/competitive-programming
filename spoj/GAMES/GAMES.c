#include<stdio.h>
#include<string.h>

int main()
{
    int i, n, t, d, m;
    char str[20];
    char *pch;
    scanf("%d", &t);
    while(t>0)
    {
        scanf("%s", str);
        pch = strchr(str, '.');
        m=1;
        if(pch!=NULL)
        {
            d=strlen(pch+1);
            n=atoi(pch+1);
            while(d--) m *= 10;
            for(i=1; i<m; i++)
            {
                if( (n*i)%m == 0 ) break;
            }
            printf("%d\n", i);
        }
        else
        {
            printf("1\n");
        }
        t--;
    }
    return 0;
}
