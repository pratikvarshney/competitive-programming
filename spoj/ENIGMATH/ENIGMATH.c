#include<stdio.h>

/* Standard C Function: Greatest Common Divisor */
int gcd( int a, int b )
{
    int c;
    while( a != 0 )
    {
        c = a;
        a = b%a;
        b = c;
    }
    return b;
}

int main()
{
    int t;
    int a, b;
    int k;

    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d", &a, &b);
        k = gcd(a, b);
        printf("%d %d\n", b/k, a/k);
    }
    return 0;
}
