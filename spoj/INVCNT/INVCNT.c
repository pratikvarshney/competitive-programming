#include<stdio.h>
#include<string.h>
#define gc getchar_unlocked

int getPosInt();
long long int inversionCount(int a[], int n);

int main()
{
    int t, n, i, j;
    t=getPosInt();

    while(t--)
    {
        n=getPosInt();
        long long int inv_count = 0;
        int val;
        int a[n];

        for(j=0; j<n; j++)
        {
            a[j]=getPosInt();
        }


        printf("%lld\n", inversionCount(a, n));
    }

    return 0;
}
long long int inversionCount(int a[], int n){
	long long int count = 0;
	if(n>1){
		
		/* Mergesort like : Divide & conquer */
		int mid = n/2;
		int r = n - mid;
		int p[mid], q[r];

		memcpy(p, a, mid * sizeof(int));
		memcpy(q, a + mid, r * sizeof(int));

		count = inversionCount(p, mid);
		count += inversionCount(q, r);
		
		/* combine */
		int i=0, j=0, k=0;
		while(i < mid && j < r){
			if(p[i] < q[j]){
				a[k++] = p[i++];
			}else{
				a[k++] = q[j++];
				count += (mid - i); /* no. of elem remaining in a => greater than q[j] viz inversion */
			}
		}
		while(i < mid){
			a[k++] = p[i++];
		}
		while(j < r){
			a[k++] = q[j++];
		}
	}
	return count;
}
int getPosInt()
{
    int val=0;
    char ch=gc();
    while(ch<'0' || ch>'9') ch=gc();
    while(ch>='0' && ch<='9')
    {
        val = (val << 1)+(val<<3)+(ch-'0');
        ch=gc();
    }
    return val;
}
