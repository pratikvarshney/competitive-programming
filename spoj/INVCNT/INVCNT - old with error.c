#include<stdio.h>

int get_count(int a[], int a_size, int val, int par);

int main()
{
    int t, n, i, j;
    scanf("%d", &t);

    while(t--)
    {
        scanf("%d", &n);
        long long int inv_count = 0;
        int par, val;
        int a[n];

        if(n==0)
        {
            printf("%lld\n", inv_count);
            continue;
        }

        //max-heap
        scanf("%d", &(a[0]));
        for(i=1; i<n; i++)
        {
            scanf("%d", &val);
            a[i]=val;
            j = i;
            par = (j-1)/2;
            while(par>=0)
            {
                if(a[par]<val)
                {
                    a[j]=a[par];
                    a[par]=val;
                }
                else
                {
                    break;
                }
                j = par;
                par = (j-1)/2;
            }
            inv_count += get_count(a, i, val, 0);

        }

        printf("%lld\n", inv_count);
    }

    return 0;
}

int get_count(int a[], int a_size, int val, int par)
{
    if(val >= a[par]) return 0;
    if( ((2*par)+2) < a_size ) return (1 + get_count(a, a_size, val, ((2*par)+1)) + get_count(a, a_size, val, ((2*par)+2)));
    if( ((2*par)+2) == a_size ) return (1 + get_count(a, a_size, val, ((2*par)+1)));
    else return 1;
}
