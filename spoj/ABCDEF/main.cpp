/*
 * main.cpp
 *
 *  Created on: 05-Jan-2017
 *      Author: Pratik Varshney
 */

#include <vector>
#include <cstdio>
#include <unordered_map>

using namespace std;

#define gc getchar

void scanint(long &val);

void scanint(long &val) {
	register int ch = gc();
	register int neg = 0;
	val = 0;
	while (ch < '0' || ch > '9') {
		if (ch == '-') {
			neg = 1;
			ch = gc();
			break;
		}
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	if (neg) {
		val = -val;
	}
}

long s[100];

long solve(long n) {
	// a*b + c
	unordered_map<long, long> abc;
	for (int i = 0; i < n; i++) {
		long a = s[i];
		for (int j = 0; j < n; j++) {
			long b = s[j];
			for (int c = 0; c < n; c++) {
				long k = (a * b) + s[c];
				if (abc.count(k) > 0) {
					abc[k] = abc[k] + 1;
				} else {
					abc[k] = 1;
				}
			}
		}
	}

	// e + f
	unordered_map<long, long> ef;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			long k = s[i] + s[j];
			if (ef.count(k) > 0) {
				ef[k] = ef[k] + 1;
			} else {
				ef[k] = 1;
			}
		}
	}

	long count = 0;

	// check a*b + c == d(e+f); d!=0
	for (int i = 0; i < n; i++) {
		if (s[i] != 0) {
			long d = s[i];
			for (unordered_map<long, long>::iterator it = ef.begin();
					it != ef.end(); ++it) {
				long rhs = d * (it->first);
				auto lhs = abc.find(rhs);
				if (lhs != abc.end()) {
					count += (lhs->second * it->second);
				}
			}
		}
	}

	return count;
}

int main() {
	long n;
	scanint(n);
	for (int i = 0; i < n; i++) {
		scanint(s[i]);
	}
	printf("%ld\n", solve(n));
	return 0;
}
