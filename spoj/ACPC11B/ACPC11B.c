#include<stdio.h>

#define MAXM 1001

#define diff(p,q) (p>q)?(p-q):(q-p);

int dbm(int m, int a[], int n, int b[]);

int main()
{
    int m, n, t, i;
    int a[MAXM];
    int b[MAXM];

    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d", &m);
        for(i=0; i<m; i++) scanf("%d", &(a[i]));

        scanf("%d", &n);
        for(i=0; i<n; i++) scanf("%d", &(b[i]));

        printf("%d\n", dbm(m, a, n, b));
    }

    return 0;
}

int dbm(int m, int a[], int n, int b[])
{
    int i, j, minm, d;

    minm = diff(a[0], b[0]);

    for(i=0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            d = diff(a[i], b[j]);
            if(minm > d) minm = d;
        }
    }

    return minm;
}
