#include<stdio.h>

#define FIB_MAX 92

int main()
{
    long long int fib[FIB_MAX];
    int i;
    char ch, pre;
    long long int last_len, res;
    //init fib
    fib[0]=1;
    fib[1]=1;
    for(i=2; i<FIB_MAX; i++) fib[i]=fib[i-1]+fib[i-2];
    //done init fib

    ch = getchar();
    while(ch!='0')
    {
        res=1;
        last_len = 1;
        pre = '9';
        while(ch!='\n')
        {
            if(ch=='0')
            {
                if((pre=='1') || (pre=='2'))
                {
                    res *= fib[last_len-1];
                    last_len = 0;
                }
                else
                {
                    res=0;
                    while(ch!='\n') ch = getchar();
                    break;
                }
            }
            else if( (((pre-'0')*10)+(ch-'0')) <= 26 )
            {
                last_len++;
            }
            else
            {
                res *= fib[last_len];
                last_len = 1;
            }
            pre = ch;
            ch = getchar();
        }
        res *= fib[last_len];
        printf("%lld\n", res);
        ch = getchar();
    }


    return 0;
}
