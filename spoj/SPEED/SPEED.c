#include<stdio.h>

/* Standard C Function: Greatest Common Divisor */
int gcd( int a, int b )
{
    int c;
    while( a != 0 )
    {
        c = a;
        a = b%a;
        b = c;
    }
    return b;
}

int abs(int a)
{
    return (a>=0)?a:-a;
}

int main()
{
    int t;
    int a, b;

    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d", &a, &b);
        printf("%d\n", abs(a-b)/gcd(abs(a), abs(b)));
    }
    return 0;
}
