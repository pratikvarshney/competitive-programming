/*
 * main.cpp
 *
 *  Created on: 07-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <cmath>
#include <climits>

using namespace std;

#define gc getchar

void scanlong(long &val);

void scanlong(long &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

long a[100002];
long left[100002];
long right[100002];

long long solve(long n) {
	for (int i = 1; i <= n; i++) {
		scanlong(a[i]);
	}
	a[0] = INT_MIN;
	a[n + 1] = INT_MIN;

	// left upto where a[i] is smallest
	for (int i = 1; i <= n; i++) {
		int l = i - 1;
		while (a[l] >= a[i]) {
			l = left[l];
		}
		left[i] = l;
		// a[i] is smallest from (left[i]+1) to i
	}

	long long max = 0;
	for (int i = n; i > 0; i--) {
		int r = i + 1;
		while (a[r] >= a[i]) {
			r = right[r];
		}
		right[i] = r;
		// a[i] is smallest from to i to (right[i]-1)
		long long area = ((long long) a[i]) * (r - left[i] - 1);
		if (max < area) {
			max = area;
		}
	}

	return max;
}

int main() {
	long n;
	scanlong(n);
	while (n > 0) {
		printf("%lld\n", solve(n));
		scanlong(n);
	}
	return 0;
}
