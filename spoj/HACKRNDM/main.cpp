/*
 * main.cpp
 *
 *  Created on: 29-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <algorithm>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	register int neg = 0;
	val = 0;
	while (ch < '0' || ch > '9') {
		if (ch == '-') {
			neg = 1;
			ch = gc();
			break;
		}
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	if (neg) {
		val = -val;
	}
}

int a[100001];

bool bsearch(int val, int n) {
	int l = 0;
	int r = n - 1;
	if (val < a[l] || val > a[r]) {
		return false;
	}

	while (l <= r) {
		int m = (l + r) / 2;
		if (a[m] == val) {
			return true;
		}
		if (a[m] < val) {
			l = m + 1;
		} else {
			r = m - 1;
		}
	}

	return false;
}

int main() {
	int n, k;
	scanint(n);
	scanint(k);
	for (int i = 0; i < n; i++) {
		scanint(a[i]);
	}
	sort(a, a + n);
	int count = 0;
	for (int i = 0; i < n; i++) {
		if (bsearch(a[i] + k, n)) {
			count++;
		}
	}
	printf("%d\n", count);
	return 0;
}
