#include<stdio.h>

int main()
{
    long long int i, n;
    int t;
    int a[4] = {942, 192, 442, 692};

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%lld", &n);
        if(n>4)printf("%lld", (n-1)/4);
        printf("%d\n", a[n%4]);

        t--;
    }

    return 0;
}
