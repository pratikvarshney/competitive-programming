#include<iostream>
#include<cstdio>
#include<array>
#include<vector>
using namespace std;

#define DIV 1000000007
#define M 100001
#define N 21


int main()
{
    int n, k;
    std::vector<std::array<int, N>> p(M);
    int t;

    /* calculate partitions (exactly k) */
    for(n=0; n<M;n++)
    {
        for(k=0; k<N; k++)
        {
            if(k==0)
            {
                p[n][k]=0;
            }
            else if(k>n)
            {
                p[n][k]=0;
            }
            else if(k==n)
            {
                p[n][k]=1;
            }
            else
            {
                p[n][k] = (p[n-1][k-1] + p[n-k][k])%DIV;
            }
        }
    }

    /* calculate distinct partition Q (Note from n=M to 0) */
    for(n=M-1; n>0; n--)
    {
        for(k=1; k<N; k++)
        {
            t = n-((k*(k-1))/2);
            if(t>=0)
                p[n][k] = p[t][k];
            else
                p[n][k] = 0;
        }
    }

    /* result <= k */
    for(n=1; n<M; n++)
    {
        for(k=1; k<N; k++)
        {
            p[n][k] = (p[n][k] + p[n-1][k])%DIV;
        }
    }

    scanf("%d", &t);
    while(t--)
    {
        scanf("%d%d", &k, &n);
        printf("%d\n", p[n][k]);
    }

    return 0;
}
