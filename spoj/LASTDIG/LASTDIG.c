#include<stdio.h>

int last_pow_dig(unsigned long a, unsigned long b);

int main()
{
    unsigned long a, b;
    int t;
    scanf("%d", &t);
    while(t>0)
    {
        scanf("%lu%lu", &a, &b);
        printf("%d\n", last_pow_dig(a, b));
        t--;
    }
    return 0;
}

int last_pow_dig(unsigned long a, unsigned long b)
{
    unsigned long p;
    unsigned long i;
    unsigned long r;

    if(a==0) return 0;
    if(b==0) return 1;
    if(a==1) return 1;
    if(b==1) return a%10;
    if((a%10)==0) return 0;

    r=1;

    while(b>=10)
    {
        p=a%10;
        i=1;
        while((10*i)<=b)
        {
            p = (p*p*p*p*p)%10;
            p = (p*p)%10;
            i *= 10;
        }
        r = (r*p)%10;
        if(r==0) return 0;
        b -= i;
    }
    while(b>0)
    {
        r = (r*a)%10;
        if(r==0) return 0;
        b--;
    }
    return r%10;
}
