#include<stdio.h>

int gcd(int a, int b);

int main()
{
    int c_no, t;
    int a, b, c;

    scanf("%d", &t);

    for(c_no=1; c_no<=t; c_no++)
    {
        scanf("%d%d%d", &a, &b, &c);
        if(c%gcd(a,b)==0)
        {
            printf("Case %d: Yes\n", c_no);
        }
        else printf("Case %d: No\n", c_no);
    }
    return 0;
}

int gcd(int a, int b)
{
    if(b>a) return gcd(b,a);
    int c;
    c=a%b;
    while(c!=0)
    {
        a=b;
        b=c;
        c=a%b;
    }
    return b;
}
