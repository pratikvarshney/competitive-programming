#include<stdio.h>
#include<string.h>

#define MAX_SIZE 1000000

int div_2(char *str);
int div_3(char *str);
int div_4(char *str);
int div_5(char *str);
int div_6(char *str);
int div_7(char *str);
int div_8(char *str);
int div_9(char *str);

int main()
{
    int t, n, len;
    char str[MAX_SIZE];

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%s%d", str, &n);

        switch(n)
        {
        case 2:
            div_2(str);
            break;
        case 3:
            div_3(str);
            break;
        case 4:
            div_4(str);
            break;
        case 5:
            div_5(str);
            break;
        case 6:
            div_6(str);
            break;
        case 7:
            div_7(str);
            break;
        case 8:
            div_8(str);
            break;
        case 9:
            div_9(str);
            break;
        }
        printf("\n");
        t--;
    }
}

int div_2(char *str)
{
    if(str[strlen(str)-1]=='0')
    {
        printf("Divisible By 2\n");
    }
    else
    {
        printf("Not Divisible By 2\n");
    }
    return 0;
}
int div_3(char *str)
{
    int q[3][2]={{0,1},{2,0},{1,2}};
    int i, j;
    i=0;
    for(j=0; str[j]!='\0'; j++)
    {
        i=q[i][str[j]-'0'];
    }
    if(i==0)
    {
        printf("Divisible By 3\n");
    }
    else
    {
        printf("Not Divisible By 3\n");
    }
    return 0;
}
int div_4(char *str)
{
    int len=strlen(str);
    if((len>2) && (str[len-1]=='0') && (str[len-2]=='0'))
    {
        printf("Divisible By 4\n");
    }
    else
    {
        printf("Not Divisible By 4\n");
    }
    return 0;
}
int div_5(char *str)
{
    int q[5][2]={{0,1},{2,3},{4,0},{1,2},{3,4}};
    int i, j;
    i=0;
    for(j=0; str[j]!='\0'; j++)
    {
        i=q[i][str[j]-'0'];
    }
    if(i==0)
    {
        printf("Divisible By 5\n");
    }
    else
    {
        printf("Not Divisible By 5\n");
    }
    return 0;
}
int div_6(char *str)
{
    int q[6][2]={{0,1},{2,3},{4,5},{0,1},{2,3},{4,5}};
    int i, j;
    i=0;
    for(j=0; str[j]!='\0'; j++)
    {
        i=q[i][str[j]-'0'];
    }
    if(i==0)
    {
        printf("Divisible By 6\n");
    }
    else
    {
        printf("Not Divisible By 6\n");
    }
    return 0;
}
int div_7(char *str)
{
    int q[7][2]={{0,1},{2,3},{4,5},{6,0},{1,2},{3,4},{5,6}};
    int i, j;
    i=0;
    for(j=0; str[j]!='\0'; j++)
    {
        i=q[i][str[j]-'0'];
    }
    if(i==0)
    {
        printf("Divisible By 7\n");
    }
    else
    {
        printf("Not Divisible By 7\n");
    }
    return 0;
}
int div_8(char *str)
{
    int len=strlen(str);
    if((len>3) && (str[len-1]=='0') && (str[len-2]=='0') && (str[len-3]=='0'))
    {
        printf("Divisible By 8\n");
    }
    else
    {
        printf("Not Divisible By 8\n");
    }
    return 0;
}
int div_9(char *str)
{
    int q[9][2]={{0,1},{2,3},{4,5},{6,7},{8,0},{1,2},{3,4},{5,6},{7,8}};
    int i, j;
    i=0;
    for(j=0; str[j]!='\0'; j++)
    {
        i=q[i][str[j]-'0'];
    }
    if(i==0)
    {
        printf("Divisible By 9\n");
    }
    else
    {
        printf("Not Divisible By 9\n");
    }
    return 0;
}
