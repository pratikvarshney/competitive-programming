import java.util.Scanner;

class DIVCHK
{
	public static void main(String[] arr)
	{
		int t;
		Scanner sc = new Scanner(System.in);
		t=sc.nextInt();
		while(t>0)
		{
			String str = sc.next();
			int n=sc.nextInt();
			int state=0;
			for(int i=0; i<str.length(); i++)
			{
				state = ((2*state)+(str.charAt(i)-'0'))%n;
			}
			if(state==0)
			{
				System.out.println("Divisible By "+n);
			}
			else
			{
				System.out.println("Not Divisible By "+n);
			}
			System.out.println();
			t--;
		}
	}
}