#include<stdio.h>
#include<string.h>

int div_2(char *str);
int div_3(char *str);
int div_4(char *str);
int div_5(char *str);

int main()
{
    int t, n, i, d;
    char str[240];

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%s%d", str, &n);

        switch(n)
        {
        case 2:
            d=div_2(str);
            break;
        case 3:
            d=div_3(str);
            break;
        case 4:
            d=div_4(str);
            break;
        case 5:
            d=div_5(str);
            break;
        }

        if(d)
        {
            printf("Divisible By %d\n", n);
        }
        else
        {
            printf("Not Divisible By %d\n", n);
        }

        printf("\n");
        t--;
    }
    return 0;
}

int div_2(char *str)
{
    if(str[strlen(str)-1]=='0') return 1;
    return 0;
}
int div_3(char *str)
{
    int q[3][2]={{0,1},{2,0},{1,2}};
    int s=0, i, len;
    len = strlen(str);
    for(i=0; i<len; i++)
    {
        s = q[s][str[i]-'0'];
    }
    if(s==0) return 1;
    return 0;
}
int div_4(char *str)
{
    int len=strlen(str);
    if((len>2) && (str[len-1]=='0') && (str[len-2]=='0')) return 1;
    return 0;
}
int div_5(char *str)
{
    int q[5][2]={{0,1},{2,3},{4,0},{1,2},{3,4}};
    int s=0, i, len;
    len = strlen(str);
    for(i=0; i<len; i++)
    {
        s = q[s][str[i]-'0'];
    }
    if(s==0) return 1;
    return 0;
}
