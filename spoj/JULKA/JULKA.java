import java.util.Scanner;
import java.math.BigInteger;

class JULKA
{
	public static void main(String[] arr)
	{
		BigInteger a, b, c, d;
		int t;
		Scanner sc = new Scanner(System.in);
		for(t=0; t<10; t++)
		{
			a=new BigInteger(sc.nextLine());
			b=new BigInteger(sc.nextLine());
			d=a.subtract(b).divide(BigInteger.valueOf(2));
			c=d.add(b);
			System.out.println(c);
			System.out.println(d);
		}
	}
}