#include<stdio.h>
// BLOCK
// 0 | 1
// 2 | 3

char a[512][512];
char b[512][512];
int n;

int best_match(int ro, int co, int size);

int main()
{
    int r, c;
    scanf("%d",&n);
    char str[520];
    for(r=0; r<n;r++)
    {
        scanf("%s", str);
        for(c=0; c<n;c++)
        {
            a[r][c]=str[c];
        }
    }
    printf("%d\n", best_match(0, 0, n));
    for(r=0; r<n;r++)
    {
        for(c=0; c<n;c++)
        {
            putchar(b[r][c]);
        }
        printf("\n");
    }
    return 0;
}

int best_match(int ro, int co, int size)
{
    int min_error, min_w, min_b, wr=0, wc=0, br=0, bc=0;
    int white_err[2][2];
    int black_err[2][2];
    int block_size = size/2;
    int r, c, i, j;
    int row_offset, col_offset;

    if(size==1)
    {
        b[ro][co]=a[ro][co];
        return 0;
    }

    //check white=0
    for(i=0; i<2; i++)
    {
        row_offset=i*block_size;
        for(j=0; j<2; j++)
        {
            col_offset = j*block_size;
            white_err[i][j]=0;
            for(r=0; r<block_size;r++)
            {
                for(c=0; c<block_size;c++)
                {
                    if(a[ro+r+row_offset][co+c+col_offset]!='0') white_err[i][j]++;
                }
            }
        }
    }
    //check black=0
    for(i=0; i<2; i++)
    {
        row_offset=i*block_size;
        for(j=0; j<2; j++)
        {
            col_offset = j*block_size;
            black_err[i][j]=0;
            for(r=0; r<block_size;r++)
            {
                for(c=0; c<block_size;c++)
                {
                    if(a[ro+r+row_offset][co+c+col_offset]!='0') black_err[i][j]++;
                }
            }
        }
    }
    wr=0;
    wc=0;
    br=0;
    bc=0;
    min_w = 1000000;
    for(r=0; r<2; r++)
    {
        for(c=0; c<2; c++)
        {
            if(min_w>white_err[r][c])
            {
                min_w=white_err[r][c];
                wr=r;
                wc=c;
            }
        }
    }
    min_b = 1000000;
    for(r=0; r<2; r++)
    {
        for(c=0; c<2; c++)
        {
            if(min_b>black_err[r][c])
            {
                min_b=black_err[r][c];
                br=r;
                bc=c;
            }
        }
    }
    if(min_w<min_b)
    {
        min_b=1000000;
        for(r=0; r<2; r++)
        {
            for(c=0; c<2; c++)
            {
                if(r!=wr || c!=wc)
                {
                    if(min_b>black_err[r][c])
                    {
                        min_b=black_err[r][c];
                        br=r;
                        bc=c;
                    }
                }
            }
        }
    }
    else
    {
        min_w=1000000;
        for(r=0; r<2; r++)
        {
            for(c=0; c<2; c++)
            {
                if(r!=br || c!=bc)
                {
                    if(min_w>white_err[r][c])
                    {
                        min_w=white_err[r][c];
                        wr=r;
                        wc=c;
                    }
                }
            }
        }
    }
    min_error = min_w + min_b;

    //fill white
    row_offset = wr*block_size;
    col_offset = wc*block_size;
    for(r=0; r<block_size;r++)
    {
        for(c=0; c<block_size;c++)
        {
            b[ro+r+row_offset][co+c+col_offset]='0';
        }
    }
    //fill black
    row_offset = br*block_size;
    col_offset = bc*block_size;
    for(r=0; r<block_size;r++)
    {
        for(c=0; c<block_size;c++)
        {
            b[ro+r+row_offset][co+c+col_offset]='1';
        }
    }
    int mark[2][2]={{0,0},{0,0}};
    mark[wr][wc]=1;
    mark[br][bc]=1;
    for(r=0; r<2; r++)
    {
        for(c=0; c<2; c++)
        {
            if(!mark[r][c])
            {
                min_error += best_match(ro+(r*block_size), co+(c*block_size), block_size);
            }
        }
    }

    return min_error;
}
