#include<stdio.h>

int main()
{
    long long int res;
    int t, n;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%d", &n);
        printf("%lld\n", (((long long int)(n)) * (n+2) * ((2*n)+1))/8);
    }
    return 0;
}
