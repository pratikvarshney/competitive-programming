#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

int main()
{
    int t, i, j, found, odd, odd_index;
    char buffer[128];

    scanf("%d", &t);
    gets(buffer);

    while(t>0)
    {
        int a[26]={0}, b[26]={0}, d[26]={0};
        gets(buffer);
        for(i=0; buffer[i]!='\0'; i++)
        {
            if((buffer[i] >= 'a') && (buffer[i] <= 'z'))
            {
                a[buffer[i]-'a']++;
            }
            if((buffer[i] >= 'A') && (buffer[i] <= 'Z'))
            {
                a[buffer[i]-'A']++;
            }
        }
        gets(buffer);
        for(i=0; buffer[i]!='\0'; i++)
        {
            if((buffer[i] >= 'a') && (buffer[i] <= 'z'))
            {
                b[buffer[i]-'a']++;
            }
            if((buffer[i] >= 'A') && (buffer[i] <= 'Z'))
            {
                b[buffer[i]-'A']++;
            }
        }
        /*printf("line 1:\n");
        for(i=0; i<26; i++)
        {
            if(a[i]>0) printf("%c = %d\n", i+'a', a[i]);
        }
        printf("line 2:\n");
        for(i=0; i<26; i++)
        {
            if(b[i]>0) printf("%c = %d\n", i+'a', b[i]);
        }*/

        found=0;
        odd=0;
        for(i=0; i<26; i++)
        {
            d[i]=abs(a[i]-b[i]);
            if(d[i]>0) found=1;
            if((d[i]%2)!=0)
            {
                odd++;
                odd_index=i;
            }
        }
        if(found==0)
        {
            printf("YES\n");
        }
        else
        {
            if(odd<=1)
            {
                for(i='a'; i<='z'; i++)
                {
                    for(j=(d[i-'a']/2)-1; j>=0; j--)
                    {
                        putchar(i);
                    }
                }
                if(odd==1) putchar(odd_index+'a');
                for(i='z'; i>='a'; i--)
                {
                    for(j=(d[i-'a']/2)-1; j>=0; j--)
                    {
                        putchar(i);
                    }
                }
                printf("\n");
            }
            else
            {
                printf("NO LUCK\n");
            }

        }

        t--;
    }

    return 0;
}
