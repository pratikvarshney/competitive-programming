/*
 * AIBOHP.cpp
 *
 *  Created on: 31-Dec-2016
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int lcs(const char* a, int as, const char* b, int bs);
inline int max(int a, int b);

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		string s;
		cin >> s;
		string rev(s);
		reverse(rev.begin(), rev.end());
		int n = s.length();
		printf("%d\n", n - lcs(s.c_str(), n, rev.c_str(), n));
	}
	return 0;
}

int lcs(const char* a, int as, const char* b, int bs) {
	vector<vector<int> > dp(as + 1, vector<int>(bs + 1, 0));
	for (int i = 0; i < as; i++) {
		for (int j = 0; j < bs; j++) {
			if (a[i] == b[j]) {
				dp[i + 1][j + 1] = 1 + dp[i][j];
			} else {
				dp[i + 1][j + 1] = max(dp[i][j + 1], dp[i + 1][j]);
			}
		}
	}
	return dp[as][bs];
}

inline int max(int a, int b) {
	return (a > b) ? a : b;
}
