#include<stdio.h>

#define MAXSUM 1000

int get_sqr_sum(int val);

int main()
{
    int happy[MAXSUM];
    char processed[MAXSUM];
    int i, n, k, d;
    //preprocess
    for(i=0; i<MAXSUM; i++)
        happy[i] = -1;
    for(i=1; i<MAXSUM; i++)
    {
        memset(processed, 0, MAXSUM);
        k=i;
        d=0;
        while(1)
        {
            d++;
            k = get_sqr_sum(k);
            if(k==1)
            {
                happy[i]=d;
                break;
            }
            else if(processed[k])
            {
                break;
            }
            processed[k]=1;
        }
    }

    //input
    scanf("%d", &n);

    if(n<MAXSUM)
    {
        printf("%d\n", happy[n]);
    }
    else
    {
        k = get_sqr_sum(n);
        if(happy[k] == -1)
        {
            printf("-1\n");
        }
        else
        {
            printf("%d\n", happy[k]+1);
        }
    }

    return 0;
}

int get_sqr_sum(int val)
{
    int sqr[10]={0, 1, 4, 9, 16, 25, 36, 49, 64, 81};
    int sum=0;
    while(val>0)
    {
        sum += sqr[val%10];
        val /= 10;
    }
    return sum;
}
