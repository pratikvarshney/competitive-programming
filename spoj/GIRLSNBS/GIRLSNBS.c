#include<stdio.h>

int main()
{
    int a, b, c;
    while(1)
    {
        scanf("%d%d", &a, &b);
        if((a==-1) && (b==-1)) break;

        if(a<b)
        {
            c = a;
            a = b;
            b = c;
        }

        b++;
        if(a%b==0)
        {
            printf("%d\n", a/b);
        }
        else
        {
            printf("%d\n", (a/b)+1);
        }
    }
    return 0;
}
