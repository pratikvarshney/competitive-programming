#include<stdio.h>
#include<math.h>

/* Standard C Function: Greatest Common Divisor */
int gcd( int a, int b )
{
    int c;
    while ( a != 0 )
    {
        c = a;
        a = b%a;
        b = c;
    }
    return b;
}

int main()
{
    int t, a, b, c, g, s, i;
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d", &a, &b);
        c=0;
        g = gcd(a, b);
        s = round(sqrt(g));
        for(i=1; i<=s; i++)
        {
            if(g%i==0) c+=2;
        }
        if((s*s)==g) c--;

        printf("%d\n", c);
    }
    return 0;
}
