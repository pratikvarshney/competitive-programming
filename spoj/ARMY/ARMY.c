#include<stdio.h>

int main()
{
    int ng, nm;
    int maxg, maxm;
    int i, val;
    int t;

    scanf("%d", &t);

    while(t--)
    {
        scanf("%d%d", &ng, &nm);
        maxg=0;
        maxm=0;
        for(i=0; i<ng; i++)
        {
            scanf("%d", &val);
            if(maxg < val) maxg = val;
        }
        for(i=0; i<nm; i++)
        {
            scanf("%d", &val);
            if(maxm < val) maxm = val;
        }

        if(maxm > maxg)
        {
            puts("MechaGodzilla");
        }
        else
        {
            puts("Godzilla");
        }
    }

    return 0;
}
