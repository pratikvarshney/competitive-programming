#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>

long int get_min_dist(std::vector<long int> &arr, int start, int end, int c);
long int min_diff(std::vector<long int> &arr, int start, int end);

int main()
{
    int t;
    long int n, c, i;
    long int min_dist=0;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%ld%ld", &n, &c);
        std::vector<long int> arr(n, 0);
        for(i=0; i<n; i++)
        {
            std::cin >> arr[i];
        }
        sort(arr.begin(), arr.end());

        min_dist = get_min_dist(arr, 0, n-1, c);

        printf("%ld\n", min_dist);
    }
    return 0;
}

long int get_min_dist(std::vector<long int> &arr, int start, int end, int c)
{
    if(c == 2){
        return arr[end] - arr[start];
    }
    if((end-start)<(c-1)){
        return 0;
    }
    int lo = min_diff(arr, start, end);
    int hi = (arr[end]-arr[start])/(c-1);
    while(lo < hi){
        int mid = lo + (((hi+1)-lo)/2);
        // atleast mid
        int possible = 1; // at start
        int begin = arr[start];
        for(int i = start + 1; i <= end; i++){
            if((arr[i] - begin) >= mid){
                begin = arr[i];
                possible++;
            }
        }

        if(possible<c){
            hi = mid - 1;
        }else{
            lo = mid;
        }
    }
    return lo;
}
long int min_diff(std::vector<long int> &arr, int start, int end){
    if(end>start){
        int min = arr[start+1]-arr[start];
        for(int i = start + 1; i<end; i++){
            int d = arr[i+1] - arr[i];
            if(min>d){
                min = d;
            }
        }
        return min;
    }
    return 0;
}