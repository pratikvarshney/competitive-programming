#include<cstdio>
#include<unordered_set>


int main()
{
    int m, k, p;
    std::unordered_set<int> s;
    while(1)
    {
        scanf("%d", &k);
        if(k==-1) break;

        p=0;
        s.clear();
        s.insert(p);
        for(m=1; m<=k; m++)
        {
            if(p>m && s.find(p-m)==s.end())
            {
                p -= m;

            }
            else
            {
                p += m;
            }
            s.insert(p);
        }
        printf("%d\n", p);
    }
    return 0;
}
