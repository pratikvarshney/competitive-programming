#include<stdio.h>

int main()
{
    int t, x, y;

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%d%d", &x, &y);
        if(x==y)
        {
            if(x%2==0) printf("%d\n", 2*x);
            else printf("%d\n", (2*(x-1))+1);
        }
        else if(x==(y+2))
        {
            if(y%2==0) printf("%d\n", (2*y)+2);
            else printf("%d\n", (2*(y-1))+3);
        }
        else
        {
            printf("No Number\n");
        }
        t--;
    }

    return 0;
}
