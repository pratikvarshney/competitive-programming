#include<stdio.h>

int match(int a[], int b[], int n);

int main()
{
    int a[100001];
    int b[100001];
    int i, n;
    while(1)
    {
        scanf("%d", &n);
        if(n==0) break;
        for(i=1; i<=n; i++)
        {
            scanf("%d", &(a[i]));
            b[a[i]]=i;
        }
        if(match(a, b, n)) puts("ambiguous");
        else puts("not ambiguous");
    }
    return 0;
}

int match(int a[], int b[], int n)
{
    int i;
    for(i=1; i<=n; i++)
    {
        if(a[i]!=b[i])
        {
            return 0;
        }
    }
    return 1;
}
