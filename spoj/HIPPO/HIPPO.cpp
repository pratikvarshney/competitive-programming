#include<iostream>
#include<cstdio>
#include<deque>
#include<queue>

using namespace std;

#define MAX_SIZE 20
#define MAX_VAL 2147483647

int spfa(deque<int> graph[], int a, int f, int n);

int main()
{
    int h, w, i, j, k, l;
    int c_no;
    char mat[MAX_SIZE][MAX_SIZE];
    int t;
    scanf("%d", &t);

    for(c_no=1; c_no<=t; c_no++)
    {
        scanf("%d%d", &h, &w);
        for(i=0; i<h; i++)
        {
            scanf("%s", mat[i]);
        }

        bool d[w*h];

        deque<int> tunnel[26];
        deque<int> graph[w*h];

        int a, b, c, f;
        char ch;

        a=-1;
        b=-1;
        c=-1;
        f=-1;

        for(i=0; i<h; i++)
        {
            for(j=0, k=(i*w); j<w; j++, k++)
            {
                ch = mat[i][j];

                if(ch == 'D')
                {
                    d[k]=true;
                }
                else
                {
                    d[k]=false;
                    if(ch == 'A') a=k;
                    else if(ch == 'B') b=k;
                    else if(ch == 'C') c=k;
                    else if(ch == '#') f=k;
                    else if((ch >= 'E') && (ch <= 'Z'))
                    {
                        tunnel[ch - 'A'].push_back(k);
                    }
                }
            }
        }

        if(a==-1 || b==-1 || c==-1 || f==-1)
        {
            cout << "Case "<< c_no <<": ";
            cout << "impossible" << endl;
            continue;
        }

        for(i=0; i<h; i++)
        {
            for(j=0, k=(i*w); j<w; j++, k++)
            {
                ch = mat[i][j];
                if(ch != 'D')
                {
                    if(i>0 && j>0)
                    {
                        l = ((i-1)*w) + (j-1);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if(i>0)
                    {
                        l = ((i-1)*w) + (j);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if(i>0 && (j+1)<w)
                    {
                        l = ((i-1)*w) + (j+1);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if(j>0)
                    {
                        l = ((i)*w) + (j-1);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if((j+1)<w)
                    {
                        l = ((i)*w) + (j+1);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if((i+1)<h && j>0)
                    {
                        l = ((i+1)*w) + (j-1);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if((i+1)<h)
                    {
                        l = ((i+1)*w) + (j);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if((i+1)<h && (j+1)<w)
                    {
                        l = ((i+1)*w) + (j+1);
                        if(!d[l]) graph[k].push_back(l);
                    }
                    if((ch >= 'E') && ch <= 'Z')
                    {
                        l = ch - 'A';
                        for(deque<int>::iterator it=tunnel[l].begin(); it != tunnel[l].end(); it++)
                        {
                            if((*it) != k) graph[k].push_back(*it);
                        }
                    }
                }
            }
        }

        cout << "Case "<< c_no <<": ";

        /*for(i=0; i<(w*h); i++)
        {
            cout << i << " -> ";
            for(deque<int>::iterator it=graph[i].begin(); it != graph[i].end(); it++)
            {
                cout << ", " << *it;
            }
            cout << endl;
        }*/

        int min_dist = spfa(graph, a, f, (w*h));
        if(min_dist == MAX_VAL)
        {
            cout << "impossible" << endl;
        }
        else
        {
            int curr_dist = spfa(graph, b, f, (w*h));
            if(curr_dist == MAX_VAL)
            {
                cout << "impossible" << endl;
            }
            else
            {
                if(min_dist < curr_dist) min_dist = curr_dist;
                curr_dist = spfa(graph, c, f, (w*h));
                if(curr_dist == MAX_VAL)
                {
                    cout << "impossible" << endl;
                }
                else
                {
                    if(min_dist < curr_dist) min_dist = curr_dist;
                    cout << min_dist << endl;
                }
            }
        }

    }
    return 0;
}

int spfa(deque<int> graph[], int a, int f, int n)
{
    queue<int> q;
    bool inq[n];
    int dist[n];
    int i;
    for(i=0; i<n; i++)
    {
        dist[i]=MAX_VAL;
        inq[i]=false;
    }
    dist[a]=0;
    q.push(a);
    inq[a]=true;

    while(!q.empty())
    {
        int curr=q.front();
        int nxt = dist[curr]+1;
        int adj;
        q.pop();
        inq[curr]=false;

        for(deque<int>::iterator it=graph[curr].begin(); it != graph[curr].end(); it++)
        {
            adj = *it;
            if((dist[adj] > nxt))
            {
                dist[adj] = nxt;
                if(!inq[adj])
                {
                    q.push(adj);
                    inq[adj]=true;
                }
            }
        }

    }

    /*cout << endl;
    for(i=0; i<n; i++)
    {
        cout << dist[i] << ",";
    }
    cout << endl; */

    return dist[f];
}
