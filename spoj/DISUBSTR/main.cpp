/*
 * main.cpp
 *
 *  Created on: 08-Jan-2017
 *      Author: Pratik Varshney
 */

#include <iostream>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

struct node {
	int start, end, index;
	map<char, node*> next;
};

node* addToSuffixtree(node* stree, const char* str, int index, int start,
		int n) {
	node* curr = stree;
	int i = start;
	while (i <= n) {
		map<char, node*>::iterator it = curr->next.find(str[i]);
		if (it == curr->next.end()) {
			node* tmp = new node;
			tmp->start = i;
			tmp->end = n;
			tmp->index = index;
			curr->next[str[i]] = tmp;
			return stree;
		} else {
			node* existing = it->second;
			int j = existing->start;
			int k = existing->start;
			while (i <= n && j <= existing->end && str[i] == str[j]) {
				i++;
				j++;
			}

			if (i <= n) {
				if (j > existing->end) {
					// case 1: continue to next
					curr = it->second;
				} else {
					// case 2: split in middle
					node* parentNode = new node;
					node* newChild = new node;
					newChild->start = i;
					newChild->end = n;
					newChild->index = index;
					parentNode->next[str[i]] = newChild; // i = newChild->start
					parentNode->start = k;
					parentNode->end = j - 1;
					parentNode->index = -1;
					existing->start = j;
					parentNode->next[str[j]] = existing; // j = existing->start
					curr->next[str[k]] = parentNode; // k = parentNode->start
					return stree;
				}
			} // else do nothing
		}
	}
	return stree;
}

node* getSuffixTree(const char* str, int n) {
	node* stree = new node;
	stree->index = -1;
	for (int start = n - 1; start >= 0; start--) {
		stree = addToSuffixtree(stree, str, start, start, n);
	}
	return stree;
}

void setSuffixArray(vector<int>& suffixArray, node* suffixTree) {
	if (suffixTree->index >= 0) {
		suffixArray.push_back(suffixTree->index);
	} else {
		for (map<char, node*>::iterator it = suffixTree->next.begin();
				it != suffixTree->next.end(); it++) {
			setSuffixArray(suffixArray, it->second);
		}
	}
}

int calcLcpAndReturnHeightSum(const char* str, int n,
		vector<int>& suffixArray) {
	int sum = 0;
	for (int i = 1; i < n; i++) {
		int j = suffixArray[i - 1];
		int k = suffixArray[i];
		int l = 0;
		while (str[j] == str[k] && j < n && k < n) {
			j++;
			k++;
			l++;
		}
		sum += l;
	}
	return sum;
}

int solve() {
	char str[1002];
	scanf("%s", str);
	int n = 0;
	while (str[n] != '\0') {
		n++;
	}

	node* suffixTree = getSuffixTree(str, n);
	// suffix array
	vector<int> suffixArray;
	setSuffixArray(suffixArray, suffixTree);
	int dup = calcLcpAndReturnHeightSum(str, n, suffixArray);
	return ((n * (n + 1)) / 2) - dup;
}

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		printf("%d\n", solve());
	}
	return 0;
}
