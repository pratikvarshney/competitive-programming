#include<stdio.h>

#define MAXM 501

int main()
{
    char s[MAXM];
    char t[MAXM];
    int i, c;

    while(1)
    {
        scanf("%s%s", s, t);
        if(s[0]=='*' && t[0]=='*') break;

        c=0;
        for(i=0; s[i]!='\0'; i++)
        {
            if(s[i]!=t[i])
            {
                c++;
                i++;
                while(s[i]!=t[i])
                {
                    i++;
                }
                i--;
            }
        }
        printf("%d\n", c);
    }

    return 0;
}
