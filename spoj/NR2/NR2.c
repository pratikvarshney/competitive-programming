#include<stdio.h>

int main()
{
    long long unsigned res_and, res_or, val;
    int n;

    scanf("%d", &n);
    scanf("%llu", &val);
    res_and = val;
    res_or = val;
    n--;

    while(n--)
    {
        scanf("%llu", &val);
        res_and &= val;
        res_or |= val;
    }
    printf("%llu", (~res_and) & res_or);

    return 0;
}
