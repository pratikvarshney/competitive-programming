#include<stdio.h>
#include<string.h>
int main()
{
    unsigned long long mask[10] = {0b0, 0b0, 0b11, 0b11, 0b1, 0b0, 0b0, 0b11, 0b11, 0b1};
    int mat[10][4] = {{0,0,0,0},{1,1,1,1},{6,2,4,8},{1,3,9,7},{6,4,6,4},{5,5,5,5},{6,6,6,6},{1,7,9,3},{6,8,4,2},{1,9,1,9}};
    int t, d;
    unsigned long long b;
    char a[2000];
    scanf("%d", &t);
    for(;t>0; t--)
    {
        scanf("%s%llu", a, &b);
        d=a[strlen(a)-1]-'0';
        if(b==0) printf("1\n");
        else printf("%d\n", mat[d][b & mask[d]]);
    }
    return 0;
}
