/*
 * main.cpp
 *
 *  Created on: 01-Jan-2017
 *      Author: Pratik Varshney
 */

// start = 12:56pm, end = 01:16, time taken  = 20 min
#include <cstdio>

int main() {
	char a[2002];
	for (int t = 1;; t++) {
		scanf("%s", a);
		if (a[0] == '-') {
			break;
		}
		int min = 0;
		int l = 0, r = 0;
		for (int i = 0; a[i] != '\0'; i++) {
			if (a[i] == '{') {
				l++;
			} else {
				r++;
			}
			if (r > l) {
				// mismatch
				min++;
				// change } to {
				l++;
				r--;
			}
		}
		if (l > r) {
			// change half of { to }
			min += ((l - r) / 2);
		}

		printf("%d. %d\n", t, min);
	}

	return 0;
}
