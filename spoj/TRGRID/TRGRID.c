#include<stdio.h>

int main()
{
    int t, n, m, k;
    char dir;
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d", &n, &m);

        //trim
        if(n>m)
        {
            k = (m-1)/2;
        }
        else
        {
            k = (n-1)/2;
        }
        k = 2*k;
        n = n-k;
        m = m-k;

        //proc
        if(n==1)
        {
            printf("R\n");
        }
        else if(m==1)
        {
            printf("D\n");
        }
        else if(n==2)
        {
            printf("L\n");
        }
        else
        {
            printf("U\n");
        }

    }
    return 0;
}
