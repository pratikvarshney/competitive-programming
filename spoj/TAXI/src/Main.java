import java.util.*;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while (t > 0) {
			solve(sc);
			t--;
		}
	}

	public static void solve(Scanner sc) {
		int p = sc.nextInt();
		int t = sc.nextInt();
		long s = sc.nextLong();
		long c = sc.nextLong();

		long[][] person = new long[p][2];
		long[][] taxi = new long[t][2];

		for (int i = 0; i < p; i++) {
			person[i][0] = sc.nextLong();
			person[i][1] = sc.nextLong();
		}

		for (int i = 0; i < t; i++) {
			taxi[i][0] = sc.nextLong();
			taxi[i][1] = sc.nextLong();
		}

		// make network flow graph

		// src
		int src = 0;

		// sink
		int sink = 1;

		int[] pNode = new int[p];
		for (int i = 0; i < p; i++) {
			pNode[i] = 2 + i;
		}

		int[] tNode = new int[t];
		for (int i = 0; i < t; i++) {
			tNode[i] = 2 + p + i;
		}

		// assign weights
		int n = 2 + p + t;
		int[][] cap = new int[n][n];
		List<HashSet<Integer>> adj = new ArrayList<HashSet<Integer>>(n);
		for (int i = 0; i < n; i++) {
			adj.add(new HashSet<Integer>());
		}

		for (int i = 0; i < p; i++) {
			cap[src][pNode[i]] = 1;
			adj.get(src).add(pNode[i]);
		}
		for (int i = 0; i < t; i++) {
			cap[tNode[i]][sink] = 1;
			adj.get(tNode[i]).add(sink);
		}
		long maxDist = (s * c) / 200;
		for (int i = 0; i < p; i++) {
			for (int j = 0; j < t; j++) {
				if (getManhattanDist(person[i], taxi[j]) <= maxDist) {
					cap[pNode[i]][tNode[j]] = 1;
					adj.get(pNode[i]).add(tNode[j]);
				}
			}
		}

		System.out.println(maxFlow(src, sink, cap, adj));

	}

	public static long maxFlow(int src, int sink, int[][] c, List<HashSet<Integer>> adj) {
		// process flow

		while (true) {
			int[] path = getPath(src, sink, c, adj);
			if (path.length == 0) {
				break;
			}
			int min = c[path[0]][path[1]];
			for (int i = 2; i < path.length; i++) {
				if (min > c[path[i - 1]][path[i]]) {
					min = c[path[i - 1]][path[i]];
				}
			}
			for (int i = 1; i < path.length; i++) {

				c[path[i - 1]][path[i]] -= min;
				if (c[path[i - 1]][path[i]] == 0) {
					adj.get(path[i - 1]).remove(path[i]);
				}

				c[path[i]][path[i - 1]] += min;
				if (c[path[i]][path[i - 1]] == min) {
					adj.get(path[i]).add(path[i - 1]);
				}
			}
		}

		long maxflow = 0;
		for (int i = 0; i < c.length; i++) {
			maxflow += c[i][src];
		}
		return maxflow;
	}

	public static int[] getPath(int src, int sink, int[][] c, List<HashSet<Integer>> adj) {
		// dfs
		Stack<Integer> s = new Stack<Integer>();
		boolean[] visited = new boolean[c.length];

		s.push(src);
		visited[src] = true;
		int[] origin = new int[c.length];
		origin[src] = -1;

		while (!s.empty()) {
			int i = s.pop();
			if (i == sink) {
				List<Integer> a = new ArrayList<Integer>();
				a.add(i);
				for (int j = origin[i]; j != -1; j = origin[j]) {
					a.add(j);
				}
				int k = a.size();
				int[] path = new int[k];
				for (Integer j : a) {
					--k;
					path[k] = j;
				}
				return path;
			}
			HashSet<Integer> set = adj.get(i);
			for (int j : set) {
				if (!visited[j] && c[i][j] > 0) {
					s.push(j);
					origin[j] = i;
					visited[j] = true;
				}
			}
		}

		return new int[] {};
	}

	public static long getManhattanDist(long[] p, long[] t) {
		return abs(p[0] - t[0]) + abs(p[1] - t[1]);
	}

	public static long abs(long a) {
		return (a < 0) ? (-a) : a;
	}

}
