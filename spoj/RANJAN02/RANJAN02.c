#include<stdio.h>

long long unsigned m[36]={0};

int main()
{
    int i, t;
    long long unsigned a=1;
    m[0]=0;
    for(i=1; i<=35; i++)
    {
        m[i]=m[i-1]+(2*a);
        a *= 3;
    }

    scanf("%d", &t);

    while(t>0)
    {
        scanf("%d", &i);
        printf("%llu\n", m[i]);
        t--;
    }

    return 0;
}
