/*
 * main.cpp
 *
 *  Created on: 29-Jan-2017
 *      Author: Pratik Varshney
 */

#include <algorithm>
#include <vector>
#include <cstdio>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

bool cmp(const pair<int, int>& lhs, const pair<int, int>& rhs) {
	return lhs.second < rhs.second;
}

void solve() {
	int n;
	scanint(n);
	if (n < 1) {
		printf("0\n");
	}
	int a, b;
	std::vector<std::pair<int, int> > v;
	for (int i = 0; i < n; i++) {
		scanint(a);
		scanint(b);
		v.push_back(std::make_pair(a, b));
	}
	sort(v.begin(), v.end(), cmp);
	int last = 0;
	int count = 1;
	for (int i = 1; i < n; i++) {
		if (v[last].second <= v[i].first) {
			last = i;
			count++;
		}
	}
	printf("%d\n", count);
}

int main() {
	int t;
	scanint(t);
	while (t--) {
		solve();
	}
	return 0;
}
