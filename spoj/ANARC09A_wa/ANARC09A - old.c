// Eliminate balanced {} => n times... remaining unbalanced... change left half to { (if req) ... and right half }

#include<stdio.h>
#include<string.h>

#define MAXM 2002
#define MARK '*'

int trimmer(char *str);
int unmark(char *str);

int main()
{
    int i, ch, len;
    char str[MAXM];
    int k=1;

    while(1)
    {
        scanf("%s", str);
        if(str[0]=='-') break;

        while(trimmer(str));

        //printf("REM: %s\n");

        ch = 0;
        len = strlen(str);
        for(i=0; i<len/2; i++)
        {
            if(str[i]!='{')
            {
                str[i] = '{';
                ch++;
            }
        }
        for(; i<len; i++)
        {
            if(str[i]!='}')
            {
                str[i] = '}';
                ch++;
            }
        }
        printf("%d. %d\n", k, ch);
        k++;
    }

    return 0;
}

int trimmer(char *str)
{
    int ch=0;
    int i;
    char pre;

    if(str[0]=='\0') return 0;
    pre = 0;
    for(i=1; str[i]!='\0'; i++)
    {
        if(str[pre]=='{' && str[i]=='}')
        {
            str[pre] = MARK;
            str[i] = MARK;
            ch++;
            i++;
            if(str[i]=='\0') break;
        }
        pre = i;
    }
    unmark(str);

    return ch;
}

int unmark(char *str)
{
    char temp[MAXM];
    int i, j;
    for(i=0, j=0; str[i]!='\0'; i++)
    {
        if(str[i]!=MARK)
        {
            temp[j] = str[i];
            j++;
        }
    }
    temp[j] = '\0';
    strcpy(str, temp);
    str[j] = '\0';
    return 0;
}
