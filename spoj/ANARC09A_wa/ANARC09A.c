// Eliminate balanced {} => n times... remaining unbalanced... change left half to { (if req) ... and right half }

#include<stdio.h>
#include<string.h>

#define MAXM 2002
#define MARK '*'

int main()
{
    int i, ch, len;
    char str[MAXM];
    char stk[MAXM];
    int top;
    int k=1;

    while(1)
    {
        scanf("%s", str);
        if(str[0]=='-') break;

        top=0;
        stk[top] = '*'; //NULL
        for(i=0; str[i]!='\0'; i++)
        {
            if(stk[top]=='{' && str[i]=='}')
            {
                top--; //pop
            }
            else
            {
                top++;
                stk[top]=str[i];//push
            }
        }
        ch=0;
        for(i=1; i<=top/2; i++)
        {
            if(stk[i]=='}')
            {
                stk[i]='{';
                ch++;
            }
        }
        for(; i<=top; i++)
        {
            if(stk[i]=='{')
            {
                stk[i]='}';
                ch++;
            }
        }

        printf("%d. %d\n", k, ch);
        k++;
    }

    return 0;
}
