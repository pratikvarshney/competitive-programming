#include<cstdio>
#include<iostream>
#include<algorithm>
#include<vector>

struct ivpair
{
    int index, value;
};

bool comp(struct ivpair i,struct ivpair j) { return (i.value<j.value); }

int main()
{
    int i, n, k, val;

    scanf("%d", &n);

    std::vector<ivpair> arr(n);

    for(i=0; i<n; i++)
    {
        scanf("%d", &val);
        arr[i].index = i;
        arr[i].value = val;
    }

    scanf("%d", &k);

    std::vector<ivpair> myheap(arr.begin(), arr.begin()+k);
    std::make_heap(myheap.begin(), myheap.end(), comp);

    std::cout<<myheap.front().value;

    for(i=k; i<n; i++)
    {
        myheap.push_back(arr[i]);
        std::push_heap(myheap.begin(), myheap.end(), comp);

        while(myheap.front().index + k <= i)
        {
            std::pop_heap(myheap.begin(), myheap.end(), comp);
            myheap.pop_back();
        }

        std::cout<< " " <<myheap.front().value;
    }

    return 0;
}
