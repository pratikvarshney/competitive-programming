#include<stdio.h>
#include<math.h>

int primes(int a, int b);

int main()
{
    int a, b, t;

    scanf("%d",&t);
    while(t>0)
    {
        scanf("%d%d",&a,&b);
        primes(a,b);
        t--;
    }

    return 0;
}

int primes(int a, int b)
{
    char *noprime = (char*)calloc(b+1, sizeof(char));

    int limit=sqrt(b);
    int check[limit];

    int sievebound = (limit-1)/2;
    char *sieve = (char*)calloc(sievebound+1, sizeof(char));
    int crosslimit = sqrt(limit-1)/2;
    int i, j, k, p;
    for(i=1; i<=crosslimit; i++)
    {
        if(!sieve[i])
        {
            for(j=2*i*(i+1); j<=sievebound; j+=(2*i)+1)
            {
                sieve[j]=1;
            }
        }
    }

    k=0;
    /*skipping 2, else check[k]=2; k++;*/
    if(a<=2 && b>=2) printf("2\n");
    for(i=1; i<=sievebound; i++)
    {
        if(!sieve[i])
        {
            check[k]=((2*i)+1);
            if(check[k]>=a) printf("%d\n", check[k]);
            k++;
        }
    }

    if(k>0 && a<=check[k-1])
    {
        a=check[k-1]+1;
    }

    for(i=0; i<k; i++)
    {
        p=check[i];
        for(j=(a-(a%p)); j<=b; j+=p)
        {
            noprime[j]=1;
        }
    }
    noprime[1]=1;

    if(a%2==0)
    {
        a++;
    }


    for(i=a; i<=b; i+=2)
    {
        if(!noprime[i]) printf("%d\n",i);
    }
    printf("\n");
    free(noprime);
    free(sieve);
    return 0;
}
