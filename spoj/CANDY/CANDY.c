#include<stdio.h>

int main()
{
    int arr[10000];
    int i, n, sum, avg, moves;

    while(1)
    {
        scanf("%d", &n);
        if(n==-1) break;

        sum=0;

        for(i=0; i<n; i++)
        {
            scanf("%d", &(arr[i]));
            sum += arr[i];
        }

        if((sum%n)==0)
        {
            avg = sum/n;
            moves = 0;

            for(i=0; i<n; i++)
            {
                if(arr[i] > avg)
                {
                    moves += (arr[i] - avg);
                }
            }
            printf("%d\n", moves);

        }
        else
        {
            printf("-1\n");
        }

    }
    return 0;
}
