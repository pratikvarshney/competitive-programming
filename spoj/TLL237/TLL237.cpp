#include<cstdio>
#include <queue>          // std::priority_queue
#include <vector>         // std::vector
#include <functional>     // std::greater

int main()
{
    int n;
    char ch;
    scanf("%d", &n);
    std::vector<int> data(n, 0);
    std::priority_queue<int, std::vector<int>, std::greater<int> > myq(data.begin(), data.end());
    do{
        ch = getchar();
    }while(ch<'1' || ch>'2');

    int a, b;

    while(ch=='1' || ch=='2')
    {
        a = myq.top();
        myq.pop();
        if(ch=='1')
        {
            a++;
            myq.push(a);
        }
        else
        {
            b = myq.top();
            myq.pop();
            b++;
            myq.push(a);
            myq.push(b);
        }
        ch = getchar();
    }
    a = myq.top();
    myq.pop();
    b = myq.top();

    printf("%d %d\n", a, b);

    return 0;
}
