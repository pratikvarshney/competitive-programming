#include<stdio.h>

#define OFFSET 1000

int maxm();

int main()
{
    int t;

    scanf("%d", &t);
    for(; t>0; t--)
        maxm();

    return 0;
}

int maxm()
{
    int n, i, k;
    int maxi, maxv;
    int a[(2*(OFFSET))+1]={0};
    scanf("%d", &n);
    for(i=0; i<n; i++)
    {
        scanf("%d", &k);
        a[k+OFFSET]++;
    }

    maxv = 0;
    maxi = 0;

    for(i=2*(OFFSET); i>=0; i--)
    {
        if(maxv < a[i])
        {
             maxv = a[i];
             maxi = i;
        }
    }
    if(maxv > (n/2))
    {
        printf("YES %d\n", maxi-OFFSET);
    }
    else printf("NO\n");
    return 0;
}
