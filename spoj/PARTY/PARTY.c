#include<stdio.h>

#define MAXVAL 100
#define MAXBUDGET 500

int main()
{
    int fee[MAXVAL], fun[MAXVAL];
    int budget, n;
    int i, j;

    int k[MAXVAL+1][MAXBUDGET+1];
    int w[MAXVAL+1][MAXBUDGET+1];

    while(1)
    {
        scanf("%d%d", &budget, &n);
        if((budget==0) && (n==0)) break;

        for(i=0; i<n; i++)
        {
            scanf("%d%d", &(fee[i]), &(fun[i]));
        }

        for(i=0; i<=n; i++)
        {
            for(j=0; j<=budget; j++)
            {
                if(i==0 || j==0) //0 items or 0 budget => 0 items
                {
                    k[i][j] = 0;
                    w[i][j] = 0;
                }
                else if(fee[i-1] <= j)
                {
                    //add item
                    if( (fun[i-1] + k[i-1][j-fee[i-1]]) > k[i-1][j])
                    {
                        k[i][j] = (fun[i-1] + k[i-1][j-fee[i-1]]);
                        w[i][j] = (fee[i-1] + w[i-1][j-fee[i-1]]);
                    }
                    else if((fun[i-1] + k[i-1][j-fee[i-1]]) == k[i-1][j])
                    {
                        if(fee[i-1] + w[i-1][j-fee[i-1]] < w[i-1][j]) //replace if less cost
                        {
                            k[i][j] = (fun[i-1] + k[i-1][j-fee[i-1]]);
                            w[i][j] = (fee[i-1] + w[i-1][j-fee[i-1]]);
                        }
                        else
                        {
                            // don't add item
                            k[i][j] = k[i-1][j];
                            w[i][j] = w[i-1][j];
                        }
                    }
                    else
                    {
                        // don't add item
                        k[i][j] = k[i-1][j];
                        w[i][j] = w[i-1][j];
                    }
                }
                else
                {
                    // don't add item, weight exceed
                    k[i][j] = k[i-1][j];
                    w[i][j] = w[i-1][j];
                }
            }
        }
        printf("%d %d\n", w[n][budget], k[n][budget]);
    }
    return 0;
}
