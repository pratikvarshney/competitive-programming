#include<stdio.h>
#include<string.h>

//TLE
//TODO : try lempel-ziff

int is_n_substr(char *str, char *query, int n);
int last_occur(char *str, char *query);//note: input str will be modified
char *str_rev(char *str);// inplace


int main()
{
    char str[50002];
    char query[50002];
    int n, i, j, found, len;
    int t;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%s%d", str, &n);
        len=strlen(str);
        found=0;
        for(i=(len-n)+1; i>0; i--) // i = length of sub string
        {
            for(j=(len-i); j>=0; j--) // j = offset
            {
                strncpy(query, str+j, i);
                query[i]='\0';
                if(is_n_substr(str, query, n))
                {
                    printf("%d %d\n", i, last_occur(str, query));
                    found=1;
                    break;
                }
            }
            if(found)break;
        }
        if(!found)printf("HATE\n");
    }
    return 0;
}
int is_n_substr(char *str, char *query, int n)
{
    int k=0;
    char *base;
    base=str;
    //printf("CHECK: %s\n", query);
    while(k<n)
    {
        base=strstr(base, query);
        if(base==NULL) break;
        base++;
        k++;
    }
    return k==n;
}
int last_occur(char *str, char *query)
{
    //assuming query is present in str && already checked
    int a=strlen(str);
    int b=strlen(query);
    str_rev(str);
    str_rev(query);
    return a - ((strstr(str, query) - str) + b - 1) ;

}
char *str_rev(char *str)
{
    char *l, *r;
    char t;
    if(!str || !*str)return str;
    for(l=str, r=str+(strlen(str)-1); r>l; l++, r--)
    {
        t=*l;
        *l=*r;
        *r=t;
    }
    return str;
}
