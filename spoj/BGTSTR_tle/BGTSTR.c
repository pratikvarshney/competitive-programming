#include<stdio.h>
#include<string.h>

int is_n_substr(char *str, char *query, int n);
int last_occur(char *str, char *query);//note: input str will be modified
char *str_rev(char *str);// inplace

char marked[50002];
int loc[50002];

int main()
{
    char str[50002];
    char query[50002];
    int n, i, j, found, len;
    int t;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%s%d", str, &n);
        if(n==1)
        {
            printf("%d %d\n", n, 0);
            continue;
        }
        len=strlen(str);
        found=0;
        int min=1, max=(len-n)+1;
        while(min<=max)
        {
            i=(min+max)/2; // i = length of sub string
            memset(marked, 0, (len-i));
            found=0;
            for(j=(len-i); j>=0; j--) // j = offset
            {
                if(marked[j]) continue;
                marked[j]=1;
                strncpy(query, str+j, i);
                query[i]='\0';
                if(is_n_substr(str, query, n))
                {
                    found=1;
                    loc[i]=j;
                    break;
                }
            }
            if(found) min = i+1;
            else max = i-1;
        }
        if(min==1)printf("HATE\n");
        else
        {
            i=min-1;
            j=loc[i];
            strncpy(query, str+j, i);
            query[i]='\0';
            printf("%d %d\n", i, last_occur(str, query));
        }
    }
    return 0;
}
int is_n_substr(char *str, char *query, int n)
{
    int k=0;
    char *base;
    base=str;
    //printf("CHECK: %s\n", query);
    while(k<n)
    {
        base=strstr(base, query);
        if(base==NULL) break;
        marked[base-str]=1;
        base++;
        k++;
    }
    return k==n;
}
int last_occur(char *str, char *query)
{
    //assuming query is present in str && already checked
    int a=strlen(str);
    int b=strlen(query);
    str_rev(str);
    str_rev(query);
    return a - ((strstr(str, query) - str) + b - 1) ;

}
char *str_rev(char *str)
{
    char *l, *r;
    char t;
    if(!str || !*str)return str;
    for(l=str, r=str+(strlen(str)-1); r>l; l++, r--)
    {
        t=*l;
        *l=*r;
        *r=t;
    }
    return str;
}
