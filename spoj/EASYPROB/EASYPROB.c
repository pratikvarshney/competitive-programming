#include<stdio.h>

int power[15]={1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384};
int disp(int n);

int main()
{
    int inp[7]={137, 1315, 73, 136, 255, 1384, 16385};
    int i;

    for(i=0; i<7; i++)
    {
        printf("%d=", inp[i]);
        disp(inp[i]);
        printf("\n");
    }

    return 0;
}
int disp(int n)
{
    int i, flag=0;
    for(i=14; i>1; i--)
    {
        if(n>=power[i])
        {
            if(flag) printf("+");
            printf("2(");
            disp(i);
            printf(")");
            n -= power[i];
            flag=1;
        }
    }
    if(n>=2)
    {
        if(flag) printf("+");
        printf("2");
        n -= 2;
        flag=1;
    }
    if(n==1)
    {
        if(flag) printf("+");
        printf("2(0)");
    }

    return 0;
}
