/*
 * main.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: pratik
 */

#include <cstdio>
#include <iostream>
#include <sstream>
#include <vector>
#include <regex>

using namespace std;

// -std=c++11

int main() {
	int maxt;
	cin >> maxt;
	string line;
	getline(cin, line); // skip \r\n
	for (int t = 1; t <= maxt; t++) {
		getline(cin, line);
		std::smatch m;
		regex e(
				"[a-zA-Z0-9][a-zA-Z0-9._]{4,}@[a-zA-Z0-9]+.(com|edu|org|co.in)");
		vector<string> res;
		while (std::regex_search(line, m, e)) {
			res.push_back(m[0]);
			line = m.suffix().str();
		}
		cout << "Case #" << t << ": " << res.size() << endl;
		for (auto s : res)
			cout << s << endl;
	}
	return 0;
}

