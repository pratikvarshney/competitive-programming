/*
 * main.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: pratik
 */

#include <cstdio>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

// -std=c++11

int checkEmailId(const char* s, int len, vector<string>& res) {
	// min len = 5(user) + 1(@) + 1(site) + 4(.com) = 11
	if (len < 11) {
		return len;
	}
	int i = 0;
	if (!((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z')
			|| (s[i] >= '0' && s[i] <= '9'))) {
		return 1; // skip 1
	}
	i++;
	while ((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z')
			|| (s[i] >= '0' && s[i] <= '9') || (s[i] == '.' || s[i] == '_')) {
		i++;
	}

	if (i < 5) {
		return i; // skip processed
	}

	// check @
	if (i >= len) {
		return len; // skip all
	}
	if (s[i] != '@') {
		return (i + 1); // skip i+1
	}
	i++;
	int at = i;

	// check site
	while ((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z')
			|| (s[i] >= '0' && s[i] <= '9')) {
		i++;
	}
	// site must be non empty
	if (i == at) {
		return at; // skip till @
	}
	if (i >= len) {
		return len; // skip all
	}
	if (s[i] != '.') {
		return at; // skip till @
	}
	i++;

	// "com", "org", "edu", "co.in"
	if (s[i] == 'c' && s[i + 1] == 'o' && s[i + 2] == 'm') {
		i += 3;
		res.push_back(string(s, 0, i));
		return i;
	}
	if (s[i] == 'o' && s[i + 1] == 'r' && s[i + 2] == 'g') {
		i += 3;
		res.push_back(string(s, 0, i));
		return i;
	}
	if (s[i] == 'e' && s[i + 1] == 'd' && s[i + 2] == 'u') {
		i += 3;
		res.push_back(string(s, 0, i));
		return i;
	}
	if (s[i] == 'c' && s[i + 1] == 'o' && s[i + 2] == '.' && s[i + 3] == 'i'
			&& s[i + 4] == 'n') {
		i += 5;
		res.push_back(string(s, 0, i));
		return i;
	}

	return at;
}

vector<string> getEmailIds(const char* s, int len) {
	vector<string> res;
	int offset = 0;
	while (offset < len) {
		offset += checkEmailId(s + offset, len - offset, res);
	}
	return res;
}

int main() {
	int maxt;
	cin >> maxt;
	string line;
	getline(cin, line); // skip \r\n
	for (int t = 1; t <= maxt; t++) {
		getline(cin, line);
		vector<string> res = getEmailIds(line.c_str(), line.size());
		cout << "Case #" << t << ": " << res.size() << endl;
		if (res.size() > 0) {
			for (string s : res) {
				cout << s << endl;
			}
		}
	}
	return 0;
}

