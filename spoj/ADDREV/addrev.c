#include<stdio.h>

#define MAX_SIZE 100

int main()
{
    int t, carry, i, j;
    char a[MAX_SIZE], b[MAX_SIZE], c[MAX_SIZE];
    scanf("%d", &t);
    while(t>0)
    {
        scanf("%s%s", a, b);
        carry=0;

        for(i=0; (a[i]!='\0') && (b[i]!='\0') ; i++)
        {
            c[i]=a[i]+(b[i]-'0')+carry;
            if(c[i]>'9')
            {
                carry=1;
                c[i] -= 10;
            }
            else
            {
                carry = 0;
            }
        }
        if(a[i]!='\0')
        {
            while(a[i]!='\0')
            {
                c[i]=a[i]+carry;
                if(c[i]>'9')
                {
                    carry=1;
                    c[i] -= 10;
                }
                else
                {
                    carry = 0;
                }
                i++;
            }
        }
        else if(b[i]!='\0')
        {
            while(b[i]!='\0')
            {
                c[i]=b[i]+carry;
                if(c[i]>'9')
                {
                    carry=1;
                    c[i] -= 10;
                }
                else
                {
                    carry = 0;
                }
                i++;
            }
        }
        if(carry!=0)
        {
            c[i]=carry+'0';
            i++;
        }
        j=0;
        while(c[j]=='0')
        {
            j++;
        }
        while(j<i)
        {
            printf("%c", c[j]);
            j++;
        }
        printf("\n");

        t--;
    }
    return 0;
}

