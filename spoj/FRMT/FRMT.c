#include<stdio.h>
#include<string.h>

#define MAX_WIDTH 80
#define MAX_ROW 25
#define MAX_COL 80

int show(char *str, int width, char align);

int main()
{
    int row, col, len, width[MAX_COL];
    char tabl[MAX_ROW][MAX_COL];
    char fmt[MAX_WIDTH];
    char str[MAX_WIDTH];
    char tmp[MAX_WIDTH];
    int tabl_width;
    int i, j;
    char *tok;

    gets(fmt);

    while(fmt[0]!='*')
    {
        row=0;
        col=strlen(fmt);

        for(i=0; i<col; i++)
        {
            width[i]=0;
        }

        while(1)
        {
            gets(str);
            if((str[0]=='*') || (str[0] == '<') || (str[0] == '=') || (str[0] == '>')) break;
            strcpy(tabl[row], str);
            row++;

            tok = strtok(str, "&");
            for(i=0; i<col; i++)
            {
                len = strlen(tok);
                if(width[i] < len) width[i] = len;
                tok = strtok(NULL, "&");
            }
        }
        tabl_width = 0;
        for(i=0; i<col; i++) tabl_width += width[i];

        tabl_width += ((3*col) - 1); //excluding leftmost and rightmost border, but including inner borders
        //top line
        putchar('@');
        for(i=0; i<tabl_width; i++) putchar('-');
        printf("@\n");

        strcpy(tmp, tabl[0]);
        tok = strtok(tmp, "&");
        for(j=0; j<col; j++)
        {
            putchar('|');
            putchar(' ');
            show(tok, width[j], fmt[j]);
            putchar(' ');
            tok = strtok(NULL, "&");
        }
        printf("|\n");

        putchar('|');
        putchar('-');
        for(j=0; j<=width[0]; j++)
        {
            putchar('-');
        }
        for(i=1; i<col; i++)
        {
            putchar('+');
            putchar('-');
            for(j=0; j<=width[i]; j++)
            {
                putchar('-');
            }
        }
        printf("|\n");

        for(i=1; i<row; i++)
        {
            strcpy(tmp, tabl[i]);
            tok = strtok(tmp, "&");
            for(j=0; j<col; j++)
            {
                putchar('|');
                putchar(' ');
                show(tok, width[j], fmt[j]);
                putchar(' ');
                tok = strtok(NULL, "&");
            }
            printf("|\n");
        }

        //bottom line
        putchar('@');
        for(i=0; i<tabl_width; i++) putchar('-');
        printf("@\n");

        strcpy(fmt, str);
    }

    return 0;
}

int show(char *str, int width, char align)
{
    int len, before, after;
    switch(align)
    {
    case '<':
        printf("%*s", -width, str);
        break;
    case '>':
        printf("%*s", width, str);
        break;
    case '=':
        len = strlen(str);
        before = (width-len)/2;
        after = width - (before+len);
        while(before>0)
        {
            putchar(' ');
            before--;
        }

        printf("%s", str);

        while(after>0)
        {
            putchar(' ');
            after--;
        }
        break;
    }
    return 0;
}
