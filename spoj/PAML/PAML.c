#include<stdio.h>
#include<string.h>
#define MAXVAL 128

int main()
{
    int t, k, n, i, j, min_stack, max_height;
    int a[128];
    char c[MAXVAL];
    char used[128];
    int num_used;

    scanf("%d", &t);

    for(k=1; k<=t; k++)
    {
        scanf("%d", &n);

        //scan & modified counting sort
        memset(c, 0, MAXVAL);
        for(i=0; i<n; i++)
        {
            scanf("%d", &(a[i]));
            c[a[i]]++;
        }
        j=0;
        for(i=0; i<=MAXVAL; i++)
        {
            while(c[i]>0)
            {
                a[j] = i;
                c[i]--;
                j++;
            }
        }
        //done scan and sort
        memset(used, 0, 128);

        num_used = 0;
        min_stack = 0;
        max_height = 0;

        int nxt_min;
        int curr_height;
        int top_strength;

        while(num_used<n)
        {
            nxt_min = 0;
            curr_height = 0;
            top_strength = 0;
            for(i=0; i<n; i++)
            {
                if(!used[i])
                {
                    if(a[i] >= nxt_min)
                    {
                        used[i] = 1;
                        num_used++;
                        curr_height++;
                        top_strength = a[i];
                        nxt_min = (curr_height > top_strength)?curr_height:top_strength;
                    }
                }
            }
            min_stack++;
            if(max_height<curr_height) max_height=curr_height;
        }
        printf("Case #%d: %d %d\n", k, min_stack, max_height);
    }

    return 0;
}
