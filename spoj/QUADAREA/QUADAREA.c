#include<stdio.h>
#include<math.h>

int main()
{
    double s, a, b, c, d;
    int t;
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%lf%lf%lf%lf", &a, &b, &c, &d);
        s = (a+b+c+d) / 2;
        printf("%.2lf\n", sqrt((s-a)*(s-b)*(s-c)*(s-d))); //Brahmagupta's formula
    }

    return 0;
}
