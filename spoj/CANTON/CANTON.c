#include<stdio.h>
#include<math.h>

int main()
{
    int t, n, s, two_s, k, p, q;
    scanf("%d", &t);
    while(t--)
    {
        scanf("%d", &s);
        two_s = 2 * s;
        n = sqrt(two_s);
        while( (n*(n+1)) < two_s) n++;

        k = ((n*(n+1))/2) - s;

        p = n - k;
        q = (n - p) + 1;

        if(n%2==0)
        {
            printf("TERM %d IS %d/%d\n", s, p, q);
        }
        else
        {
            printf("TERM %d IS %d/%d\n", s, q, p);
        }

    }
    return 0;
}
