/*
 * main.cpp
 *
 *  Created on: 01-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>

#define gc getchar

void calculateLPS(int* lps, const char* a, int n);

int main() {

	int n;
	while (scanf("%d", &n) != EOF) {

		// read input
		char str[n + 1];
		char ch;
		ch = gc();
		while (ch == '\r' || ch == '\n') {
			ch = gc(); // skip initial newline char
		}
		str[0] = ch;
		for (int i = 1; i < n; i++) {
			str[i] = gc();
		}
		str[n] = '\0';

		int lps[n];
		calculateLPS(lps, str, n);

		ch = gc();
		while (ch == '\r' || ch == '\n') {
			ch = gc(); // skip initial newline char
		}

		// kmp
		int currIndex = 0;
		int matchLength = 0;
		while (ch != '\r' && ch != '\n') {
			while (matchLength < n && ch != '\r' && ch != '\n') {
				if (ch == str[matchLength]) {
					ch = gc();
					currIndex++;
					matchLength++;
				} else {
					if (matchLength == 0) {
						// no match
						ch = gc();
						currIndex++;
					} else {
						// try previous prefix
						matchLength = lps[matchLength - 1];
					}
				}
			}
			if (matchLength == n) {
				printf("%d\n", currIndex - n);
			}
			matchLength = lps[n - 1];
		}
		printf("\n");
	}

	return 0;
}

void calculateLPS(int* lps, const char* a, int n) {
	lps[0] = 0; // always
	int prefixLen = 0;
	int i = 1;
	while (i < n) {
		if (a[i] == a[prefixLen]) {
			lps[i] = prefixLen + 1;
			i++;
			prefixLen++;
		} else {
			if (prefixLen == 0) {
				lps[i] = 0;
				i++;
			} else {
				prefixLen = lps[prefixLen - 1];
				// no i++ here (need to check if current prefix is also a suffix)
			}
		}
	}
}
