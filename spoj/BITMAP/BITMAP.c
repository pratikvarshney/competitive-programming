#include<stdio.h>

#define MAXSIZE 200
#define MAXVAL 1000000

int init(int res[MAXSIZE][MAXSIZE], char mat[MAXSIZE][MAXSIZE], int n, int m);
int check(int res[MAXSIZE][MAXSIZE], char mat[MAXSIZE][MAXSIZE], int n, int m, int p, int q, int dist);

int que_a[MAXSIZE*MAXSIZE], que_b[MAXSIZE*MAXSIZE], x;

int main()
{
    int n, m, t, i, j;
    char mat[MAXSIZE][MAXSIZE];
    int res[MAXSIZE][MAXSIZE];

    scanf("%d", &t);
    for(;t>0; t--)
    {
        scanf("%d%d", &n, &m);
        for(i=0; i<n; i++)
        {
            scanf("%s", &(mat[i]));
        }
        init(res, mat, n, m);
        x=0;

        for(i=0; i<n; i++)
        {
            for(j=0; j<m; j++)
            {
                if(mat[i][j] == '1')
                {
                    que_a[x] = i;
                    que_b[x] = j;
                    x++;
                }
            }
        }
        for(i=0; i<x; i++)
        {
            check(res, mat, n, m, que_a[i], que_b[i], res[que_a[i]][que_b[i]]+1);
        }

        for(i=0; i<n; i++)
        {
            printf("%d", res[i][0]);
            for(j=1; j<m; j++)
            {
                printf(" %d", res[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}

int init(int res[MAXSIZE][MAXSIZE], char mat[MAXSIZE][MAXSIZE], int n, int m)
{
    int i, j;
    for(i=0; i<n; i++)
    {
        for(j=0; j<m; j++)
        {
            res[i][j] = (mat[i][j]=='1')?0:MAXVAL;
        }
    }
    return 0;
}

int check(int res[MAXSIZE][MAXSIZE], char mat[MAXSIZE][MAXSIZE], int n, int m, int p, int q, int dist)
{
    //up
    if(p>0)
    {
        if(res[p-1][q]>dist)
        {
            res[p-1][q] = dist;
            que_a[x] = p-1;
            que_b[x] = q;
            x++;
        }
    }

    //down
    if((p+1)<n)
    {
        if(res[p+1][q]>dist)
        {
            res[p+1][q] = dist;
            que_a[x] = p+1;
            que_b[x] = q;
            x++;
        }
    }

    //left
    if(q>0)
    {
        if(res[p][q-1]>dist)
        {
            res[p][q-1] = dist;
            que_a[x] = p;
            que_b[x] = q-1;
            x++;
        }
    }

    //right
    if((q+1)<m)
    {
        if(res[p][q+1]>dist)
        {
            res[p][q+1] = dist;
            que_a[x] = p;
            que_b[x] = q+1;
            x++;
        }
    }

    return 0;
}
