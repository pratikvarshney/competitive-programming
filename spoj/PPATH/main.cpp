/*
 * main.cpp
 *
 *  Created on: 05-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
#include <queue>
#include <cstring>

#define gc getchar

using namespace std;

vector<int> prime;
vector<vector<int> > graphNode;
int idx[10000];

void init4DigitPrimes() {
	// init
	int limit = 10000 + 1;
	int sievebound = (limit - 1) / 2;
	bool sieve[sievebound + 1] = { 0 };
	int crosslimit = sqrt(limit - 1) / 2;
	int i, j;
	for (i = 1; i <= crosslimit; i++) {
		if (!sieve[i]) {
			for (j = 2 * i * (i + 1); j <= sievebound; j += (2 * i) + 1) {
				sieve[j] = 1;
			}
		}
	}

	for (i = 1; i <= sievebound; i++) {
		if (!sieve[i]) {
			int k = (2 * i) + 1;
			if (k >= 1000 && k <= 9999) {
				idx[k] = prime.size();
				prime.push_back(k);
			}
		}
	}
}

bool check(int a, int b) {
	int diff = 0;
	for (int i = 0; i < 4; i++) {
		if ((a % 10) == (b % 10)) {
			diff++;
		}
		a /= 10;
		b /= 10;
	}
	return (diff == 3);
}

void initGraph() {
	graphNode = vector<vector<int> >(prime.size(), vector<int>());
	for (int i = 0; i < prime.size(); i++) {
		for (int j = 0; j < prime.size(); j++) {
			if (i != j) {
				if (check(prime[i], prime[j])) {
					graphNode[i].push_back(j);
					graphNode[j].push_back(i);
				}
			}
		}
	}
}

void scanPosInt(int &val);

void scanPosInt(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

void solve() {
	int a, b;
	scanPosInt(a);
	scanPosInt(b);
	if (a == b) {
		printf("0\n");
		return;
	}
	vector<int> distance(prime.size(), -1);
	// bfs
	queue<int> q;
	q.push(idx[a]);
	distance[idx[a]] = 0;
	while (!q.empty()) {
		int i = q.front();
		q.pop();
		int nxt = distance[i] + 1;
		for (int j = 0; j < graphNode[i].size(); j++) {
			int k = graphNode[i][j];
			if (distance[k] == -1) {
				if (prime[k] == b) {
					printf("%d\n", nxt);
					return;
				}
				distance[k] = nxt;
				q.push(k);
			}
		}
	}
	printf("Impossible\n");
}

int main() {
	init4DigitPrimes();
	initGraph();
	int t;
	scanPosInt(t);
	while (t--) {
		solve();
	}
	return 0;
}
