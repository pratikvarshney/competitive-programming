#include<iostream>
#include<cstdio>
#include<vector>

int main()
{
    int N, u, v, i, j, k, depth, max_depth, far_node_a, far_node_b;
    scanf("%d", &N);
    std::vector<int> node[N+1];
    int d[100001];
    int p[100001];
    int pushed = 0;
    //create tree
    for(i=1; i<N; i++)
    {
        scanf("%d%d", &u, &v);
        node[u].push_back(v);
        node[v].push_back(u);
    }

    //reset
    for(i=1; i<=N; i++) d[i]=-1;

    p[0] = v;
    d[v] = 0;

    //dfs
    depth=0;

    pushed = 1;
    for(j=0; j<N; j++)
    {
        i = p[j];
        for(std::vector<int>::iterator it2=node[i].begin(); it2!=node[i].end(); it2++)
        {
            k = *it2;
            if(d[k] == -1)
            {
                p[pushed] = k;
                pushed++;
                d[k] = d[i] + 1;
                if(depth<d[k])
                {
                    depth = d[k];
                    far_node_a = k;
                }
            }
        }
    }

    //reset
    for(i=1; i<=N; i++) d[i]=-1;

    p[0] = far_node_a;
    d[far_node_a] = 0;

    //dfs
    depth=0;

    pushed = 1;
    for(j=0; j<N; j++)
    {
        i = p[j];
        for(std::vector<int>::iterator it2=node[i].begin(); it2!=node[i].end(); it2++)
        {
            k = *it2;
            if(d[k] == -1)
            {
                p[pushed] = k;
                pushed++;
                d[k] = d[i] + 1;
                if(depth<d[k])
                {
                    depth = d[k];
                    far_node_b = k;
                }
            }
        }
    }

    printf("%d\n", depth);
    return 0;
}
