#include<stdio.h>

int main()
{
    int t;
    long long int res, n;
    char op;
    scanf("%d", &t);
    for(;t>0;t--)
    {
        scanf("%lld", &res);

        while(1)
        {
            while((op = getchar())==' ');
            if(op=='=') break;

            scanf("%lld", &n);

            switch(op)
            {
                case '+': res = res + n;
                break;

                case '-': res = res - n;
                break;

                case '*': res = res * n;
                break;

                case '/': res = res / n;
                break;
            }
        }
        printf("%lld\n", res);
    }

    return 0;
}
