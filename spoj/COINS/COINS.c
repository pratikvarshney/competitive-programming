#include<stdio.h>

#define MAX_SIZE 256000

long long int lookup[MAX_SIZE]={0};

long long int maxm(long long int n);

int main()
{
    long long int i, n, temp;
    for(i=1; i<MAX_SIZE; i++)
    {
        temp = lookup[i/2] + lookup[i/3] + lookup[i/4];
        lookup[i] = (i>temp)?i:temp;
    }
    while(scanf("%lld", &n)>0)
    {
        printf("%lld\n", maxm(n));
    }
    return 0;
}

long long int maxm(long long int n)
{
    long long int sum;
    if(n<MAX_SIZE) return lookup[n];

    sum = maxm(n/2) + maxm(n/3) + maxm(n/4);
    return (sum>n)?sum:n;
}
