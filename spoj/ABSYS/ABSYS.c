#include<stdio.h>
#include<string.h>
#include<ctype.h>

#define MAX_SIZE 20

int main()
{
    int t;
    char temp[MAX_SIZE], a[MAX_SIZE], b[MAX_SIZE], c[MAX_SIZE];
    int p, q, r;

    scanf("%d", &t);
    while(t>0)
    {
        scanf("%[^0-9machul]", temp);
        scanf("%[0-9machul]", a);
        scanf("%[^0-9machul]", temp);
        scanf("%[0-9machul]", b);
        scanf("%[^0-9machul]", temp);
        scanf("%[0-9machul]", c);

        if(strstr(a, "machula"))
        {
            q = atoi(b);
            r = atoi(c);
            p = r - q;
        }
        else if(strstr(b, "machula"))
        {
            p = atoi(a);
            r = atoi(c);
            q = r - p;
        }
        else
        {
            p = atoi(a);
            q = atoi(b);
            r = p + q;
        }

        printf("%d + %d = %d\n", p, q, r);

        t--;
    }
    return 0;
}
