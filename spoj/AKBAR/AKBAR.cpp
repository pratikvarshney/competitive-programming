#include<iostream>
#include<cstdio>
#include<deque>
#include<vector>
#include<queue>

using namespace std;

class node
{
public:
    int city;
    int s;

    node(int cval, int sval);
};

node::node(int cval, int sval)
{
    city=cval;
    s=sval;
}

int mark(vector< deque<int> > &graph, vector<int> &city, int k, int s);

int main()
{
    int t;

    scanf("%d", &t);

    while(t>0)
    {
        int n, r, m, k, s, a, b;
        int i;

        scanf("%d%d%d", &n, &r, &m);

        vector< deque<int> > graph(n+1);
        vector<int> city(n+1, 0);

        for(i=0; i<r; i++)
        {
            scanf("%d%d", &a, &b);
            graph[a].push_back(b);
            graph[b].push_back(a);
        }

        /*for(i=1; i<=n; i++)
        {
            cout << i << " -> ";
            for(deque<int>::iterator it=graph[i].begin(); it!=graph[i].end(); it++)
            {
                cout << ", " <<*it;
            }
            cout << endl;
        }*/

        for(i=1; i<=m; i++)
        {
            scanf("%d%d", &k, &s);
            mark(graph, city, k, s);
        }

        int flag=1;
        for(vector<int>::iterator it=city.begin()+1; it!=city.end(); it++)
        {
            if((*it)!=1)
            {
                flag=0;
                break;
            }
        }
        if(flag==1)
        {
            cout << "Yes" << endl;
        }
        else
        {
            cout << "No" << endl;
        }

        t--;
    }

    return 0;
}

int mark(vector< deque<int> > &graph, vector<int> &city, int k, int s)
{
    vector<bool> pushed(city.size(), false);

    queue<node> myq;
    myq.push(node(k, s));
    pushed[k]=true;

    int ct;
    int st;

    while(!myq.empty())
    {
        ct = myq.front().city;
        st = myq.front().s;
        myq.pop();
        city[ct]++;
        if(st>0)
        {
            for(deque<int>::iterator it=graph[ct].begin(); it!=graph[ct].end(); it++)
            {
                if(!pushed[*it])
                {
                    myq.push(node(*it, st-1));
                    pushed[*it]=true;
                }
            }
        }
    }

    return 0;
}
