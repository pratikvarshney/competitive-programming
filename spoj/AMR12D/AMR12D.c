#include<stdio.h>
#include<string.h>

#define MAXLEN 11

int main()
{
    int t, len, i, flag;
    char str[MAXLEN];
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%s", str);
        len = strlen(str);
        flag=0;
        for(i=len/2; i>=0; i--)
        {
            if(str[i]!=str[(len-1)-i])
            {
                flag=1;
                break;
            }
        }
        if(flag)
        {
            printf("NO\n");
        }
        else
        {
            printf("YES\n");
        }
    }
    return 0;
}
