#include<stdio.h>

#define MAXVAL 10000

#define max(a,b) (((a)>(b))?(a):(b))

int main()
{
    int t, c;
    int n, i;
    long long total[MAXVAL+3];
    long long maxm;
    scanf("%d", &t);
    for(c=1; c<=t; c++)
    {
        scanf("%d", &n);
        total[0]=0;
        total[1]=0;
        total[2]=0;
        n = n + 3;
        for(i=3; i<n; i++)
        {
            scanf("%lld", &(total[i]));
            total[i] += max(total[i-2], total[i-3]);
        }
        maxm = max(total[n-1], total[n-2]);
        printf("Case %d: %lld\n", c, maxm);
    }
    return 0;
}
