/*
 * main.cpp
 *
 *  Created on: 10-Jan-2017
 *      Author: Pratik Varshney
 */

#include <iostream>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

struct node {
	long start, end, index;
	map<char, node*> next;
};

node* addToSuffixtree(node* stree, const char* str, long index, long start,
		long n) {
	node* curr = stree;
	long i = start;
	while (i <= n) {
		map<char, node*>::iterator it = curr->next.find(str[i]);
		if (it == curr->next.end()) {
			node* tmp = new node;
			tmp->start = i;
			tmp->end = n;
			tmp->index = index;
			curr->next[str[i]] = tmp;
			return stree;
		} else {
			node* existing = it->second;
			long j = existing->start;
			long k = existing->start;
			while (i <= n && j <= existing->end && str[i] == str[j]) {
				i++;
				j++;
			}

			if (i <= n) {
				if (j > existing->end) {
					// case 1: continue to next
					curr = it->second;
				} else {
					// case 2: split in middle
					node* parentNode = new node;
					node* newChild = new node;
					newChild->start = i;
					newChild->end = n;
					newChild->index = index;
					parentNode->next[str[i]] = newChild; // i = newChild->start
					parentNode->start = k;
					parentNode->end = j - 1;
					parentNode->index = -1;
					existing->start = j;
					parentNode->next[str[j]] = existing; // j = existing->start
					curr->next[str[k]] = parentNode; // k = parentNode->start
					return stree;
				}
			} // else do nothing
		}
	}
	return stree;
}

node* getSuffixTree(const char* str, long n) {
	node* stree = new node;
	stree->index = -1;
	for (long start = n - 1; start >= 0; start--) {
		stree = addToSuffixtree(stree, str, start, start, n);
	}
	return stree;
}

void setSuffixArray(vector<long>& suffixArray, node* suffixTree) {
	if (suffixTree->index >= 0) {
		suffixArray.push_back(suffixTree->index);
	} else {
		for (map<char, node*>::iterator it = suffixTree->next.begin();
				it != suffixTree->next.end(); it++) {
			setSuffixArray(suffixArray, it->second);
		}
	}
}

long calcLcpAndReturnHeightSum(const char* str, long n,
		vector<long>& suffixArray) {
	long sum = 0;
	for (long i = 1; i < n; i++) {
		long j = suffixArray[i - 1];
		long k = suffixArray[i];
		long l = 0;
		while (str[j] == str[k] && j < n && k < n) {
			j++;
			k++;
			l++;
		}
		sum += l;
	}
	return sum;
}

long solve() {
	char str[50002];
	scanf("%s", str);
	long n = 0;
	while (str[n] != '\0') {
		n++;
	}

	node* suffixTree = getSuffixTree(str, n);
	// suffix array
	vector<long> suffixArray;
	setSuffixArray(suffixArray, suffixTree);
	long dup = calcLcpAndReturnHeightSum(str, n, suffixArray);
	return ((n * (n + 1)) / 2) - dup;
}

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		printf("%ld\n", solve());
	}
	return 0;
}
