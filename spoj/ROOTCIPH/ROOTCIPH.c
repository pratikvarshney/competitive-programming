#include<stdio.h>

/*
#include<math.h>

#define PI 3.14159265359

long long cdist(double a, double b, double c);
*/

int main()
{
    int t;
    long long a, b, c;
    long long res;

    scanf("%d", &t);

    for(; t>0; t--)
    {
        scanf("%lld%lld%lld", &a, &b, &c);
        res = (a*a) - (2*b);
        printf("%lld\n", res);
    }

    return 0;
}

/*

long long cdist(double a, double b, double c)
{
    long long x1, x2, x3;
    double q, r, d, r13;

    q = ((3*b)-(a*a))/9;
    r = ((9*a*b)-(27*c)-(2*a*a*a))/54;
    d = (q*q*q) + (r*r);

    r13 = acos(r/sqrt(-(q*q*q)))/3; //radian

    x1 = round((2*sqrt(-q)*cos(r13)) - (a/3));
    x2 = round((2*sqrt(-q)*cos(r13 + ((2*PI)/3))) - (a/3));
    x3 = round((2*sqrt(-q)*cos(r13 + ((4*PI)/3))) - (a/3));

    return ((x1*x1) + (x2*x2) + (x3*x3));
}
*/

/*

Formula :
http://en.wikibooks.org/wiki/Trigonometry/The_solution_of_cubic_equations

*/
