#include<stdio.h>

#define MAXVAL 500001

int hash_check_n_update(unsigned int arr[MAXVAL], unsigned int n);

int main()
{
    unsigned int arr[MAXVAL]={0};
    unsigned int i, n, k;

    //init
    //for(i=0; i<MAXVAL; i++) arr[i]=0;

    scanf("%u", &n);
    for(i=0; i<n; i++)
    {
        scanf("%u", &k);
        hash_check_n_update(arr, k);
    }
    for(i=0; i<MAXVAL; i++)
    {
        if(arr[i] != 0)
        {
            printf("%u\n", arr[i]);
            break;
        }
    }

    return 0;
}

int hash_check_n_update(unsigned int arr[MAXVAL], unsigned int n)
{
    int i;
    i = n%MAXVAL;
    while(arr[i] != 0)
    {
        if(arr[i] == n)
        {
            arr[i] = 0;
            return i;
        }
        i++;
        if(i==MAXVAL) i=0;
    }
    arr[i] = n;
    return -1;
}
