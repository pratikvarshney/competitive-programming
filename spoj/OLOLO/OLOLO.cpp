#include<iostream>
#include<cstdio>
#include<vector>

int main()
{
    int i, n, k, j, found;
    scanf("%d", &n);
    std::vector<int> arr(n, -1);

    for(i=0; i<n; i++)
    {
        scanf("%d", &k);

        //check n update
        j = k%n;
        found = 0;
        while(arr[j] != -1)
        {
            if(arr[j] == k)
            {
                //remove
                arr[j] = -1;
                found = 1;
                break;
            }
            j++;
            if(j==n) j=0;
        }
        //insert
        if(found == 0)
        {
            arr[j] = k;
        }

    }


    for(std::vector<int>::iterator it=arr.begin(); it!=arr.end(); it++)
    {
        if(*it != -1)
        {
            std::cout << *it ;
            break;
        }
    }

    return 0;
}
