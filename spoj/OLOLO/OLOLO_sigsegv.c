#include<stdio.h>

#define MAXVAL 125000001

int main()
{
    char* arr;
    unsigned int i, n, k, j;
    int maxm;

    //init
    //for(i=0; i<MAXVAL; i++) arr[i]=0;

    scanf("%u", &n);
    arr = (char*)calloc(n/8, 8);
    for(i=0; i<n; i++)
    {
        scanf("%u", &k);
        arr[k/8] ^= (1 << (7-(k%8))); //XOR
    }
    for(i=0; i<MAXVAL; i++)
    {
        if(arr[i] != 0)
        {
            j = 0;
            while(arr[i] != 0)
            {
                arr[i] <<= 1;
                j++;
            }
            j--;
            printf("%u\n", (i*8)+j);
            break;
        }
    }

    return 0;
}
