/*
 * main.cpp
 *
 *  Created on: 28-Feb-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <algorithm>
#include <cmath>

using namespace std;

#define gc getchar

int getint() {
	register int ch = gc();
	register int val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	return val;
}

#define A 200000
#define N 30000
#define V 1000001
// BLOCK_SIZE ~ sqrt(N)
#define BLOCK_SIZE 200

struct node {
	int L, R, i;
} query[A];

int a[N], ans[A], cnt[V], answer = 0;

bool cmp(node x, node y) {
	// diff block
	if (x.L / BLOCK_SIZE != y.L / BLOCK_SIZE) {
		return x.L / BLOCK_SIZE < y.L / BLOCK_SIZE;
	}
	// same block
	return x.R < y.R;
}

void add(int i) {
	cnt[a[i]]++;
	if (cnt[a[i]] == 1) {
		answer++;
	}
}

void remove(int i) {
	cnt[a[i]]--;
	if (cnt[a[i]] == 0) {
		answer--;
	}
}

void calc_ans(int q) {
	int l = query[0].L;
	int r = l;
	for (int i = 0; i < q; i++) {
		while (l < query[i].L) {
			remove(l++);
		}
		while (l > query[i].L) {
			add(--l);
		}
		while (r <= query[i].R) {
			add(r++);
		}
		while (r > query[i].R + 1) {
			remove(--r);
		}
		ans[query[i].i] = answer;
	}
}

int main() {
	int n, q;
	n = getint();
	for (int i = 0; i < n; i++) {
		a[i] = getint();
		cnt[a[i]] = 0;
	}
	q = getint();
	for (int i = 0; i < q; i++) {
		query[i].i = i;
		query[i].L = getint() - 1; // 0 based indexing
		query[i].R = getint() - 1;
	}

	sort(query, query + q, cmp);

	calc_ans(q);

	for (int i = 0; i < q; i++) {
		printf("%d\n", ans[i]);
	}

	return 0;
}
