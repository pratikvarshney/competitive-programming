#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector

using namespace std;

bool compr (int i,int j) { return (i>j); }

int main()
{
    int req, frnd, val, sum;
    int i, j, n, t;
    scanf("%d", &t);
    for(i=1; i<=t; i++)
    {
        sum = 0;
        n=0;

        scanf("%d%d", &req, &frnd);
        vector<int> a(frnd);

        for(j=0; j<frnd; j++)
        {
            cin >> a[j];
        }

        sort (a.begin(), a.end(), compr);

        for (std::vector<int>::iterator it=a.begin(); it!=a.end(); ++it)
        {
            n++;
            sum += *it;
            if(sum>=req) break;
        }

        if(sum<req)
        {
            printf("Scenario #%d:\nimpossible\n", i);
        }
        else
        {
            printf("Scenario #%d:\n%d\n", i, n);
        }
    }
    return 0;
}
