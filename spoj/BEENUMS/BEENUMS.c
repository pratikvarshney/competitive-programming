#include<stdio.h>
#include<math.h>

int main()
{
    long long n, i;

    while(1)
    {
        scanf("%lld", &n);
        if(n==-1) break;

        i = (-3 + floor(sqrt(9+(12*(n-1)))) )/6;
        if( ((3*i*i) + (3*i) + 1) == n ) printf("Y\n");
        else printf("N\n");
    }
    return 0;
}
