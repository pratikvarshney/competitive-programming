#include<stdio.h>

int main()
{
    int t, n, k, i, j;
    scanf("%d", &t);

    while(t>0)
    {
        scanf("%d", &n);
        k=0;
        i=5;
        while(i<=n)
        {
            k += (n/i);
            i *= 5;
        }
        printf("%d\n", k);

        t--;
    }
    return 0;
}

