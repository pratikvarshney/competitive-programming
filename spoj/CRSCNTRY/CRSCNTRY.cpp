#include<cstdio>
#include<iostream>
#include<vector>

int LCSLength(std::vector<int> a, std::vector<int> b);

int main()
{
    std::vector<int> agnes;
    std::vector<int> tom;

    int d, v, mx, lcs_len;

    scanf("%d", &d);

    for(; d>0; d--)
    {
        mx=0;

        //agnes
        agnes.clear();
        while(1)
        {
            scanf("%d", &v);
            if(v==0) break;

            agnes.push_back(v);
        }

        //tom
        while(1)
        {
            tom.clear();
            while(1)
            {
                scanf("%d", &v);
                if(v==0) break;

                tom.push_back(v);
            }
            if(tom.size()==0) break;

            lcs_len = LCSLength(agnes, tom);
            if(mx<lcs_len) mx=lcs_len;
        }

        printf("%d\n", mx);

    }

    return 0;
}

int LCSLength(std::vector<int> a, std::vector<int> b)
{
    int m, n;
    int i, j;
    m = a.size();
    n = b.size();

    std::vector< std::vector< int > > c ( m+1, std::vector<int> ( n+1, 0 ) );

    for(i=0; i<m; i++)
    {
        for(j=0; j<n; j++)
        {
            if(a[i]==b[j])
            {
                c[i+1][j+1] = c[i][j] + 1;
            }
            else
            {
                if(c[i+1][j] > c[i][j+1])
                {
                    c[i+1][j+1] = c[i+1][j];
                }
                else
                {
                    c[i+1][j+1] = c[i][j+1];
                }
            }
        }
    }

    return c[m][n];
}
