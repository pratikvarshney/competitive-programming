/*
 * main.cpp
 *
 *  Created on: 02-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <climits>

using namespace std;

#define MAX 100

#define gc getchar

int getPosInt();

int getPosInt() {
	int val = 0;
	char ch = gc();
	while (ch < '0' || ch > '9')
		ch = gc();
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	return val;
}

int solve(int* a, int n);

int main() {
	int n;
	while (scanf("%d", &n) != EOF) {
		int a[n];
		for (int i = 0; i < n; i++) {
			a[i] = getPosInt();
		}
		printf("%d\n", solve(a, n));
	}
	return 0;
}

int solve(int* a, int n) {
	if (n < 2) {
		return 0;
	}
	if (n == 2) {
		return a[0] * a[1];
	}
	// case: n > 2
	int smoke[MAX][MAX];
	int color[MAX][MAX];

	// init (for length l = 1)
	for (int i = 0; i < n; i++) {
		smoke[i][i] = 0;
		color[i][i] = a[i];
	}

	// process
	for (int l = 2; l <= n; l++) {
		for (int i = 0; (i + l - 1) < n; i++) {
			int j = i + l - 1;
			int min = INT_MAX;
			for (int k = i; k < j; k++) {
				// check i to k, k+1 to j
				int smokeVal = smoke[i][k] + smoke[k + 1][j]
						+ (color[i][k] * color[k + 1][j]);
				if (min > smokeVal) {
					min = smokeVal;
					color[i][j] = (color[i][k] + color[k + 1][j]) % 100;
					smoke[i][j] = min;
				}
			}
		}
	}
	return smoke[0][n - 1];
}
