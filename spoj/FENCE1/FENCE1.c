#include<stdio.h>

#define TWO_PI 6.283185

int main()
{
    float a;
    int l;
    while(1)
    {
        scanf("%d", &l);
        if(l==0) break;

        printf("%.2f\n", (((float)l)*l)/TWO_PI);
    }

    return 0;
}
