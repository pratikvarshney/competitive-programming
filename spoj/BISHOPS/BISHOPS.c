#include<stdio.h>
#include<string.h>

int main()
{
    char input[128];
    char res[128];
    int i, j, len, carry, temp;

    while(!feof(stdin))
    {
        scanf("%s\n", input);

        if(strcmp(input,"0")==0) printf("0\n");
        else if(strcmp(input,"1")==0) printf("1\n");
        else
        {
            len = strlen(input);
            res[0] = '0';
            res[len+1]='\0';
            carry = 0;
            //calc 2*input
            for(i=len-1; i>=0; i--)
            {
                temp = ((input[i]-'0')*2) + carry;
                carry = temp / 10;
                res[i+1] = (temp%10)+'0';
            }
            res[0] = carry + '0';

            //calc res - 2
            carry = 2;
            for(i=len; i>=0; i--)
            {
                if(res[i] >= (carry+'0'))
                {
                    res[i] -= carry;
                    break;
                }
                else
                {
                    res[i] += (10 - carry);
                    carry = 1;
                }
            }

            if(res[0]=='0') printf("%s\n", res+1);
            else printf("%s\n", res);
        }
    }

    return 0;
}
