#include<stdio.h>
#include<string.h>

int main()
{
    int c, i, j, len, a, b;
    char input[201];

    while(1)
    {
        scanf("%d", &c);
        if(c==0) break;

        scanf("%s", input);
        len = strlen(input);

        for(i=0; i<c; i++)
        {
            j=i;
            a=((2*(c-i))-1);
            b=(2*c)-a;
            while(j<len)
            {
                putchar(input[j]);
                j += a;

                if(j>=len) break;

                putchar(input[j]);
                j += b;
            }
        }
        printf("\n");
    }

    return 0;
}
