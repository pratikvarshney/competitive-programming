/*
 * main.cpp
 *
 *  Created on: 08-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

int solve() {
	int h, a;
	scanint(h);
	scanint(a);
	int steps = 1; // air
	h += 3;
	a += 2;
	while (true) {
		if (h > 5 && a > 10) {
			// water & air
			h -= 2; // -5 +3
			a -= 8; // -10 + 2
			steps += 2;
		} else if (h > 20 && a > 0) {
			// or h > 20 && a <= 10
			// fire and air
			h -= 17; // -20 +3
			a += 7; // +5 +2
			steps += 2;
		} else {
			return steps;
		}
	}
	return 0;
}

int main() {
	int t;
	scanint(t);
	while (t--) {
		printf("%d\n", solve());
	}
	return 0;
}
