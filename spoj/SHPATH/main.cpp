/*
 * main.cpp
 *
 *  Created on: 26-Feb-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <vector>
#include <unordered_map>

using namespace std;

#define gc getchar

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

struct node {
	int val;
	int priority;
	node() {
	}
	node(int v, int r) {
		val = v;
		priority = r;
	}
	void assign(const node& copy) {
		val = copy.val;
		priority = copy.priority;
	}
	void set(int v, int r) {
		val = v;
		priority = r;
	}
};

node q[10002];
int pos[10002];

class min_priority_queue {
private:
	int size;

	void swap(int i, int j) {
		node tmp;
		tmp.assign(q[i]);
		q[i].assign(q[j]);
		q[j].assign(tmp);
		pos[q[i].val] = i;
		pos[q[j].val] = j;
	}

	void heapify_up(int i) {
		while (q[i].priority < q[i / 2].priority) {
			swap(i, i / 2);
			i /= 2;
		}
	}

	void heapify_down(int i) {
		while (2 * i < size) {
			int min_child = 2 * i; // 1st child
			if (min_child + 1 < size
					&& q[min_child + 1].priority < q[min_child].priority) {
				min_child++; // 2nd child
			}
			if (q[i].priority <= q[min_child].priority) {
				return;
			}
			swap(i, min_child);
			i = min_child;
		}
	}

public:
	min_priority_queue(int n) {
		q[0].set(0, -1); // adding 1 extra elem to make 1 base indexing
		size = 1;
	}
	bool empty() {
		return size <= 1; // for 1 based indexing
	}
	void add_with_priority(int v, int p) {
		// append to last
		int curr = size;
		pos[v] = curr;
		q[size++].set(v, p);

		heapify_up(curr);
	}
	void decrease_priority(int v, int p) {
		int pos_v = pos[v];
		if (p < q[pos_v].priority) {
			q[pos_v].priority = p;
			heapify_up(pos_v);
		}
	}
	int extract_min() {
		int min = q[1].val;
		size--;
		swap(1, size);
		pos[min] = 0;
		heapify_down(1);
		return min;
	}
};

int dijsktra(vector<vector<pair<int, int> > >& graph, int src, int dest,
		int n) {
	// queue: add_with_priority, decrease_priority, extract_min
	vector<int> dist(n + 1, 1000000);
	vector<bool> added(n + 1, false);
	min_priority_queue q(n);
	dist[src] = 0;
	q.add_with_priority(src, 0);
	added[src] = true;
	while (!q.empty()) {
		int u = q.extract_min();
		if (u == dest) {
			return dist[u];
		}
		if (graph[u].size() > 0) {
			for (int j = 0; j < graph[u].size(); j++) {
				int v = graph[u][j].first;
				int alt = dist[u] + graph[u][j].second;
				if (alt < dist[v]) {
					dist[v] = alt;
					if (added[v]) {
						q.decrease_priority(v, alt);
					} else {
						q.add_with_priority(v, alt);
						added[v] = true;
					}
				}
			}
		}
	}
	return -1;
}

void solve() {
	int n, p, nr, cost, r;
	scanint(n);
	vector<vector<pair<int, int> > > graph(n + 1);
	unordered_map<string, int> map;
	char city[20];
	for (int i = 1; i <= n; i++) {
		scanf("%s", city);
		map[string(city)] = i;
		scanint(p);
		for (int j = 0; j < p; j++) {
			scanint(nr);
			scanint(cost);
			graph[i].emplace_back(nr, cost);
		}
	}
	scanint(r);
	char a[20], b[20];
	for (int i = 0; i < r; i++) {
		scanf("%s%s", a, b);
		printf("%d\n", dijsktra(graph, map[string(a)], map[string(b)], n));
	}
}

int main() {
	int s;
	scanint(s);
	while (s--) {
		solve();
	}
	return 0;
}
