/*
 * main.cpp
 *
 *  Created on: 06-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <unordered_map>

using namespace std;

#define gc getchar

#define MOD 1000000007

void scanint(int &val);

void scanint(int &val) {
	register int ch = gc();
	val = 0;
	while (ch < '0' || ch > '9') {
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
}

// f(2k) = f(k)[2f(k+1) - f(k)]
// f(2k+1) = f(k+1)^2 + f(k)^2

// memoization
unordered_map<int, int> map;

long long f(int n) {
	if (n == 0)
		return 0;
	if (n == 1 || n == 2)
		return 1;
	unordered_map<int, int>::iterator it = map.find(n);
	if (it != map.end()) {
		return it->second;
	}
	int k = n / 2;
	long long fk = f(k);
	long long fk1 = f(k + 1);
	int val = 0;
	if (n & 1) {
		// case 2k+1 (odd number)
		val = (((fk1 * fk1) % MOD) + ((fk * fk) % MOD)) % MOD;
	} else {
		val = (fk * ((2 * fk1) - fk)) % MOD;
	}
	map[n] = val;
	return val;
}

long long sum(int n) {
	return f(n + 2) - 1;
}

void solve() {
	int n, m;
	scanint(n);
	scanint(m);
	long long ans = (sum(m) - sum(n - 1)) % MOD;
	if (ans < 0) {
		ans += MOD;
	}
	printf("%lld\n", ans);
	/*printf("%lld\n",
	 (((f((m - n) + 1) * f(n)) % MOD)
	 + (((f((m - n) + 2) - 1) * f(n + 1)) % MOD)) % MOD);*/
}

int main() {

	int t;
	scanint(t);
	while (t--) {
		solve();
	}
	return 0;
}
