#include<stdio.h>

#define MAXSIZE 101

int val[MAXSIZE];
int init_num_sq();

int main()
{
    int n;
    init_num_sq();

    while(1)
    {
        scanf("%d", &n);
        if(n==0) break;

        printf("%d\n", val[n]);
    }

    return 0;
}

int init_num_sq()
{
    int i;
    val[0]=0;
    for(i=1; i<MAXSIZE; i++)
    {
        val[i] = val[i-1] + (i*i);
    }
    return 0;
}
