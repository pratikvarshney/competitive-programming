#include<iostream>
#include<cstdio>
#include <algorithm>    // std::sort
#include <vector>       // std::vector

int main()
{
    int t, n, k, i, minm;
    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%d%d", &n, &k);
        std::vector<int> arr(n);
        for(i=0; i<n; i++)
        {
            std::cin >> arr[i];
        }
        std::sort(arr.begin(), arr.end());
        k--; //to avoid using k-1 ... later
        std::vector<int> res(n-k);

        std::vector<int>::iterator it1=arr.begin();
        std::vector<int>::iterator it2=arr.begin()+k;
        for(i=0; it2 != arr.end(); it1++, it2++, i++)
        {
            res[i] = (*it2) - (*it1);
        }

        minm = res[0];
        for(std::vector<int>::iterator it=res.begin(); it!=res.end(); it++)
        {
            if(minm > (*it)) minm = (*it);
        }
        printf("%d\n", minm);
    }
    return 0;
}
