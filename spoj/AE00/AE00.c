#include<stdio.h>

int main()
{
    int n, i, sum, temp;
    scanf("%d", &n);
    sum = 0;
    for(i=1; i<=n; i++)
    {
        temp = (n/i)-(i-1);
        if(temp>0)
        {
            sum += temp;
        }
        else break;
    }
    printf("%d\n", sum);
    return 0;
}
