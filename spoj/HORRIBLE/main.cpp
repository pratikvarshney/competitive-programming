/*
 * main.cpp
 *
 *  Created on: 04-Jan-2017
 *      Author: Pratik Varshney
 */

#include <cstdio>
#include <cstring>
#include <cmath>

#define MAX (1<<18)

long long st[MAX];
long long lazy[MAX];

void lazyUpdate(int si, int ss, int se, int p, int q, long long v);
long long query(int si, int ss, int se, int p, int q);
inline int getMid(int l, int r) {
	return (l + r) / 2;
}

void solve() {
	int n, c, d, p, q;
	long long v;
	scanf("%d%d", &n, &c);
	int len = (1 << ((int) ceil(log2(n) + 1))) + 1;

	// construct st (no need to calc sum as all values are 0)
	memset(st, 0, sizeof(long long) * len);
	memset(lazy, 0, sizeof(long long) * len);

	for (int i = 0; i < c; i++) {
		scanf("%d%d%d", &d, &p, &q);
		p--; // index starts from 0 instead of 1
		q--;
		if (d == 0) {
			scanf("%lld", &v);
			lazyUpdate(1, 0, n - 1, p, q, v);
		} else {
			printf("%lld\n", query(1, 0, n - 1, p, q));
		}
	}
}

long long query(int si, int ss, int se, int p, int q) {
	// out of range
	if (se < p || q < ss) {
		return 0;
	}

	// completely in range
	if (p <= ss && se <= q) {
		return st[si];
	} else {
		int mid = getMid(ss, se);
		if (lazy[si] > 0) {

			// force propagate lazy values down
			lazyUpdate(2 * si, ss, mid, ss, mid, lazy[si]);
			lazyUpdate((2 * si) + 1, mid + 1, se, mid + 1, se, lazy[si]);

			// reset current lazy value
			lazy[si] = 0;
		}
		return query(2 * si, ss, mid, p, q)
				+ query((2 * si) + 1, mid + 1, se, p, q);
	}
	return 0;
}

void lazyUpdate(int si, int ss, int se, int p, int q, long long v) {
	// out of range
	if (se < p || q < ss) {
		return;
	}

	// completely in range
	if (p <= ss && se <= q) {

		// leaf node
		if (ss == se) {
			st[si] += v;
		} else {
			lazy[si] += v; // pending update
			st[si] += (v * ((se - ss) + 1));
		}
	} else {
		int mid = getMid(ss, se);

		if (lazy[si] > 0) {

			// force propagate lazy values down
			lazyUpdate(2 * si, ss, mid, ss, mid, lazy[si]);
			lazyUpdate((2 * si) + 1, mid + 1, se, mid + 1, se, lazy[si]);

			// reset current lazy value
			lazy[si] = 0;
		}

		// propagate current values down
		lazyUpdate(2 * si, ss, mid, p, q, v);
		lazyUpdate((2 * si) + 1, mid + 1, se, p, q, v);

		// update final value
		st[si] = st[2 * si] + st[(2 * si) + 1];
	}
}

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		solve();
	}
}
