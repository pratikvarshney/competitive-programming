#include<stdio.h>

#define MAXM 200

int main()
{
    int t, n, i, j, k, found;
    int d[MAXM][MAXM];

    scanf("%d", &t);

    for(; t>0; t--)
    {
        scanf("%d", &n);
        for(i=0; i<n; i++)
        {
            for(j=0; j<n; j++)
            {
                scanf("%d", &(d[i][j]));
            }
        }

        for(i=0; i<n; i++)
        {
            for(j=i+1; j<n; j++)
            {
                found=0;
                for(k=0; k<n; k++)
                {
                    if(k==i) continue;
                    if(k==j) continue;
                    if((d[i][k]+d[k][j])==d[i][j])
                    {
                        found=1;
                        break;
                    }
                }
                if(!found)
                {
                    printf("%d %d\n", i+1, j+1);
                }
            }
        }

        printf("\n");
    }
    return 0;
}
