#include <iostream>
#include <cstdio>

using namespace std;

#define min(a,b) (((a)<(b))?(a):(b))

int opt(const char* a, const char* b, int m, int n) {
	int p[n + 1];
	int q[n + 1];
	int* dp = p;
	int* dp2 = q;
	for (int i = n; i >= 0; i--) {
		dp[i] = n - i;
	}
	for (int i = m - 1; i >= 0; i--) {
		// swap arrays
		int* tmp = dp;
		dp = dp2;
		dp2 = tmp;
		// next iter
		dp[n] = m - i;
		for (int j = n - 1; j >= 0; j--) {
			if (a[i] == b[j]) {
				dp[j] = dp2[j + 1];
			} else {
				dp[j] = 1 + min(dp2[j + 1], min(dp2[j], dp[j + 1]));
			}
		}
	}
	return dp[0];
}

void solve() {
	string a, b;
	cin >> a >> b;
	cout << opt(a.c_str(), b.c_str(), a.size(), b.size()) << endl;
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		solve();
	}
	return 0;
}
