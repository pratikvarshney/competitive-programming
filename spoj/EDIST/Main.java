import java.util.*;
import java.lang.*;

class Main {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while (t > 0) {
			solve(sc);
			t--;
		}
	}

	static void solve(Scanner sc) {
		String a = sc.next();
		String b = sc.next();
		System.out.println(opt(a, b));
	}

	static int opt(String a, String b) {
		int[][] dp = new int[a.length() + 1][b.length() + 1];
		for (int i = b.length(); i >= 0; i--) {
			dp[a.length()][i] = b.length() - i;
		}
		for (int i = a.length(); i >= 0; i--) {
			dp[i][b.length()] = a.length() - i;
		}
		for (int i = a.length() - 1; i >= 0; i--) {
			for (int j = b.length() - 1; j >= 0; j--) {
				if (a.charAt(i) == b.charAt(j)) {
					dp[i][j] = dp[i + 1][j + 1];
				} else {
					dp[i][j] = 1 + Math.min(dp[i + 1][j + 1], Math.min(dp[i + 1][j], dp[i][j + 1]));
				}
			}
		}
		return dp[0][0];
	}
}