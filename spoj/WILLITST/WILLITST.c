#include<stdio.h>

int main()
{
    unsigned long long n;
    scanf("%llu", &n);
    if(n<=1)
    {
        printf("TAK\n");
    }
    else if((n & (-n)) == n) /* check 2^k == n */
    {
        printf("TAK\n");
    }
    else
    {
        printf("NIE\n");
    }
    return 0;
}
