#include <cstdio>
#include <cmath>
#include <climits>
#include <cstdlib>

#define gc getchar

int getInt();

int getInt() {
	int val = 0;
	char ch = gc();
	bool neg = false;
	while (ch < '0' || ch > '9') {
		if (ch == '-') {
			neg = true;
			ch = gc();
			break;
		}
		ch = gc();
	}
	while (ch >= '0' && ch <= '9') {
		val = (val << 1) + (val << 3) + (ch - '0');
		ch = gc();
	}
	return (neg) ? (-val) : val;
}

inline int max(int p, int q);
inline int getMid(int l, int r);
void constructST(int l, int r, int si);

struct node {
	int sum = 0, bestLeftSum = 0, bestRightSum = 0, bestSum = 0;
	void merge(node& l, node& r) {
		sum = l.sum + r.sum;
		bestLeftSum = max(l.bestLeftSum, l.sum + r.bestLeftSum);
		bestRightSum = max(r.bestRightSum, l.bestRightSum + r.sum);
		bestSum = max(max(l.bestSum, r.bestSum),
				l.bestRightSum + r.bestLeftSum);
	}
	void setAll(int v) {
		sum = bestLeftSum = bestRightSum = bestSum = v;
	}
};

void query(node& res, int ss, int se, int si, int x, int y);
void update(int l, int r, int si, int i, int v);

node st[1 << 17]; // 2^17
int a[1 << 16];

int main() {
	int n;
	n = getInt();
	for (int i = 0; i < n; i++) {
		a[i] = getInt();
	}

	// segment tree
	constructST(0, n - 1, 1);
	node res;
	int m;
	m = getInt();
	for (int i = 0; i < m; i++) {
		int c = getInt();

		if (c == 1) {
			int x = getInt() - 1;
			int y = getInt() - 1;
			query(res, 0, n - 1, 1, x, y);
			printf("%d\n", res.bestSum);
		} else {
			// update
			int x = getInt() - 1;
			int v = getInt();
			update(0, n - 1, 1, x, v);
		}

	}

	return 0;
}

void update(int l, int r, int si, int i, int v) {
	if (l == i && r == i) {
		st[si].setAll(v);
	} else {
		int mid = getMid(l, r);
		if (i <= mid) {
			update(l, mid, 2 * si, i, v);
		} else {
			update(mid + 1, r, (2 * si) + 1, i, v);
		}
		st[si].merge(st[2 * si], st[(2 * si) + 1]);
	}
}

void query(node& res, int ss, int se, int si, int x, int y) {
	if (ss == x && se == y) {
		res = st[si];
		return;
	}

	int mid = getMid(ss, se);

	// res in left
	if (y <= mid) {
		query(res, ss, mid, 2 * si, x, y);
		return;
	}

	// res in right
	if (x > mid) {
		query(res, mid + 1, se, (2 * si) + 1, x, y);
		return;
	}

	// crossing mid
	node l, r;
	query(l, ss, mid, 2 * si, x, mid);
	query(r, mid + 1, se, (2 * si) + 1, mid + 1, y);
	res.merge(l, r);
	return;
}

void constructST(int l, int r, int si) {
	if (l == r) {
		st[si].setAll(a[l]);
	} else {
		int mid = getMid(l, r);
		constructST(l, mid, 2 * si);
		constructST(mid + 1, r, (2 * si) + 1);
		st[si].merge(st[2 * si], st[(2 * si) + 1]);
	}
}

inline int getMid(int l, int r) {
	return (l + r) / 2;
}

inline int max(int p, int q) {
	return (p > q) ? p : q;
}
