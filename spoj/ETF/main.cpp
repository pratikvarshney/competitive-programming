/*
 * main.cpp
 *
 *  Created on: 31-Dec-2016
 *      Author: Pratik Varshney
 */

#include<cstdio>
#include<cmath>

using namespace std;

int main() {
	// init
	int limit = 1000;
	long long sum = 0;
	int sievebound = (limit - 1) / 2;
	bool sieve[sievebound + 1] = { 0 };
	int crosslimit = sqrt(limit - 1) / 2;
	int i, j;
	for (i = 1; i <= crosslimit; i++) {
		if (!sieve[i]) {
			for (j = 2 * i * (i + 1); j <= sievebound; j += (2 * i) + 1) {
				sieve[j] = 1;
			}
		}
	}
	int prime[1000];
	prime[0] = 2;
	int numPrimes = 1;

	for (i = 1; i <= sievebound; i++) {
		if (!sieve[i]) {
			prime[numPrimes] = ((2 * i) + 1);
			numPrimes++;
		}
	}

	// process
	int t;
	scanf("%d", &t);
	while (t--) {
		int n;
		scanf("%d", &n);
		// factors
		int factor[20];
		int numFactors = 0;
		int k = n;
		int m = (int) ceil(sqrt(n));
		for (int i = 0; k > 1 && i < numPrimes && prime[i] <= m; i++) {
			if ((k % prime[i]) == 0) {
				factor[numFactors] = prime[i];
				numFactors++;
				k /= prime[i];
				while ((k % prime[i]) == 0) {
					k /= prime[i];
				}
			}
		}
		if (k > 1) {
			factor[numFactors] = k;
			numFactors++;
		}
		long long res = 0;

		long long numerator = 1;
		long long denominator = 1;
		for (int i = 0; i < numFactors; i++) {
			denominator *= factor[i];
			numerator *= (factor[i] - 1);
		}
		res = (n * numerator) / denominator;
		printf("%lld\n", res);
	}

	return 0;
}
