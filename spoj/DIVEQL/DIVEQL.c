#include<stdio.h>

#define DIV 1000000007

long long unsigned power(long long unsigned a, long long unsigned b);
long long unsigned inv_mod(long long unsigned a, long long unsigned n);

int main()
{
    int t;
    long long unsigned n, p, z, x;

    scanf("%d", &t);
    while(t--)
    {
        scanf("%llu%llu", &n, &p);
        x = power(p, n-1);
        z = (p * x)%DIV;
        //compute z = z-1
        if(z==0) z=DIV-1;
        else z--;

        z = (z * inv_mod(p-1, DIV))%DIV;
        printf("%llu %llu\n", z, x);
    }

    return 0;
}

long long unsigned power(long long unsigned a, long long unsigned b)
{
    long long unsigned res, t;

    res = 1;
    t = a%DIV;

    //calc
    while(b>0)
    {
        if((b&1)!=0)
        {
            res = (res * t)%DIV;
        }
        t = (t*t)%DIV;
        b >>= 1;
    }

    return res;
}

long long unsigned inv_mod(long long unsigned a, long long unsigned n)
{
    long long t, newt, r, newr, q;
    long long temp;
    t = 0;
    newt = 1;
    r = n;
    newr = a;
    while(newr != 0)
    {
        q = r/newr;
        temp = newt;
        newt = t - q * newt;
        t = temp;
        temp = newr;
        newr = r - q * newr;
        r = temp;
    }
    if(t<0) t += n;
    return (long long unsigned)t;
}
