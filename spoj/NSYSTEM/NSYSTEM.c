#include<stdio.h>

int mcxi_to_dec(char* str);
int dec_to_mcxi(int val, char* str);

int main()
{
    int t;
    char a[10];
    char b[10];
    char c[10];

    scanf("%d", &t);
    for(; t>0; t--)
    {
        scanf("%s%s", a, b);
        dec_to_mcxi(mcxi_to_dec(a)+mcxi_to_dec(b), c);
        printf("%s\n", c);
    }
    return 0;
}

int mcxi_to_dec(char* str)
{
    int m, c, x, i;
    int val=0;
    for(i=0; str[i]!='\0'; i++)
    {
        switch(str[i])
        {
        case 'm':
            if(i>0 && (str[i-1]>='2' && str[i-1]<='9'))
            {
                val+= ((str[i-1]-'0')*1000);
            }
            else
            {
                val+=1000;
            }
            break;
        case 'c':
            if(i>0 && (str[i-1]>='2' && str[i-1]<='9'))
            {
                val+= ((str[i-1]-'0')*100);
            }
            else
            {
                val+=100;
            }
            break;
        case 'x':
            if(i>0 && (str[i-1]>='2' && str[i-1]<='9'))
            {
                val+= ((str[i-1]-'0')*10);
            }
            else
            {
                val+=10;
            }
            break;
        case 'i':
            if(i>0 && (str[i-1]>='2' && str[i-1]<='9'))
            {
                val+= (str[i-1]-'0');
            }
            else
            {
                val+=1;
            }
            break;
        default:
            break;
        }
    }
    return val;
}

int dec_to_mcxi(int val, char* str)
{
    int m, c, x, i;
    int k;

    m = val/1000;
    c = (val/100)%10;
    x = (val/10)%10;
    i = val%10;

    k=0; //index

    //m
    if(m>0)
    {
        if(m>1)
        {
            str[k]=m+'0';
            k++;
        }
        str[k]='m';
        k++;
    }

    //c
    if(c>0)
    {
        if(c>1)
        {
            str[k]=c+'0';
            k++;
        }
        str[k]='c';
        k++;
    }

    //x
    if(x>0)
    {
        if(x>1)
        {
            str[k]=x+'0';
            k++;
        }
        str[k]='x';
        k++;
    }

    //i
    if(i>0)
    {
        if(i>1)
        {
            str[k]=i+'0';
            k++;
        }
        str[k]='i';
        k++;
    }

    str[k]='\0';


    return 0;
}
