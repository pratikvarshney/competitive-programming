#include<stdio.h>

#define MAXSIZE 100
#define MAXVAL 200

int main()
{
    int mat[MAXSIZE][MAXSIZE]={0};
    int n, m, i, j, min;
    scanf("%d%d", &n, &m);
    for(i=1; i<=n; i++)
    {
        mat[i][0] = MAXVAL;
        mat[i][n+1] = MAXVAL;
        for(j=1; j<=m; j++)
        {
            scanf("%d", &(mat[i][j]));
        }
    }

    for(i=n; i>1; i--)
    {
        for(j=1; j<=m; j++)
        {
            min = mat[i][j-1];
            if(min > mat[i][j]) min = mat[i][j];
            if(min > mat[i][j+1]) min = mat[i][j+1];
            mat[i-1][j] += min;
        }
    }

    min = mat[1][1];
    for(j=2; j<=m; j++)
    {
        if(min > mat[1][j]) min = mat[1][j];
    }

    printf("%d\n", min);
    return 0;
}
