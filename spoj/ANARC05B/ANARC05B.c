#include<stdio.h>

#define MAXSIZE 20000
#define SENTINAL 1234567

int main()
{
    int arr_a[MAXSIZE], arr_b[MAXSIZE], a, b, i, j;
    int sub_sum_a[MAXSIZE], sub_sum_b[MAXSIZE], k;
    int sum;

    while(1)
    {
        scanf("%d", &a);
        if(a==0) break;

        for(i=0; i<a; i++)
        {
            scanf("%d", &(arr_a[i]));
        }

        scanf("%d", &b);

        for(i=0; i<b; i++)
        {
            scanf("%d", &(arr_b[i]));
        }
        arr_a[a] = SENTINAL;
        arr_b[b] = SENTINAL;

        i=0;
        j=0;
        k=0;
        sub_sum_a[0]=0;
        sub_sum_b[0]=0;
        while(1)
        {
            while(arr_a[i]!=arr_b[j])
            {
                while(arr_a[i]<arr_b[j])
                {
                    sub_sum_a[k] += arr_a[i];
                    i++;
                }
                while(arr_b[j]<arr_a[i])
                {
                    sub_sum_b[k] += arr_b[j];
                    j++;
                }
            }
            if(arr_a[i] == SENTINAL && arr_b[j] == SENTINAL)
            {
                k++;
                break;
            }
            sub_sum_a[k] += arr_a[i];
            i++;
            sub_sum_b[k] += arr_b[j];
            j++;
            k++;
            sub_sum_a[k]=0;
            sub_sum_b[k]=0;
        }
        sum = 0;
        for(i=0; i<k; i++)
        {
            sum += (sub_sum_a[i] > sub_sum_b[i])?sub_sum_a[i]:sub_sum_b[i];
        }
        printf("%d\n", sum);
    }
    return 0;
}
