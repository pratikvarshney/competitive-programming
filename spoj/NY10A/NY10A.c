#include<stdio.h>
#include<string.h>

int main()
{
    char seq[8][4]={"TTT", "TTH", "THT", "THH", "HTT", "HTH", "HHT", "HHH"};
    int p, i, n, c;
    char str[48];
    char *temp;
    scanf("%d", &p);

    for(;p>0; p--)
    {
        scanf("%d", &n);
        scanf("%s", str);
        printf("%d", n);
        for(i=0; i<8; i++)
        {
            c=0;
            temp = str;
            while(1)
            {
                temp = strstr(temp, seq[i]);
                if(temp != NULL)
                {
                    c++;
                    temp++;
                }
                else break;
            }
            printf(" %d", c);
        }
        printf("\n");
    }
    return 0;
}
